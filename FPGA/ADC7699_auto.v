`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:23:41 10/31/2012 
// Design Name: 
// Module Name:    ADC7699_Interface 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module ADC7699_auto(
 input clk,
 input reset,
 input start_conv_1,
 input start_conv_2,
 input start_conv_3,
 input start_conv_4,
 input start_conv_5,
 input start_conv_6,
 input start_conv_7,
 input start_conv_8,
 input start_conv_iout, //converts both IOUTP nad IOUTM
 input start_conv_single, //converts single current chosen by iout_sel = 2'b00 and puts it on {IOUTP_VAL, IOUTP_RANGE}
 //input [1:0] itov_mux,
 output wire [15:0] out_reg,
 output wire [15:0] ioutp_val,
 output wire [15:0] ioutm_val,
 output reg [2:0] ioutp_range,
 output reg [2:0] ioutm_range,
 output reg done,
 output reg iout_done,
 output reg lock_rladder,
 
 //ADC
 input SDO,
 output wire SCLK,
 output wire CNV,
 output wire DIN,
 
 //controls to FGNNChip
 output reg [2:0] range_sel,
 output reg [1:0] iout_sel
);


// parameters declaration
// state machine states
parameter s_ready = 					28'b0000000000000000000000000001;
parameter s_norm1 = 					28'b0000000000000000000000000010;
parameter s_norm2 = 					28'b0000000000000000000000000100;
parameter s_norm3	=	   			28'b0000000000000000000000001000;
parameter s_ioutp1 =					28'b0000000000000000000000010000;
parameter s_ioutp2 =					28'b0000000000000000000000100000;
parameter s_ioutp3 =					28'b0000000000000000000001000000;
parameter s_ioutp4 =					28'b0000000000000000000010000000;
parameter s_ioutp5 =					28'b0000000000000000000100000000;
parameter s_ioutp6 =					28'b0000000000000000001000000000;
parameter s_ioutp7 =					28'b0000000000000000010000000000;
parameter s_ioutm1 =					28'b0000000000000000100000000000;
parameter s_ioutm2 =					28'b0000000000000001000000000000;
parameter s_ioutm3 =					28'b0000000000000010000000000000;
parameter s_ioutm4 =					28'b0000000000000100000000000000;
parameter s_ioutm5 =					28'b0000000000001000000000000000;
parameter s_ioutm6 =					28'b0000000000010000000000000000;
parameter s_ioutm7 =					28'b0000000000100000000000000000;
parameter s_wait1 =					28'b0000000001000000000000000000;
parameter s_wait2 =					28'b0000000010000000000000000000;
parameter s_single1 =				28'b0000000100000000000000000000;
parameter s_single2 =				28'b0000001000000000000000000000;
parameter s_single3 =				28'b0000010000000000000000000000;
parameter s_single4 =				28'b0000100000000000000000000000;
parameter s_single5 =				28'b0001000000000000000000000000;
parameter s_single6 =				28'b0010000000000000000000000000;
parameter s_single7 =				28'b0100000000000000000000000000;
parameter s_single_wait1 =			28'b1000000000000000000000000000;



parameter SHIFT_BY = 2;

parameter N_iter 	= 					5'h4; //number of samples in averaging
parameter THR_HI  =					16'd15728; //2.4V
parameter THR_LO	=					16'd3276; //0.5V
parameter WAIT_5mS = 				20'h52080; //7ms actually		//20'h3A980;
parameter WAIT_750uS = 				20'h11940; //1.5ms	//16'h8CA0;
parameter WAIT_500uS = 				16'hBB80; //1ms	//16'h5DC0;
parameter WAIT_200uS = 				16'h4B00; //400us	//16'h2580;
parameter DIR_UP	=					2'b01;
parameter DIR_DOWN	=				2'b10;
parameter DIR_NONE	=				2'b00;

reg start_conv;
reg [15:0] cfg;
wire cycle_done;
ADC7699_cycle adc_cycle_inst(.clk(clk),.reset(reset),.start_conv(start_conv),.cfg(cfg),.SDO(SDO),.CNV(CNV),.DIN(DIN),.done(cycle_done),.out_reg(out_reg),.SCLK(SCLK));

//signal declaration
wire [15:0] channel1_code = 16'b1111000111000100; //next channel to be read is 0
wire [15:0] channel2_code = 16'b1111001111000100; //code for full power mode, next channel to be read is 1
wire [15:0] channel3_code = 16'b1111010111000100; //code for full power mode, next channel to be read is 2
wire [15:0] channel4_code = 16'b1111011111000100; //code for full power mode, next channel to be read is 3
wire [15:0] channel5_code = 16'b1111100111000100; //code for full power mode, next channel to be read is 4
wire [15:0] channel6_code = 16'b1111101111000100; //code for full power mode, next channel to be read is 5
wire [15:0] channel7_code = 16'b1111110111000100; //code for full power mode, next channel to be read is 6
wire [15:0] channel8_code = 16'b1111111111000100; //code for full power mode, next channel to be read is 7

reg [15+SHIFT_BY:0] ioutp_val_i;
reg [15+SHIFT_BY:0] ioutm_val_i;
reg [4:0] iter;
reg [27:0] state;
reg [19:0] t_wait; //300 us between range changes
reg [1:0] range_dir;

assign ioutp_val = ioutp_val_i[15+SHIFT_BY:SHIFT_BY];
assign ioutm_val = ioutm_val_i[15+SHIFT_BY:SHIFT_BY];

always @(posedge clk)
	if (reset) begin
		state <= s_ready;
		done <= 1'b0;
		iout_done <= 1'b0;
		start_conv <= 1'b0;
		ioutp_val_i <= 0;
		ioutm_val_i <= 0;
		ioutp_range <= 3'b010;
		ioutm_range <= 3'b010;
		range_sel <= 3'b010;
		iout_sel <= 2'b01;
		range_dir <= DIR_NONE;
		lock_rladder <= 0;
	end
	
	else case (state)
	
// ------------	INITIAL STATE --------------------------
	s_ready: begin
	done <= 1'b0;
	iout_done <= 1'b0;
	range_sel <= 3'b010;
	iout_sel <= 2'b01;
	range_dir <= DIR_NONE;
	lock_rladder <= 0;
	if (start_conv_1) begin
		state <= s_norm1;
		cfg <= channel1_code;
		start_conv <= 1'b1;
	end
	else if (start_conv_2) begin
		state <= s_norm1;
		cfg <= channel2_code;
		start_conv <= 1'b1;
	end
	else if (start_conv_3) begin
		state <= s_norm1;
		cfg <= channel3_code;
		start_conv <= 1'b1;
	end
	else if (start_conv_4) begin
		state <= s_norm1;
		cfg <= channel4_code;
		start_conv <= 1'b1;
	end
	else if (start_conv_5) begin
		state <= s_norm1;
		cfg <= channel5_code;
		start_conv <= 1'b1;
	end
	else if (start_conv_6) begin
		state <= s_norm1;
		cfg <= channel6_code;
		start_conv <= 1'b1;
	end
	else if (start_conv_7) begin
		state <= s_norm1;
		cfg <= channel7_code;
		start_conv <= 1'b1;
	end
	else if (start_conv_8) begin
		state <= s_norm1;
		cfg <= channel8_code;
		start_conv <= 1'b1;
	end
	else if (start_conv_iout) begin
		state <= s_ioutp1;
		cfg <= channel6_code;
		start_conv <= 1'b0;
	end
	else if (start_conv_single) begin
		state <= s_single1;
		cfg <= channel6_code;
		start_conv <= 1'b0;
	end
	else begin
		state <= s_ready;
		start_conv <= 1'b0;
	end	
	end 

// ------------	NORMAL CONVERSION (ONE_SHOT CONVERSION) --------------------------
	s_norm1: begin
	done <= 1'b0;
	if (cycle_done == 1'b1) begin
		state <= s_norm2;
		start_conv <= 1'b1;
	end
	else begin
		state <= s_norm1;
		start_conv <= 1'b0;
	end
	end
	
	s_norm2: begin
	done <= 1'b0;
	if (cycle_done == 1'b1) begin
		state <= s_norm3;
		start_conv <= 1'b1;
	end
	else begin
		state <= s_norm2;
		start_conv <= 1'b0;
	end
	end
	
	s_norm3: begin
	start_conv <= 1'b0;
	if (cycle_done == 1'b1) begin
		state <= s_ready;
		done <= 1'b1;
	end
	else begin
		state <= s_norm3;
		done <= 1'b0;
	end
	end
	
// ------------------------------------------------------------	
// ------------	IOUTP/M CONVERSION --------------------------	
// ------------------------------------------------------------
// ------------ IOUTP -------------------
	s_ioutp1: begin
	iout_done <= 1'b0;
	range_sel <= 3'b010;
	start_conv <= 1'b0;
	iout_sel <= 2'b01;
	t_wait <= WAIT_500uS;
	state <= s_wait1;
	end
	
	s_wait1: begin
	iout_done <= 1'b0;
	start_conv <= 1'b0;
	iout_sel <= 2'b01;
	if (t_wait == 0) begin
		state <= s_ioutp2;
	end
	else begin
		t_wait <= t_wait - 1;
		state <= s_wait1;
	end
	end
	
	//start 1st conversion
	s_ioutp2: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b01;
	start_conv <= 1'b1;
	state <= s_ioutp3;
	end
	
	//wait and start 2nd conversion
	s_ioutp3: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b01;
	if (cycle_done == 1'b1) begin
		state <= s_ioutp4;
		start_conv <= 1'b1;
	end
	else begin
		state <= s_ioutp3;
		start_conv <= 1'b0;
	end
	end
	
	//wait and start 3d conversion
	s_ioutp4: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b01;
	if (cycle_done == 1'b1) begin
		state <= s_ioutp5;
		iter <= N_iter;
		start_conv <= 1'b1;
		ioutp_val_i <= 0;
	end
	else begin
		state <= s_ioutp4;
		start_conv <= 1'b0;
	end
	end

	//wait for ith conversion completion
	s_ioutp5: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b01;
	if (cycle_done == 1'b1) begin
		state <= s_ioutp6;
		ioutp_val_i <= ioutp_val_i + out_reg;
		iter <= iter - 1;
		start_conv <= 1'b0;
	end
	else begin
		state <= s_ioutp5;		
		start_conv <= 1'b0;
	end
	end
	
	s_ioutp6: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b01;
	if (iter == 0) begin
		start_conv <= 1'b0;
		state <= s_ioutp7;
	end
	else begin
		start_conv <= 1'b1;
		state <= s_ioutp5;
	end
	end
	
	// check if we need to change the range
	s_ioutp7: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b01;
	start_conv <= 1'b0;
	if ((ioutp_val > THR_HI) & (range_sel < 3'b100) & (range_dir != DIR_DOWN)) begin
		range_sel <= range_sel + 1;
		range_dir <= DIR_UP;
		state <= s_wait1;
		case (range_sel)
			3'b010: t_wait <= WAIT_200uS;
			3'b011: t_wait <= WAIT_200uS;
			default: t_wait <= WAIT_200uS;
		endcase		
	end
	else if ((ioutp_val < THR_LO) & (range_sel > 3'b000) & (range_dir != DIR_UP)) begin
		range_sel <= range_sel - 1;
		range_dir <= DIR_DOWN;
		state <= s_wait1;
		case (range_sel)
			3'b010: t_wait <= WAIT_750uS;
			3'b001: t_wait <= WAIT_5mS;
			default: t_wait <= WAIT_200uS;
		endcase
	end
	else begin
		state <= s_ioutm1;
		ioutp_range <= range_sel;
	end
	end	
	
	
// ------------ IOUTM -------------------
	s_ioutm1: begin
	iout_done <= 1'b0;
	range_sel <= 3'b010;
	start_conv <= 1'b0;
	iout_sel <= 2'b10;
	t_wait <= WAIT_500uS;
	range_dir <= DIR_NONE;
	state <= s_wait2;
	end
	
	s_wait2: begin
	iout_done <= 1'b0;
	start_conv <= 1'b0;
	iout_sel <= 2'b10;
	if (t_wait == 0) begin
		state <= s_ioutm2;
	end
	else begin
		t_wait <= t_wait - 1;
		state <= s_wait2;
	end
	end
	
	s_ioutm2: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b10;
	start_conv <= 1'b1;
	state <= s_ioutm3;
	end
	
	s_ioutm3: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b10;
	if (cycle_done == 1'b1) begin
		state <= s_ioutm4;
		start_conv <= 1'b1;
	end
	else begin
		state <= s_ioutm3;
		start_conv <= 1'b0;
	end
	end
	
	s_ioutm4: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b10;
	if (cycle_done == 1'b1) begin
		state <= s_ioutm5;
		iter <= N_iter;
		start_conv <= 1'b1;
		ioutm_val_i <= 0;
	end
	else begin
		state <= s_ioutm4;
		start_conv <= 1'b0;
	end
	end

	//wait for ith conversion completion
	s_ioutm5: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b10;
	if (cycle_done == 1'b1) begin
		state <= s_ioutm6;
		ioutm_val_i <= ioutm_val_i + out_reg;
		iter <= iter - 1;
		start_conv <= 1'b0;
	end
	else begin
		state <= s_ioutm5;		
		start_conv <= 1'b0;
	end
	end
	
	s_ioutm6: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b10;
	if (iter == 0) begin
		start_conv <= 1'b0;
		state <= s_ioutm7;
	end
	else begin
		start_conv <= 1'b1;
		state <= s_ioutm5;
	end
	end
	
	// check if we need to change the range
	s_ioutm7: begin
	iout_sel <= 2'b10;
	start_conv <= 1'b0;
	if ((ioutm_val > THR_HI) & (range_sel < 3'b100) & (range_dir != DIR_DOWN)) begin
		range_sel <= range_sel + 1;
		iout_done <= 1'b0;
		range_dir <= DIR_UP;
		state <= s_wait2;
		case (range_sel)
			3'b010: t_wait <= WAIT_200uS;
			3'b011: t_wait <= WAIT_200uS;
			default: t_wait <= WAIT_200uS;
		endcase
	end
	else if ((ioutm_val < THR_LO) & (range_sel > 3'b000) & (range_dir != DIR_UP)) begin
		range_sel <= range_sel - 1;
		iout_done <= 1'b0;
		range_dir <= DIR_DOWN;
		state <= s_wait2;
		case (range_sel)
			3'b010: t_wait <= WAIT_750uS;
			3'b001: t_wait <= WAIT_5mS;
			default: t_wait <= WAIT_200uS;
		endcase
	end
	else begin
		state <= s_ready;
		ioutm_range <= range_sel;
		iout_done <= 1'b1;
	end
	end	
	
// ------------------------------------------------------------	
// ------------	SINGLE CONVERSION --------------------------	
// ------------------------------------------------------------
	s_single1: begin
	iout_done <= 1'b0;
	range_sel <= 3'b010;
	start_conv <= 1'b0;
	iout_sel <= 2'b00;
	t_wait <= WAIT_500uS;
	state <= s_single_wait1;
	lock_rladder <= 1'b1;
	end
	
	s_single_wait1: begin
	iout_done <= 1'b0;
	start_conv <= 1'b0;
	iout_sel <= 2'b00;
	lock_rladder <= 1'b1;
	if (t_wait == 0) begin
		state <= s_single2;
	end
	else begin
		t_wait <= t_wait - 1;
		state <= s_single_wait1;
	end
	end
	
	//start 1st conversion
	s_single2: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b00;
	start_conv <= 1'b1;
	state <= s_single3;
	lock_rladder <= 1'b1;
	end
	
	//wait and start 2nd conversion
	s_single3: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b00;
	lock_rladder <= 1'b1;
	if (cycle_done == 1'b1) begin
		state <= s_single4;
		start_conv <= 1'b1;
	end
	else begin
		state <= s_single3;
		start_conv <= 1'b0;
	end
	end
	
	//wait and start 3d conversion
	s_single4: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b00;
	lock_rladder <= 1'b1;
	if (cycle_done == 1'b1) begin
		state <= s_single5;
		iter <= N_iter;
		start_conv <= 1'b1;
		ioutp_val_i <= 0;
	end
	else begin
		state <= s_single4;
		start_conv <= 1'b0;
	end
	end

	//wait for ith conversion completion
	s_single5: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b00;
	lock_rladder <= 1'b1;
	if (cycle_done == 1'b1) begin
		state <= s_single6;
		ioutp_val_i <= ioutp_val_i + out_reg;
		iter <= iter - 1;
		start_conv <= 1'b0;
	end
	else begin
		state <= s_single5;		
		start_conv <= 1'b0;
	end
	end
	
	s_single6: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b00;
	lock_rladder <= 1'b1;
	if (iter == 0) begin
		start_conv <= 1'b0;
		state <= s_single7;
	end
	else begin
		start_conv <= 1'b1;
		state <= s_single5;
	end
	end
	
	// check if we need to change the range
	s_single7: begin
	iout_done <= 1'b0;
	iout_sel <= 2'b00;
	start_conv <= 1'b0;
	lock_rladder <= 1'b1;
	if ((ioutp_val > THR_HI) & (range_sel < 3'b100) & (range_dir != DIR_DOWN)) begin
		range_sel <= range_sel + 1;
		range_dir <= DIR_UP;
		state <= s_single_wait1;
		case (range_sel)
			3'b010: t_wait <= WAIT_200uS;
			3'b011: t_wait <= WAIT_200uS;
			default: t_wait <= WAIT_200uS;
		endcase		
	end
	else if ((ioutp_val < THR_LO) & (range_sel > 3'b000) & (range_dir != DIR_UP)) begin
		range_sel <= range_sel - 1;
		range_dir <= DIR_DOWN;
		state <= s_single_wait1;
		case (range_sel)
			3'b010: t_wait <= WAIT_750uS;
			3'b001: t_wait <= WAIT_5mS;
			default: t_wait <= WAIT_200uS;
		endcase
	end
	else begin
		state <= s_ready;
		ioutp_range <= range_sel;
		iout_done <= 1'b1;
	end
	end		
	
	
	
	
endcase

endmodule


