`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:17:47 01/04/2011 
// Design Name: 
// Module Name:    DAC5380_Interface 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DAC5380_Interface(
    output wire [13:0] DAC_DB,
    output wire [1:0] DAC_REG,
    output wire [5:0] DAC_ADDR,
    output reg DAC_WR,
    output reg DAC_A_B,
    output reg DAC_LDAC,
	 
	 input dac_clk,
	 input dac_reset1,
	 input dac_start,
	 input [1:0] hi_dac_reg,
	 input [13:0] hi_dac_db,
	 input [5:0] hi_dac_addr	 
    );

reg [3:0] cnt_down;
reg state;
parameter p_clk_period = 4'h5;
parameter s_idle = 1'b0;
parameter s_write_1 = 1'b1;

assign DAC_DB = hi_dac_db;
assign DAC_REG = hi_dac_reg;
assign DAC_ADDR = hi_dac_addr;

always @(posedge dac_clk)
	if (dac_reset1) begin
		state <= s_idle;
	end	

	else case (state)
	
	s_idle: begin
	if (dac_start) begin
		state <= s_write_1;
		cnt_down <= p_clk_period;
	end
	else begin
		state <= s_idle;
		DAC_WR <= 1'b1;
		DAC_A_B <= 1'b0;
		DAC_LDAC <= 1'b0;
		cnt_down <= p_clk_period;
	end
	end

	s_write_1: begin
	if (cnt_down == 0) begin
		state <= s_idle;
		DAC_WR <= 1'b0;
	end
	else begin
		state <= s_write_1;
		cnt_down <= cnt_down - 1;	
		DAC_WR <= 1'b0;
	end	
	end		
endcase

endmodule
