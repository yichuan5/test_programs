`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:18:44 01/20/2011 
// Design Name: 
// Module Name:    DPulse_ctrl 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DPulse_ctrl(
    input clk,
    input set_to_0,
    input set_to_1,
    input reset,
    input start_pulse,
    input [15:0] dpulse_lo,
	 input [7:0] dpulse_hi,
	 output reg NN_DPULSE
    );

reg [23:0] cntr;
reg [1:0] state;

parameter s_ready = 		 2'b00;
parameter s_pulse1 = 	 2'b01;
parameter s_pulse2 = 	 2'b10;
parameter s_pulse3 = 	 2'b11;
	 
	 
always @(posedge clk)
	if (reset) begin
		state <= s_ready;
		NN_DPULSE <= 1'b0;
		cntr[23:16] <= dpulse_hi;
		cntr[15:0] <= dpulse_lo;
	end
	
	else case (state)
	
	s_ready: begin
		cntr[23:16] <= dpulse_hi;
		cntr[15:0] <= dpulse_lo;
		if (set_to_0) begin
			state <= s_pulse1;
		end
		else if	(set_to_1) begin
			state <= s_pulse2;
		end
		else if (start_pulse) begin
			state <= s_pulse3;
		end
		else begin
			state <= s_ready;
		end
	end

	s_pulse1: begin
		NN_DPULSE <= 1'b0;
		state <= s_ready;
	end	
	
	s_pulse2: begin
		NN_DPULSE <= 1'b1;
		state <= s_ready;
	end

	s_pulse3: begin
		if (cntr != 24'h000000) begin
			state <= s_pulse3;
			cntr <= cntr - 1;
			NN_DPULSE <= 1'b1;
		end	
		else begin
			state <= s_ready;
			NN_DPULSE <= 1'b0;
		end	
	end
	endcase	

endmodule
