`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:07:40 12/13/2012 
// Design Name: 
// Module Name:    prog_dyn_mem 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module prog_dyn_mem(
	//to host interface
	input clk,
	input reset,
	input start_prog,
	input start_readout,
	input [10:0] num_of_loc,
	input [15:0] dyn_prog_time,
	input [15:0] readout_time, //default 1 ms
	input fgaddrbuf_wren,
	input [10:0] fgaddrbuf_din,
	input idacvalbuf_lo_wren,
	input idacvalbuf_hi_wren,
	input [15:0] idacvalbuf_lo_din,
	input [11:0] idacvalbuf_hi_din,
	input readoutbuf_rden,
	output wire [15:0] readoutbuf_dout,
	input readoutbuf_rng_rden,
	output wire [2:0] readoutbuf_rng_dout,
	input dynwaitram_wren,
	input [15:0] dynwaitram_din,
	
	//to on-chip idac
	output reg [23:0] idac_val,
	output reg idac_start_conv,
	
	//to NNFG chip
	output reg [4:0] row,
	output reg [5:0] col,
	output reg addr_en,
	
	//from ADC7699_auto
	output reg start_conv_single,
	input conv_done,
	input [15:0] ioutp_val,
	input [2:0] ioutp_range,
	
	//misc
	output reg auto_on,
	output reg prog_dyn_done,
	output reg readout_done
	
    );

/********* algorithm ***********************	
auto = 1
for curr_loc = 1:num_loc
	idac_val[11:0] <- idacvalbuf_lo[common_addr_b]
	idac_val[23:12] <- idacvalbuf_hi[common_addr_b]
	idac_trig_val
	row<-fgaddrbuf[10:6][common_addr_b]
	col<-fgaddrbuf[5:0][common_addr_b]
	wait for a couple of clk periods
	addr_en = 1;
	wait for dyn_prog_time
	addr_en = 0;
	common_addr_b += 1
end	
auto = 0
*/	

wire [10:0] fgaddrbuf_dout;
wire [15:0] idacvalbuf_lo_dout;
wire [11:0] idacvalbuf_hi_dout;
wire [15:0] readoutbuf_din;
wire [2:0] readoutbuf_rng_din;
reg readoutbuf_wren;
reg readoutbuf_rng_wren;
reg [10:0] common_addr_b;
reg [10:0] fgaddrbuf_addr_a;
reg [10:0] idacvalbuf_lo_addr_a;
reg [10:0] idacvalbuf_hi_addr_a;
reg [3:0] dynwaitram_addr_a;
wire [15:0] dynwaitram_dout;

reg [15:0] curr_loc;
reg [31:0] wait_cntr;
reg [3:0] wait_ind;
reg [9:0] state;

assign readoutbuf_din = ioutp_val;
assign readoutbuf_rng_din = ioutp_range;

dgaddrbuf fgaddrbuf_inst (
	.clka(clk),
	.wea(fgaddrbuf_wren), // Bus [0 : 0] 
	.addra(fgaddrbuf_addr_a), // Bus [10 : 0] 
	.dina(fgaddrbuf_din), // Bus [10 : 0] 
	.clkb(clk),
	.addrb(common_addr_b), // Bus [10 : 0] 
	.doutb(fgaddrbuf_dout)); // Bus [10 : 0] 
	
idacandtime idacvalbuf_lo (
	.clka(clk),
	.wea(idacvalbuf_lo_wren), // Bus [0 : 0] 
	.addra(idacvalbuf_lo_addr_a), // Bus [10 : 0] 
	.dina(idacvalbuf_lo_din), // Bus [11 : 0] 
	.clkb(clk),
	.addrb(common_addr_b), // Bus [10 : 0] 
	.doutb(idacvalbuf_lo_dout)); // Bus [11 : 0] 

idacvalbuf idacvalbuf_hi (
	.clka(clk),
	.wea(idacvalbuf_hi_wren), // Bus [0 : 0] 
	.addra(idacvalbuf_hi_addr_a), // Bus [10 : 0] 
	.dina(idacvalbuf_hi_din), // Bus [11 : 0] 
	.clkb(clk),
	.addrb(common_addr_b), // Bus [10 : 0] 
	.doutb(idacvalbuf_hi_dout)); // Bus [11 : 0]

readoutbuf readoutbuf_inst (
	.clk(clk),
	.srst(reset),
	.din(readoutbuf_din), // Bus [15 : 0] 
	.wr_en(readoutbuf_wren),
	.rd_en(readoutbuf_rden),
	.dout(readoutbuf_dout), // Bus [15 : 0] 
	.full(),
	.empty());
	
readoutbuf_rng readoutbuf_rng_inst (
	.clk(clk),
	.srst(reset),
	.din(readoutbuf_rng_din), // Bus [2 : 0] 
	.wr_en(readoutbuf_rng_wren),
	.rd_en(readoutbuf_rng_rden),
	.dout(readoutbuf_rng_dout), // Bus [2 : 0] 
	.full(),
	.empty());
	
dynwaitram dynwaitram_inst (
	.clka(clk),
	.wea(dynwaitram_wren), // Bus [0 : 0] 
	.addra(dynwaitram_addr_a), // Bus [3 : 0] 
	.dina(dynwaitram_din), // Bus [15 : 0] 
	.clkb(clk),
	.addrb(wait_ind), // Bus [3 : 0] 
	.doutb(dynwaitram_dout)); // Bus [15 : 0] 


parameter s_ready =		10'b0000000001;
parameter s_prog1 =		10'b0000000010;
parameter s_prog2 =		10'b0000000100;
parameter s_wait1 =		10'b0000001000;
parameter s_wait2 =		10'b0000100000;
parameter s_ro1 =			10'b0001000000;
parameter s_ro2 =			10'b0010000000;
parameter s_ro3 =			10'b0100000000;
parameter s_ro_wait1 =	10'b1000000000;


always @(posedge clk)
if (reset) begin
	state <= s_ready;
	fgaddrbuf_addr_a <= 0;
	idacvalbuf_lo_addr_a <= 0;
	idacvalbuf_hi_addr_a <= 0;
	dynwaitram_addr_a <= 0;
	readoutbuf_wren <= 0;
	readoutbuf_rng_wren <= 0;
	common_addr_b <= 0;
	curr_loc <= 1;
	wait_cntr <= 0;
	wait_ind	<= 0;
	
	idac_val <= 24'h0;
	idac_start_conv <= 0;
	start_conv_single <= 0;
	row <= 0;
	col <= 0;
	addr_en <= 0;
	auto_on <= 0;
	prog_dyn_done <= 0;
	readout_done <= 0;
end
else if (fgaddrbuf_wren) begin
	fgaddrbuf_addr_a <= fgaddrbuf_addr_a + 1; // update input address for okPipeIn
end
else if (idacvalbuf_lo_wren) begin
	idacvalbuf_lo_addr_a <= idacvalbuf_lo_addr_a + 1;	// update input address for okPipeIn
end
else if (idacvalbuf_hi_wren) begin
	idacvalbuf_hi_addr_a <= idacvalbuf_hi_addr_a + 1;	// update input address for okPipeIn
end
else if (dynwaitram_wren) begin
	dynwaitram_addr_a <= dynwaitram_addr_a + 1;	// update input address for okPipeIn
end	
else case (state)
s_ready: begin
	common_addr_b <= 0; //address of the memory location to be sent to IDAC
	curr_loc <= 1; //index variable
	idac_start_conv <= 0;
	prog_dyn_done <= 0;
	readout_done <= 0;
	start_conv_single <= 0;
	row <= 0;
	col <= 0;
	addr_en <= 0;
	readoutbuf_wren <= 0;
	readoutbuf_rng_wren <= 0;
		
	if (start_prog) begin
		state <= s_prog1;
		auto_on <= 1'b1;
	end
	else if (start_readout) begin
		state <= s_ro1;
		auto_on <= 1'b1;
	end
	else begin
		state <= s_ready;
		auto_on <= 1'b0;
	end
end	

//*************** dynamic programming *****************************
//FOR LOOP
s_prog1: begin
idac_start_conv <= 0;
addr_en <= 0;
if (curr_loc > num_of_loc) begin //used all vectors in ram
	state <= s_ready;
	prog_dyn_done <= 1;
	auto_on <= 1'b0;
end	
else begin
	state <= s_prog2;
	curr_loc <= curr_loc + 1;
end
end

s_prog2: begin
idac_val[11:0] <= idacvalbuf_lo_dout[11:0];
idac_val[23:12] <= idacvalbuf_hi_dout;
wait_ind <= idacvalbuf_lo_dout[15:12];
col <= fgaddrbuf_dout[5:0];
row <= fgaddrbuf_dout[10:6];
common_addr_b <= common_addr_b + 1;
idac_start_conv <= 1;
wait_cntr <= 32'h4F;
addr_en <= 0;
state <= s_wait1;
end

s_wait1: begin
idac_start_conv <= 0;
if (wait_cntr == 0) begin
	state <= s_wait2;
	addr_en <= 1;
	wait_cntr[7:0] <= 8'h00;
	wait_cntr[23:8] <= dynwaitram_dout;
	wait_cntr[31:24] <= 8'h00;
end
else begin
	state <= s_wait1;
	wait_cntr <= wait_cntr - 1;
end	
end

s_wait2: begin
if (wait_cntr == 0) begin
	state <= s_prog1;
	addr_en <= 0;
end
else begin
	state <= s_wait2;
	wait_cntr <= wait_cntr - 1;
	addr_en <= 1;
end	
end

//*************** dynamic arrya current readout *****************************
//FOR LOOP
s_ro1: begin
addr_en <= 0;
start_conv_single <= 0;
readoutbuf_wren <= 0;
readoutbuf_rng_wren <= 0;
if (curr_loc > num_of_loc) begin //used all vectors in ram
	state <= s_ready;
	readout_done <= 1;
	auto_on <= 1'b0;
end	
else begin
	state <= s_ro2;
	curr_loc <= curr_loc + 1;
end
end

s_ro2: begin
addr_en <= 0;
readoutbuf_wren <= 0;
readoutbuf_rng_wren <= 0;
start_conv_single <= 0;
col <= fgaddrbuf_dout[5:0];
row <= fgaddrbuf_dout[10:6];
common_addr_b <= common_addr_b + 1;
wait_cntr[7:0] <= 8'h00;
wait_cntr[23:8] <= readout_time;
wait_cntr[31:24] <= 8'h00;
state <= s_ro_wait1;
end

s_ro_wait1: begin
addr_en <= 0;
readoutbuf_wren <= 0;
readoutbuf_rng_wren <= 0;
if (wait_cntr == 0) begin
	state <= s_ro3;
	start_conv_single <= 1;
end
else begin
	state <= s_ro_wait1;
	wait_cntr <= wait_cntr - 1;
	start_conv_single <= 0;
end	
end

s_ro3: begin
start_conv_single <= 0;
addr_en <= 0;
if (conv_done) begin
	readoutbuf_wren <= 1;
	readoutbuf_rng_wren <= 1;
	state <= s_ro1;
end
else begin
	readoutbuf_wren <= 0;
	readoutbuf_rng_wren <= 0;
	state <= s_ro3;
end	
end

endcase

endmodule
