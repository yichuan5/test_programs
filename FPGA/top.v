`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:05:19 10/29/2012 
// Design Name: 
// Module Name:    top_test 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`default_nettype none

module top_auto(
   input  wire [7:0]  hi_in,
   output wire [1:0]  hi_out,
   inout  wire [15:0] hi_inout,
   
   output wire        i2c_sda,
   output wire        i2c_scl,
   output wire        hi_muxsel,
	 
//	DAC controls

	 output [13:0] DAC_DB,
	 output [1:0] DAC_REG,
	 output [5:0] DAC_ADDR,
	 output DAC_WR,
	 output DAC_A_B, //dont care
	 output DAC_LDAC,

//ADC	 
	 output ADC_SCLK,
	 output ADC_CNV,
	 output ADC_DIN,
	 output [1:0] ADC_CYCLE,
	 input ADC_SDO,

//NNFG Chip
	output PORT_SEL,
	output CLK_RANGE,
	output CLK_VAL,
	output OE,
	output SCAN_IN,
	input SCAN_OUT,
	
	output DATA_IN,
	output LATCH_EN,
	output DPULSE,
	output [4:0] ROW_ADDR,
	output [5:0] COL_ADDR,
	output LOCK,
	output MEAS,
	output PROG,
	output ADDR_EN,
	output GSEL0,
	output [1:0] DSEL,
	output IMASTER_SEL,
	output [1:0] IDAC_SEL,
	output ITOV_SEL,
	output [1:0] ITOV_INP_SEL,
	output [2:0] ITOV_RSEL
	
    );
	 
assign i2c_sda = 1'bz;
assign i2c_scl = 1'bz;
assign hi_muxsel = 1'b0;

//*************** wires declaration ***************************************
wire [30:0] ok1;
wire [16:0] ok2;
wire ti_clk;

//hi wires
wire [15:0] ep00wire;
wire [15:0] ep01wire;
wire [15:0] ep02wire;
wire [15:0] ep03wire;
wire [15:0] ep04wire;
wire [15:0] ep05wire;
wire [15:0] ep06wire;
wire [15:0] ep07wire;
wire [15:0] ep08wire;
wire [15:0] ep09wire;
wire [15:0] ep0Awire;
wire [15:0] ep0Bwire;
wire [15:0] ep0Cwire;

wire [15:0] ep20wire;
wire [15:0] ep21wire;
wire [15:0] ep22wire;
wire [15:0] ep23wire;
wire [15:0] ep24wire;
wire [15:0] ep25wire;
wire [15:0] ep26wire;
wire [15:0] ep40wire;
wire [15:0] ep41wire;
wire [15:0] ep42wire;
wire [15:0] ep43wire;
wire [15:0] ep44wire;
wire [15:0] ep60wire;

//dac
wire [1:0] hi_dac_reg;
wire [5:0] hi_dac_addr;
wire [13:0] hi_dac_db;
wire dac_start;
wire dac_reset1;

//adc
wire adc_reset, adc_sc0, adc_sc1, adc_sc2, adc_sc3, adc_sc4, adc_sc5, adc_sc6, adc_sc7, adc_sc_iout;
wire [15:0] adc_out_reg;
wire adc_done;
wire [15:0] adc_ioutp_val;
wire [15:0] adc_ioutm_val;
wire [2:0] adc_ioutp_range;
wire [2:0] adc_ioutm_range;
wire adc_iout_done;
wire [2:0] adc_range_sel;
wire [1:0] adc_iout_sel;

//on-chip idac
wire [7:0] nn_dac_range;
wire [23:0] nn_dac_val;
wire trig_range;
wire trig_val;
wire nn_dac_reset;
wire [23:0] so_reg;

//iout_sel
wire [30:0] iout_sel_word;
wire iout_sel_reset;		
wire iout_sel_trig;
wire [4:0] row_addr1;
wire [4:0] row_addr2;

//dpulse_control
wire dp_set_to_0;
wire dp_set_to_1;
wire dp_reset;
wire dp_start;
wire [15:0] dp_length;

/*********** Opal Kelly EndPoints Instantiation **********************/

//Host Interface
// Instantiate the okHost and connect endpoints.
wire [17*4-1:0]  ok2x;
okHost okHI(.hi_in(hi_in), .hi_out(hi_out), .hi_inout(hi_inout), .ti_clk(ti_clk),
	.ok1(ok1), .ok2(ok2));

okWireOR # (.N(4)) wireOR (ok2, ok2x);



// ********************* Wire IN ************************************
okWireIn ep00 (.ok1(ok1), .ep_addr(8'h00), .ep_dataout(ep00wire));
assign hi_dac_reg = ep00wire[1:0];
				
okWireIn ep01 (.ok1(ok1), .ep_addr(8'h01), .ep_dataout(ep01wire));
assign hi_dac_addr = ep01wire[5:0];
	
okWireIn ep02 (.ok1(ok1), .ep_addr(8'h02), .ep_dataout(ep02wire));
assign hi_dac_db = ep02wire[13:0];
	
okWireIn ep03 (.ok1(ok1), .ep_addr(8'h03), .ep_dataout(ep03wire)); // idac range
assign nn_dac_range = ep03wire[7:0];
	
okWireIn ep04 (.ok1(ok1), .ep_addr(8'h04), .ep_dataout(ep04wire)); // idac val1 (lower 12 bits)
assign nn_dac_val[11:0] = ep04wire[11:0];
	
okWireIn ep05 (.ok1(ok1), .ep_addr(8'h05), .ep_dataout(ep05wire)); // idac val2 (lower 12 bits)
assign nn_dac_val[23:12] = ep05wire[11:0];

okWireIn ep06 (.ok1(ok1), .ep_addr(8'h06), .ep_dataout(ep06wire)); // iout_sel_word lower 16 bits
assign iout_sel_word[15:0] = ep06wire;

okWireIn ep07 (.ok1(ok1), .ep_addr(8'h07), .ep_dataout(ep07wire)); // iout_sel_word upper 15 bits
assign iout_sel_word[30:16] = ep07wire[14:0];

okWireIn ep08 (.ok1(ok1), .ep_addr(8'h08), .ep_dataout(ep08wire)); // dpulse_control length
assign dp_length = ep08wire;

okWireIn ep09 (.ok1(ok1), .ep_addr(8'h09), .ep_dataout(ep09wire)); // nn chip address register
assign COL_ADDR[5:0] = ep09wire[5:0];
assign row_addr2[4:0] = ep09wire[10:6];

okWireIn ep0A (.ok1(ok1), .ep_addr(8'h0A), .ep_dataout(ep0Awire)); // nn chip array config
assign LOCK = ep0Awire[0];
assign MEAS = ep0Awire[1];
assign PROG = ep0Awire[2];
assign ADDR_EN = ep0Awire[3];
assign GSEL0 = ep0Awire[4];
assign DSEL[0] = ep0Awire[5];
assign DSEL[1] = ep0Awire[6];

okWireIn ep0B (.ok1(ok1), .ep_addr(8'h0B), .ep_dataout(ep0Bwire)); // idac control register
assign IMASTER_SEL = ep0Bwire[0];
assign IDAC_SEL[0] = ep0Bwire[1];
assign IDAC_SEL[1] = ep0Bwire[2];

okWireIn ep0C (.ok1(ok1), .ep_addr(8'h0C), .ep_dataout(ep0Cwire)); // itov control register
assign ITOV_SEL = ep0Cwire[0];
assign ITOV_INP_SEL[1:0] = ep0Cwire[2:1];
assign ITOV_RSEL[2:0] = ep0Cwire[5:3];

// ********************* Wire OUT ************************************
okWireOut ep20(.ok1(ok1), .ok2(ok2x[ 0*17 +: 17 ]), .ep_addr(8'h20), .ep_datain(ep20wire)); //ADC
assign ep20wire = adc_out_reg;

okWireOut ep21(.ok1(ok1), .ok2(ok2x[ 2*17 +: 17 ]), .ep_addr(8'h21), .ep_datain(ep21wire)); //On-chip IDAC val1
assign ep21wire[11:0] = so_reg[11:0];
assign ep21wire[15:12] = 4'h0;

okWireOut ep22(.ok1(ok1), .ok2(ok2x[ 3*17 +: 17 ]), .ep_addr(8'h22), .ep_datain(ep22wire)); //On-chip IDAC val2
assign ep22wire[11:0] = so_reg[23:12];
assign ep22wire[15:12] = 4'h0;

okWireOut ep23(.ok1(ok1), .ok2(ok2x[ 4*17 +: 17 ]), .ep_addr(8'h23), .ep_datain(ep23wire)); //ADC
assign ep23wire = adc_ioutp_val;

okWireOut ep24(.ok1(ok1), .ok2(ok2x[ 5*17 +: 17 ]), .ep_addr(8'h24), .ep_datain(ep24wire)); //ADC
assign ep24wire = adc_ioutm_val;

okWireOut ep25(.ok1(ok1), .ok2(ok2x[ 6*17 +: 17 ]), .ep_addr(8'h20), .ep_datain(ep25wire)); //ADC
assign ep25wire[2:0] = adc_ioutp_range;
assign ep25wire[15:3] = 0;

okWireOut ep26(.ok1(ok1), .ok2(ok2x[ 7*17 +: 17 ]), .ep_addr(8'h26), .ep_datain(ep26wire)); //ADC
assign ep26wire = adc_ioutm_range;
assign ep26wire[15:3] = 0;


// ********************* Trigger In ************************************
okTriggerIn ep40 (.ok1(ok1), .ep_addr(8'h40),.ep_clk(ti_clk), .ep_trigger(ep40wire)); //DAC
assign dac_start = ep40wire[0];
assign dac_reset1 = ep40wire[1];

okTriggerIn ep41 (.ok1(ok1), .ep_addr(8'h41),.ep_clk(ti_clk), .ep_trigger(ep41wire)); //ADC
assign adc_reset = ep41wire[0];
assign adc_sc0 = ep41wire[1];
assign adc_sc1 = ep41wire[2];
assign adc_sc2 = ep41wire[3];
assign adc_sc3 = ep41wire[4];
assign adc_sc4 = ep41wire[5];
assign adc_sc5 = ep41wire[6];
assign adc_sc6 = ep41wire[7];
assign adc_sc7 = ep41wire[8];
assign adc_sc_iout = ep41wire[9];

okTriggerIn ep42 (.ok1(ok1), .ep_addr(8'h42),.ep_clk(ti_clk), .ep_trigger(ep42wire)); //On-chip IDAC
assign nn_dac_reset = ep42wire[0];
assign trig_range = ep42wire[1];
assign trig_val = ep42wire[2];

okTriggerIn ep43 (.ok1(ok1), .ep_addr(8'h43),.ep_clk(ti_clk), .ep_trigger(ep43wire)); //iout_sel_control
assign iout_sel_reset = ep43wire[0];
assign iout_sel_trig = ep43wire[1];

okTriggerIn ep44 (.ok1(ok1), .ep_addr(8'h44),.ep_clk(ti_clk), .ep_trigger(ep44wire)); //dpulse
assign dp_reset = ep44wire[0];
assign dp_set_to_0 = ep44wire[1];
assign dp_set_to_1 = ep44wire[2];
assign dp_start = ep44wire[3];

// ********************* Trigger Out ************************************
okTriggerOut ep60 (.ok1(ok1), .ok2(ok2x[ 1*17 +: 17 ]), .ep_addr(8'h60), .ep_clk(ti_clk), .ep_trigger(ep60wire));
assign ep60wire[0] = adc_done;
assign ep60wire[1] = adc_iout_done;

/*
DAC5380_Interface dac1(.DAC_DB(DAC_DB),.DAC_REG(DAC_REG),.DAC_ADDR(DAC_ADDR),.DAC_WR(DAC_WR),.DAC_A_B(DAC_A_B),
								.DAC_LDAC(DAC_LDAC),.dac_clk(ti_clk),.dac_reset1(dac_reset1),.dac_start(dac_start),.hi_dac_reg(hi_dac_reg),
								.hi_dac_db(hi_dac_db),.hi_dac_addr(hi_dac_addr));
								

ADC7699_Interface adc1(.clk(ti_clk),.reset(adc_reset),.start_conv_0(adc_sc0),.start_conv_1(adc_sc1),.start_conv_2(adc_sc2),
							.start_conv_3(adc_sc3),.start_conv_4(adc_sc4),.start_conv_5(adc_sc5),.start_conv_6(adc_sc6),.start_conv_7(adc_sc7),
							.SDO(ADC_SDO),.out_reg(adc_out_reg),.SCLK(ADC_SCLK),.CNV(ADC_CNV),.DIN(ADC_DIN),.done(adc_done),.cycle1(ADC_CYCLE));


NN_IDAC_Interface nn_idac(.range(nn_dac_range),.val(nn_dac_val),.trig_range(trig_range),.trig_val(trig_val),.clk(ti_clk),.reset(nn_dac_reset),
								.so_reg(so_reg),.PORT_SEL(PORT_SEL),.CLK_RANGE(CLK_RANGE),.CLK_VAL(CLK_VAL),.OE(OE),.SCAN_IN(SCAN_IN),.SCAN_OUT(SCAN_OUT));

						
IOUT_SEL_Interface nn_sel(.iout_sel_word(iout_sel_word),.trig(iout_sel_trig),.clk(ti_clk),.reset(iout_sel_reset),.DATA_IN(DATA_IN),
									.ROW_ADDR(row_addr1),.LATCH_EN(LATCH_EN));
			

DPulse_ctrl dpulse_b(.clk(ti_clk),.set_to_0(dp_set_to_0),.set_to_1(dp_set_to_1),.reset(dp_reset),.start_pulse(dp_start),
							.dpulse_length(dp_length),.NN_DPULSE(DPULSE));

			
*/



ADC7699_auto adc_auto_inst(.clk(ti_clk),.reset(adc_reset),.start_conv_0(adc_sc0),.start_conv_1(adc_sc1),.start_conv_2(adc_sc2),
							.start_conv_3(adc_sc3),.start_conv_4(adc_sc4),.start_conv_5(adc_sc5),.start_conv_6(adc_sc6),.start_conv_7(adc_sc7),
							.start_conv_iout(adc_sc_iout),.ioutp_val(adc_ioutp_val),.ioutm_val(adc_ioutm_val),.ioutp_range(adc_ioutp_range),
							.ioutm_range(adc_ioutm_range),.iout_done(adc_iout_done),.range_sel(adc_range_sel),.iout_sel(adc_iout_sel),
							.SDO(ADC_SDO),.out_reg(adc_out_reg),.SCLK(ADC_SCLK),.CNV(ADC_CNV),.DIN(ADC_DIN),.done(adc_done));




assign ROW_ADDR = row_addr1[4:0] | row_addr2[4:0];

endmodule
