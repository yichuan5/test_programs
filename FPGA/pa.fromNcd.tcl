
# PlanAhead Launch Script for Post PAR Floorplanning, created by Project Navigator

create_project -name NNFG2_auto -dir "E:/NNFG2_auto_starting point/planAhead_run_2" -part xc3s1500fg320-4
set srcset [get_property srcset [current_run -impl]]
set_property design_mode GateLvl $srcset
set_property edif_top_file "E:/NNFG2_auto_starting point/top_auto.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/NNFG2_auto_starting point} {ipcore_dir} }
add_files [list {ipcore_dir/addrinbuf.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/datainbuf.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/dataoutfifo.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/dataoutfifo_bram.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/dataoutfifo_range.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/dataoutfifo_range_blk.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/dgaddrbuf.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/dynwaitram.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/idacandtime.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/idacvalbuf.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/readoutbuf.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/readoutbuf_rng.ncf}] -fileset [get_property constrset [current_run]]
set_property target_constrs_file "top_auto.ucf" [current_fileset -constrset]
add_files [list {top_auto.ucf}] -fileset [get_property constrset [current_run]]
link_design
read_xdl -file "E:/NNFG2_auto_starting point/top_auto.ncd"
if {[catch {read_twx -name results_1 -file "E:/NNFG2_auto_starting point/top_auto.twx"} eInfo]} {
   puts "WARNING: there was a problem importing \"E:/NNFG2_auto_starting point/top_auto.twx\": $eInfo"
}
