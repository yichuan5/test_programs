`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:35:56 12/13/2012
// Design Name:   Forward_Ctrl
// Module Name:   C:/school/FALL12/xilinx_projects/NNFG2_auto/Forward_Ctrl_tb.v
// Project Name:  NNFG2_auto
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Forward_Ctrl
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Forward_Ctrl_tb;

	// Inputs
	reg clk;
	reg reset;
	reg start_forward;
	reg [15:0] num_of_vect;
	reg [7:0] num_of_vin;
	reg [15:0] fwd_prop_delay;
	reg [13:0] datainbuf_din;
	reg datainbuf_wren;
	reg [5:0] addrinbuf_din;
	reg addrinbuf_wren;
	reg dataoutfifo_pv_rden;
	reg dataoutfifo_mv_rden;
	reg dataoutfifo_range_rden;
	reg [15:0] pv_din;
	reg [15:0] mv_din;
	reg [2:0] p_range;
	reg [2:0] m_range;
	reg adc_done;

	// Outputs
	wire [15:0] dataoutfifo_pv_dout;
	wire [15:0] dataoutfifo_mv_dout;
	wire [5:0] dataoutfifo_range_dout;
	wire forward_done;
	wire [15:0] cycles;
	wire dac_start;
	wire [13:0] dac_din;
	wire [5:0] dac_addr;
	wire [1:0] dac_reg;
	wire adc_start_conv;
	wire forward_enabled;

	// Instantiate the Unit Under Test (UUT)
	Forward_Ctrl uut (
		.clk(clk), 
		.reset(reset), 
		.start_forward(start_forward), 
		.num_of_vect(num_of_vect), 
		.num_of_vin(num_of_vin), 
		.fwd_prop_delay(fwd_prop_delay), 
		.datainbuf_din(datainbuf_din), 
		.datainbuf_wren(datainbuf_wren), 
		.addrinbuf_din(addrinbuf_din), 
		.addrinbuf_wren(addrinbuf_wren), 
		.dataoutfifo_pv_dout(dataoutfifo_pv_dout), 
		.dataoutfifo_pv_rden(dataoutfifo_pv_rden), 
		.dataoutfifo_mv_dout(dataoutfifo_mv_dout), 
		.dataoutfifo_mv_rden(dataoutfifo_mv_rden), 
		.dataoutfifo_range_dout(dataoutfifo_range_dout), 
		.dataoutfifo_range_rden(dataoutfifo_range_rden), 
		.forward_done(forward_done), 
		.cycles(cycles), 
		.dac_start(dac_start), 
		.dac_din(dac_din), 
		.dac_addr(dac_addr), 
		.dac_reg(dac_reg), 
		.adc_start_conv(adc_start_conv), 
		.pv_din(pv_din), 
		.mv_din(mv_din), 
		.p_range(p_range), 
		.m_range(m_range), 
		.adc_done(adc_done), 
		.forward_enabled(forward_enabled)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		start_forward = 0;
		num_of_vect = 3;
		num_of_vin = 2;
		fwd_prop_delay = 16'h0000;
		datainbuf_din = 0;
		datainbuf_wren = 0;
		addrinbuf_din = 0;
		addrinbuf_wren = 0;
		dataoutfifo_pv_rden = 0;
		dataoutfifo_mv_rden = 0;
		dataoutfifo_range_rden = 0;
		pv_din = 16'hA0A1;
		mv_din = 16'hB0B1;
		p_range = 3'b010;
		m_range = 3'b101;
		adc_done = 0;

		// Wait 100 ns for global reset to finish
		#100; reset = 1;
		#20; reset = 0;
		
		//write to datainbuf
		#100;
		datainbuf_din = 14'h11;
		datainbuf_wren = 1;
		#20; datainbuf_din = 14'h22;
		#20; datainbuf_din = 14'h33;
		#20; datainbuf_din = 14'h44;
		#20; datainbuf_din = 14'h55;
		#20; datainbuf_din = 14'h66;
		#20; datainbuf_wren = 0;
		datainbuf_din = 0;
		
		//write to addrinbuf
		#40;
		addrinbuf_din = 6'h1;
		addrinbuf_wren = 1;
		#20; addrinbuf_din = 6'h2;
		#20; addrinbuf_wren = 0;
		addrinbuf_din = 6'h0;
      
		//start forward
		#100;
		start_forward = 1;
		#20; start_forward = 0;
		
		//emulate adc_done
		#5000; adc_done = 1;
		#20; adc_done = 0;
		#5000; adc_done = 1;
		#20; adc_done = 0;
		#5000; adc_done = 1;
		#20; adc_done = 0;
		
		//emulate reading from fifo
		#1000; dataoutfifo_pv_rden = 1;
		#60; dataoutfifo_pv_rden = 0;
		
		#1000; dataoutfifo_mv_rden = 1;
		#60; dataoutfifo_mv_rden = 0;
		
		#1000; dataoutfifo_range_rden = 1;
		#60; dataoutfifo_range_rden = 0;
		
		
	end
	
	always #10 clk = ~clk;
      
endmodule

