`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:10:06 12/10/2012 
// Design Name: 
// Module Name:    ADC7699_cycle 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ADC7699_cycle(
 input clk,
 input reset,
 input start_conv,
 input wire [15:0] cfg,
 input SDO,	 // from ADC
 output wire [15:0] out_reg,
 output reg SCLK,
 output reg CNV,
 output reg DIN,
 output reg done
);

// state machine states
parameter s_ready = 					18'b000000000000000001;
parameter s_conv1 = 					18'b000000000000000010;
parameter s_conv2 = 					18'b000000000000000100;
parameter s_conv3	=	   			18'b000000000000001000;
parameter s_conv4 =					18'b000000000000010000;
parameter s_conv5 =					18'b000000000000100000;
parameter s_loop1 =					18'b000000000001000000;
parameter s_loop2 =					18'b000000000010000000;

reg [7:0] ind_wait; 
reg [3:0] index;
reg [4:0] iter;
reg [17:0] state;
reg [15:0] input_reg; //shift register
reg [15:0] channel_code;
reg sclk_en;

assign out_reg = input_reg;

// sclk 24 MHz
always @(posedge clk) begin
	if (sclk_en)
		SCLK <= ~SCLK; //division by 2
	else
		SCLK <= 0;
end

always @(posedge clk)
	if (reset) begin
		state <= s_ready;
		sclk_en <= 1'b0;
		done <= 1'b0;
		CNV <= 1'b0;
		DIN <= 1'b0;
		input_reg[15:0] <= 16'h0000;
	end
	
	else case (state)
	
// ------------	INITIAL STATE --------------------------
	s_ready: begin
	done <= 1'b0;
	sclk_en <= 1'b0;
	DIN <= 1'b0;
	CNV <= 1'b0;
	if (start_conv) begin
		state <= s_conv1;
		channel_code <= cfg;
	end
	else begin
		state <= s_ready;
	end	
	end 

// ------------	CONVERT --------------------------
	s_conv1: begin
	CNV <= 1'b1; //!!!
	done <= 1'b0;
	sclk_en <= 1'b0;
	DIN <= 1'b0;
	state <= s_conv2;
	end
	
	s_conv2: begin
	CNV <= 1'b0; //!!!
	done <= 1'b0;
	sclk_en <= 1'b0;
	DIN <= 1'b0;
	iter <= 5'hF;
	index <= 4'hF;
	state <= s_conv3;
	end
	
	s_conv3: begin
	CNV <= 1'b0; //!!!
	done <= 1'b0;
	sclk_en <= 1'b1; //!!!
	DIN <= channel_code[index];
	state <= s_loop1;
	end
	
// reading from SDO, writing to DIN configuration word	
	s_loop1: begin //rising SCK edge
	CNV <= 1'b0;
	done <= 1'b0;
	input_reg[index] <= SDO;
	iter <= iter - 1;
	index <= index - 1;
	state <= s_loop2;
	end
	
	s_loop2: begin //falling SCK edge
	CNV <= 1'b0;
	done <= 1'b0;
	if (iter != 0) begin
		DIN <= channel_code[index];
		state <= s_loop1;
	end
	else begin
		state <= s_conv4;
		sclk_en <= 1'b0;
	end
	end
	
	s_conv4: begin
	input_reg[index] <= SDO;
	CNV <= 1'b0;
	done <= 1'b0;
	sclk_en <= 1'b0;
	state <= s_conv5;
	ind_wait <= 8'h60;
	end
	
	
	s_conv5: begin
	CNV <= 1'b0;
	sclk_en <= 1'b0;
	if (ind_wait != 0) begin
		state <= s_conv5;
		ind_wait <= ind_wait - 1;
	end
	else begin
		done <= 1'b1;
		state <= s_ready;
	end
	end
	

endcase



endmodule
