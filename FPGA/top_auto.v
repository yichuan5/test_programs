`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:05:19 10/29/2012 
// Design Name: 
// Module Name:    top_test 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`default_nettype none

module top_auto(
   input  wire [7:0]  hi_in,
   output wire [1:0]  hi_out,
   inout  wire [15:0] hi_inout,
   
   output wire        i2c_sda,
   output wire        i2c_scl,
   output wire        hi_muxsel,

output wire	[7:0] led,
 
//	DAC controls

	 output [13:0] DAC_DB,
	 output [1:0] DAC_REG,
	 output [5:0] DAC_ADDR,
	 output DAC_WR,
	 output DAC_A_B, //dont care
	 output DAC_LDAC

//ADC	 
	 ///output ADC_SCLK,
	 ///output ADC_CNV,
	 ///output ADC_DIN,
	 ///input  ADC_SDO,

//NNFG Chip
	///output PORT_SEL,
	///output CLK_RANGE,
	///output CLK_VAL,
	///output OE,
	///output SCAN_IN,
	///input SCAN_OUT,
	
	//output DATA_IN,
	//output LATCH_EN,
	///output DPULSE,
	///output [3:0] ROW_ADDR,
	///output [4:0] COL_ADDR,
	///output LOCK,
	///output MEAS,
	///output PROG,
	///output ADDR_EN,
	///output GSEL0,
	///output [1:0] DSEL,
	///output IMASTER_SEL,
	///output IDAC_SEL,
	//output ITOV_SEL,
	///output [2:0] ITOV_SEL,
	///output [2:0] ITOV_RSEL,
	
	///output NN_SEL,
//Yichuan

	///output SREG_CLK,          
	///output REG_CLK,                 
	///output WRITE_REG,             
	///output RST_REG,                
	///input  SREG_OUT,          
	//output C_RST,             
	//output C_CLK
	///output SH1_C,
	///output SH2_C,
	///output SH3_C,
	///output [2:1] RSHCLK,

	///output [17:0] TB 
	
    );
	
	
assign i2c_sda = 1'bz;
assign i2c_scl = 1'bz;
assign hi_muxsel = 1'b0;

//*************** wires declaration ***************************************
wire [30:0] ok1;
wire [16:0] ok2;
wire ti_clk;

//hi wires
wire [15:0] ep00wire;
wire [15:0] ep01wire;
wire [15:0] ep02wire;
wire [15:0] ep03wire;
wire [15:0] ep04wire;
wire [15:0] ep05wire;
wire [15:0] ep06wire;
wire [15:0] ep07wire;
wire [15:0] ep08wire;
wire [15:0] ep09wire;
wire [15:0] ep0Awire;
wire [15:0] ep0Bwire;
wire [15:0] ep0Cwire;
wire [15:0] ep0Dwire;
wire [15:0] ep0Ewire;
wire [15:0] ep0Fwire;
wire [15:0] ep10wire;
wire [15:0] ep11wire;
wire [15:0] ep12wire;
wire [15:0] ep13wire;
wire [15:0] ep14wire;


wire [15:0] ep20wire;
wire [15:0] ep21wire;
wire [15:0] ep22wire;
wire [15:0] ep23wire;
wire [15:0] ep24wire;
wire [15:0] ep25wire;
wire [15:0] ep26wire;

wire [15:0] ep40wire;
wire [15:0] ep41wire;
wire [15:0] ep42wire;
wire [15:0] ep43wire;
wire [15:0] ep44wire;
wire [15:0] ep45wire;
wire [15:0] ep46wire;

wire [15:0] ep60wire;
wire [15:0] ep61wire;
wire [15:0] ep62wire;

wire [15:0] ep80wire;
wire [15:0] ep81wire;
wire [15:0] ep82wire;
wire [15:0] ep83wire;
wire [15:0] ep84wire;
wire [15:0] ep85wire;

wire [15:0] epA0wire;
wire [15:0] epA1wire;
wire [15:0] epA2wire;
wire [15:0] epA3wire;
wire [15:0] epA4wire;


//DAC INTERFACE
wire [1:0] hi_dac_reg;
//wire [1:0] dac_reg;
//wire [1:0] fwd_dac_reg;
wire [5:0] hi_dac_addr;
//wire [5:0] dac_addr;
//wire [5:0] fwd_dac_addr;
wire [13:0] hi_dac_db;
//wire [13:0] dac_db;
//wire [13:0] fwd_dac_db;
wire dac_start;
wire hi_dac_start;
wire dac_reset1;
//wire fwd_dac_start;


/*//IDAC INTERFACE
wire [7:0] hi_dac_range;
wire [23:0] nn_dac_val;
wire [23:0] hi_idac_val;
wire [23:0] dyn_idac_val;
wire hi_trig_range;
wire trig_val;
wire hi_trig_val;
wire dyn_trig_val;
wire hi_dac_reset;
wire [23:0] hi_so_reg;
wire [7:0] idac_clk_div_ratio;*/


/*//ADC_AUTO
wire adc_reset, adc_sc1, adc_sc2, adc_sc3, adc_sc4, adc_sc5, adc_sc6, adc_sc7, adc_sc8, adc_sc_iout, hi_adc_sc_iout, fwd_adc_sc_iout, adc_sc_single;
wire hi_adc_sc_single, dyb_adc_sc_single;
wire [15:0] adc_out_reg;
wire adc_done;
wire [15:0] adc_ioutp_val;
wire [15:0] adc_ioutm_val;
wire [2:0] adc_ioutp_range;
wire [2:0] adc_ioutm_range;
wire adc_iout_done;
wire [2:0] adc_range_sel;
wire [2:0] hi_range_sel;
wire [1:0] adc_iout_sel;
wire [1:0] hi_inp_sel;
wire lock_rladder;*/

/*//iout_sel
wire [30:0] iout_sel_word;
wire iout_sel_reset;		
wire iout_sel_trig;
wire [4:0] row_addr1;
wire [4:0] row_addr2;*/

/*//dpulse_control
wire dp_set_to_0;
wire dp_set_to_1;
wire dp_reset;
wire dp_start;
wire [15:0] dp_length_lo;
wire [7:0] dp_length_hi;*/

/*//FORWARD CONTROL
wire start_frw;
wire forward_done;
wire frw_reset;
wire forward_enable;
wire [15:0] num_of_vec;
wire [7:0] num_of_vin;
wire [15:0] fwd_prop_delay;
wire [13:0] fwd_dac_din;*/

/*//DYNAMIC INTERFACE
wire dyn_reset;
wire dyn_start_prog;
wire dyn_start_readout;
wire [10:0] num_of_loc;
wire [5:0] dyn_col_addr;
wire [4:0] dyn_row_addr;
wire [5:0] hi_col_addr;
wire [4:0] hi_row_addr;
//wire [15:0] dyn_prog_time;
wire [15:0] readout_time;
wire hi_addr_en;
wire dyn_addr_en;
wire auto_on;
wire prog_dyn_done;
wire readout_done;*/


/*********** Opal Kelly EndPoints Instantiation **********************/

//Host Interface
// Instantiate the okHost and connect endpoints.
wire [17*21-1:0]  ok2x;
okHost okHI(.hi_in(hi_in), .hi_out(hi_out), .hi_inout(hi_inout), .ti_clk(ti_clk),
	.ok1(ok1), .ok2(ok2));

okWireOR # (.N(21)) wireOR (ok2, ok2x);


//// ************************************************************************************
//// ********************* Wire IN ************************************
//// ************************************************************************************

okWireIn ep00 (.ok1(ok1), .ep_addr(8'h00), .ep_dataout(ep00wire));
assign hi_dac_reg = ep00wire[1:0];

/////okWireIn ep00 (.ok1(ok1), .ep_addr(8'h00), .ep_dataout(ep00wire));
/////assign C_RST = ep0Awire[0];
/////assign C_CLK = ep0Awire[1];

			
okWireIn ep01 (.ok1(ok1), .ep_addr(8'h01), .ep_dataout(ep01wire));
assign hi_dac_addr = ep01wire[5:0];

/////okWireIn ep01 (.ok1(ok1), .ep_addr(8'h01), .ep_dataout(ep01wire));
/////assign DAC_ADDR = ep01wire[5:0];
/////assign DAC_ADDR = 6'b111111;
/////assign led = {8{0}};
assign led = 8'b01001111;
/////assign DAC_ADDR = 6'b111111;

okWireIn ep02 (.ok1(ok1), .ep_addr(8'h02), .ep_dataout(ep02wire));
assign hi_dac_db = ep02wire[13:0];

//okWireIn ep03 (.ok1(ok1), .ep_addr(8'h03), .ep_dataout(ep03wire)); // idac range
//assign hi_dac_range = ep03wire[7:0];
//	
//okWireIn ep04 (.ok1(ok1), .ep_addr(8'h04), .ep_dataout(ep04wire)); // idac val1 (lower 12 bits)
//assign hi_idac_val[11:0] = ep04wire[11:0];
//	
//okWireIn ep05 (.ok1(ok1), .ep_addr(8'h05), .ep_dataout(ep05wire)); // idac val2 (lower 12 bits)
//assign hi_idac_val[23:12] = ep05wire[11:0];
//
//okWireIn ep06 (.ok1(ok1), .ep_addr(8'h06), .ep_dataout(ep06wire)); // iout_sel_word lower 16 bits
//assign iout_sel_word[15:0] = ep06wire;
//
//okWireIn ep07 (.ok1(ok1), .ep_addr(8'h07), .ep_dataout(ep07wire)); // iout_sel_word upper 15 bits
//assign iout_sel_word[30:16] = ep07wire[14:0];
//
//okWireIn ep08 (.ok1(ok1), .ep_addr(8'h08), .ep_dataout(ep08wire)); // dpulse_control length
//assign dp_length_lo = ep08wire;
//
//okWireIn ep09 (.ok1(ok1), .ep_addr(8'h09), .ep_dataout(ep09wire)); // nn chip address register
//assign hi_col_addr[5:0] = ep09wire[5:0];
//assign hi_row_addr[4:0] = ep09wire[10:6];
//
//okWireIn ep0A (.ok1(ok1), .ep_addr(8'h0A), .ep_dataout(ep0Awire)); // nn chip array config
//assign LOCK = ep0Awire[0];
//assign MEAS = ep0Awire[1];
//assign PROG = ep0Awire[2];
//assign hi_addr_en = ep0Awire[3];
//assign GSEL0 = ep0Awire[4];
//assign DSEL[0] = ep0Awire[5];
//assign DSEL[1] = ep0Awire[6];
//
//okWireIn ep0B (.ok1(ok1), .ep_addr(8'h0B), .ep_dataout(ep0Bwire)); // idac control register
//assign IMASTER_SEL = ep0Bwire[0];
//assign IDAC_SEL[0] = ep0Bwire[1];
//assign IDAC_SEL[1] = ep0Bwire[2];
//
//okWireIn ep0C (.ok1(ok1), .ep_addr(8'h0C), .ep_dataout(ep0Cwire)); // itov control register
//assign ITOV_SEL = ep0Cwire[0];
//assign hi_inp_sel[1:0] = ep0Cwire[2:1];
//assign hi_range_sel[2:0] = ep0Cwire[5:3];
//
//okWireIn ep0D (.ok1(ok1), .ep_addr(8'h0D), .ep_dataout(ep0Dwire)); // DYNAMIC MEMORY PROGRAM
//assign num_of_loc = ep0Dwire[10:0];
//
////okWireIn ep0E (.ok1(ok1), .ep_addr(8'h0E), .ep_dataout(ep0Ewire)); // DYNAMIC MEMORY PROGRAM
////assign dyn_prog_time = ep0Ewire;
//
//okWireIn ep0F (.ok1(ok1), .ep_addr(8'h0F), .ep_dataout(ep0Fwire)); // DYNAMIC MEMORY PROGRAM
//assign readout_time = ep0Fwire;
//
//okWireIn ep10 (.ok1(ok1), .ep_addr(8'h10), .ep_dataout(ep10wire)); // FORWARD CONTROL
//assign num_of_vec = ep10wire;
//
//okWireIn ep11 (.ok1(ok1), .ep_addr(8'h11), .ep_dataout(ep11wire)); // FORWARD CONTROL
//assign num_of_vin = ep11wire[7:0];
//
//okWireIn ep12 (.ok1(ok1), .ep_addr(8'h12), .ep_dataout(ep12wire)); // FORWARD CONTROL
//assign fwd_prop_delay = ep12wire;
//
//okWireIn ep13 (.ok1(ok1), .ep_addr(8'h13), .ep_dataout(ep13wire)); // DAC INTERFACE
//assign idac_clk_div_ratio = ep13wire[7:0];
//
//okWireIn ep14 (.ok1(ok1), .ep_addr(8'h14), .ep_dataout(ep14wire)); // dpulse_control length_hi
//assign dp_length_hi = ep14wire[7:0];

/*// ************************************************************************************
// ********************* Wire OUT ************************************
// ************************************************************************************

okWireOut ep20(.ok1(ok1), .ok2(ok2x[ 0*17 +: 17 ]), .ep_addr(8'h20), .ep_datain(ep20wire)); //ADC
assign ep20wire = adc_out_reg;

okWireOut ep21(.ok1(ok1), .ok2(ok2x[ 1*17 +: 17 ]), .ep_addr(8'h21), .ep_datain(ep21wire)); //On-chip IDAC val1
assign ep21wire[11:0] = hi_so_reg[11:0];
assign ep21wire[15:12] = 4'h0;

okWireOut ep22(.ok1(ok1), .ok2(ok2x[ 2*17 +: 17 ]), .ep_addr(8'h22), .ep_datain(ep22wire)); //On-chip IDAC val2
assign ep22wire[11:0] = hi_so_reg[23:12];
assign ep22wire[15:12] = 4'h0;

okWireOut ep23(.ok1(ok1), .ok2(ok2x[ 3*17 +: 17 ]), .ep_addr(8'h23), .ep_datain(ep23wire)); //ADC
assign ep23wire = adc_ioutp_val;

okWireOut ep24(.ok1(ok1), .ok2(ok2x[ 4*17 +: 17 ]), .ep_addr(8'h24), .ep_datain(ep24wire)); //ADC
assign ep24wire = adc_ioutm_val;

okWireOut ep25(.ok1(ok1), .ok2(ok2x[ 5*17 +: 17 ]), .ep_addr(8'h25), .ep_datain(ep25wire)); //ADC
assign ep25wire[2:0] = adc_ioutp_range;
assign ep25wire[15:3] = 0;

okWireOut ep26(.ok1(ok1), .ok2(ok2x[ 6*17 +: 17 ]), .ep_addr(8'h26), .ep_datain(ep26wire)); //ADC
assign ep26wire[2:0] = adc_ioutm_range;
assign ep26wire[15:3] = 0;
*/
// ************************************************************************************
// ********************* Trigger In ************************************
// ************************************************************************************

okTriggerIn ep40 (.ok1(ok1), .ep_addr(8'h40),.ep_clk(ti_clk), .ep_trigger(ep40wire)); //DAC
assign hi_dac_start = ep40wire[0];
assign dac_reset1 = ep40wire[1];
/*
okTriggerIn ep41 (.ok1(ok1), .ep_addr(8'h41),.ep_clk(ti_clk), .ep_trigger(ep41wire)); //ADC
assign adc_reset = ep41wire[0];
assign adc_sc1 = ep41wire[1];
assign adc_sc2 = ep41wire[2];
assign adc_sc3 = ep41wire[3];
assign adc_sc4 = ep41wire[4];
assign adc_sc5 = ep41wire[5];
assign adc_sc6 = ep41wire[6];
assign adc_sc7 = ep41wire[7];
assign adc_sc8 = ep41wire[8];
assign hi_adc_sc_iout = ep41wire[9];
assign hi_adc_sc_single = ep41wire[10];

okTriggerIn ep42 (.ok1(ok1), .ep_addr(8'h42),.ep_clk(ti_clk), .ep_trigger(ep42wire)); //On-chip IDAC
assign hi_dac_reset = ep42wire[0];
assign hi_trig_range = ep42wire[1];
assign hi_trig_val = ep42wire[2];

okTriggerIn ep43 (.ok1(ok1), .ep_addr(8'h43),.ep_clk(ti_clk), .ep_trigger(ep43wire)); //iout_sel_control
assign iout_sel_reset = ep43wire[0];
assign iout_sel_trig = ep43wire[1];

okTriggerIn ep44 (.ok1(ok1), .ep_addr(8'h44),.ep_clk(ti_clk), .ep_trigger(ep44wire)); //dpulse
assign dp_reset = ep44wire[0];
assign dp_set_to_0 = ep44wire[1];
assign dp_set_to_1 = ep44wire[2];
assign dp_start = ep44wire[3];

okTriggerIn ep45 (.ok1(ok1), .ep_addr(8'h45),.ep_clk(ti_clk), .ep_trigger(ep45wire)); //DYNAMIC MEMORY PROGRAM
assign dyn_reset = ep45wire[0];
assign dyn_start_prog = ep45wire[1];
assign dyn_start_readout = ep45wire[2];

okTriggerIn ep46 (.ok1(ok1), .ep_addr(8'h46),.ep_clk(ti_clk), .ep_trigger(ep46wire)); //FORWARD
assign frw_reset = ep46wire[0];
assign start_frw = ep46wire[1];*/

/*// ************************************************************************************
// ********************* Trigger Out **************************************************
// ************************************************************************************

okTriggerOut ep60 (.ok1(ok1), .ok2(ok2x[ 7*17 +: 17 ]), .ep_addr(8'h60), .ep_clk(ti_clk), .ep_trigger(ep60wire));
assign ep60wire[0] = adc_done;
assign ep60wire[1] = adc_iout_done;

//FORWARD
okTriggerOut ep61 (.ok1(ok1), .ok2(ok2x[ 8*17 +: 17 ]), .ep_addr(8'h61), .ep_clk(ti_clk), .ep_trigger(ep61wire));
assign ep61wire[0] = forward_done;

//DYNAMIC_PROG
okTriggerOut ep62 (.ok1(ok1), .ok2(ok2x[ 9*17 +: 17 ]), .ep_addr(8'h62), .ep_clk(ti_clk), .ep_trigger(ep62wire));
assign ep62wire[0] = prog_dyn_done;
assign ep62wire[1] = readout_done;*/


/*// ************************************************************************************
// ********************* Pipe In ***************************************************
// ************************************************************************************
//FORWARD DATAINBUF
wire datainbuf_wren;
wire [15:0] datainbuf_din;
okPipeIn     ep80 (.ok1(ok1), .ok2(ok2x[ 10*17 +: 17 ]), .ep_addr(8'h80), .ep_write(datainbuf_wren), .ep_dataout(datainbuf_din));

//FORWARD ADDRINBUF
wire addrinbuf_wren;
wire [15:0] addrinbuf_din;
okPipeIn     ep81 (.ok1(ok1), .ok2(ok2x[ 11*17 +: 17 ]), .ep_addr(8'h81), .ep_write(addrinbuf_wren), .ep_dataout(addrinbuf_din));

//DYNAMIC PROG FGADDRBUF
wire fgaddrbuf_wren;
wire [15:0] fgaddrbuf_din;
okPipeIn     ep82 (.ok1(ok1), .ok2(ok2x[ 12*17 +: 17 ]), .ep_addr(8'h82), .ep_write(fgaddrbuf_wren), .ep_dataout(fgaddrbuf_din));

//DYNAMIC PROG IDACVALBUF_LO
wire idacvalbuf_lo_wren;
wire [15:0] idacvalbuf_lo_din;
okPipeIn     ep83 (.ok1(ok1), .ok2(ok2x[ 13*17 +: 17 ]), .ep_addr(8'h83), .ep_write(idacvalbuf_lo_wren), .ep_dataout(idacvalbuf_lo_din));

//DYNAMIC PROG IDACVALBUF_HI
wire idacvalbuf_hi_wren;
wire [15:0] idacvalbuf_hi_din;
okPipeIn     ep84 (.ok1(ok1), .ok2(ok2x[ 14*17 +: 17 ]), .ep_addr(8'h84), .ep_write(idacvalbuf_hi_wren), .ep_dataout(idacvalbuf_hi_din));

//DYNAMIC PROG DYNWAITRAM
wire dynwaitram_wren;
wire [15:0] dynwaitram_din;
okPipeIn     ep85 (.ok1(ok1), .ok2(ok2x[ 20*17 +: 17 ]), .ep_addr(8'h85), .ep_write(dynwaitram_wren), .ep_dataout(dynwaitram_din));*/


/*// ************************************************************************************
// ********************* Pipe Out ***************************************************
// ************************************************************************************
//FORWARD DATAOUT PVALUE
wire [15:0] dataoutfifo_pv_dout;
wire dataoutfifo_pv_rden;
okPipeOut    epA0 (.ok1(ok1), .ok2(ok2x[ 15*17 +: 17 ]), .ep_addr(8'ha0), .ep_read(dataoutfifo_pv_rden), .ep_datain(dataoutfifo_pv_dout));

//FORWARD DATAOUT MVALUE
wire [15:0] dataoutfifo_mv_dout;
wire dataoutfifo_mv_rden;
okPipeOut    epA1 (.ok1(ok1), .ok2(ok2x[ 16*17 +: 17 ]), .ep_addr(8'ha1), .ep_read(dataoutfifo_mv_rden), .ep_datain(dataoutfifo_mv_dout));

//FORWARD DATAOUT RANGE
wire [15:0] dataoutfifo_range_dout;
assign dataoutfifo_range_dout[15:6] = 0;
wire dataoutfifo_range_rden;
okPipeOut    epA2 (.ok1(ok1), .ok2(ok2x[ 17*17 +: 17 ]), .ep_addr(8'ha2), .ep_read(dataoutfifo_range_rden), .ep_datain(dataoutfifo_range_dout));

//DYNAMIC PROG READOUT_VAL
wire [15:0] readoutbuf_dout;
wire readoutbuf_rden;
okPipeOut    epA3 (.ok1(ok1), .ok2(ok2x[ 18*17 +: 17 ]), .ep_addr(8'ha3), .ep_read(readoutbuf_rden), .ep_datain(readoutbuf_dout));

//DYNAMIC PROG READOUT_RANGE
wire [15:0] readoutbuf_rng_dout;
assign readoutbuf_rng_dout[15:3] = 0;
wire readoutbuf_rng_rden;
okPipeOut    epA4 (.ok1(ok1), .ok2(ok2x[ 19*17 +: 17 ]), .ep_addr(8'ha4), .ep_read(readoutbuf_rng_rden), .ep_datain(readoutbuf_rng_dout));
*/




//************* DAC INTERFACE ***********************************************************************************************
/////assign DAC_ADDR = hi_dac_addr;
/////assign DAC_DB = hi_dac_db;
/////assign DAC_REG = hi_dac_reg;
//assign dac_start = hi_dac_start | fwd_dac_start;
assign dac_start = hi_dac_start;
//assign dac_reg = (forward_enable) ? fwd_dac_reg : hi_dac_reg;
//assign dac_db = (forward_enable) ? fwd_dac_din : hi_dac_db;
//assign dac_addr = (forward_enable) ? fwd_dac_addr : hi_dac_addr;

DAC5380_Interface dac1(.DAC_DB(DAC_DB),.DAC_REG(DAC_REG),.DAC_ADDR(DAC_ADDR),.DAC_WR(DAC_WR),.DAC_A_B(DAC_A_B),
								.DAC_LDAC(DAC_LDAC),.dac_clk(ti_clk),.dac_reset1(dac_reset1),.dac_start(dac_start),.hi_dac_reg(hi_dac_reg),
								.hi_dac_db(hi_dac_db),.hi_dac_addr(hi_dac_addr));


//DAC5380_Interface dac1(.DAC_DB(DAC_DB),.DAC_REG(DAC_REG),.DAC_ADDR(DAC_ADDR),.DAC_WR(DAC_WR),.DAC_A_B(DAC_A_B),
								//.DAC_LDAC(DAC_LDAC),.dac_clk(ti_clk),.dac_reset1(dac_reset1),.dac_start(dac_start),.hi_dac_reg(dac_reg),
								//.hi_dac_db(dac_db),.hi_dac_addr(dac_addr));

/*								
//************* IDAC INTERFACE ***********************************************************************************************
assign nn_dac_val = (auto_on) ? dyn_idac_val : hi_idac_val;
assign trig_val = (auto_on) ? dyn_trig_val : hi_trig_val;
NN_IDAC_Interface nn_idac(.range(hi_dac_range),.val(nn_dac_val),.trig_range(hi_trig_range),.trig_val(trig_val),.clk(ti_clk),.reset(hi_dac_reset),
								.so_reg(hi_so_reg),.PORT_SEL(PORT_SEL),.CLK_RANGE(CLK_RANGE),.CLK_VAL(CLK_VAL),.OE(OE),.SCAN_IN(SCAN_IN),.SCAN_OUT(SCAN_OUT),
								.clk_div_ratio(idac_clk_div_ratio));


//************* Config Neural Array INTERFACE ***********************************************************************************************						
IOUT_SEL_Interface nn_sel(.iout_sel_word(iout_sel_word),.trig(iout_sel_trig),.clk(ti_clk),.reset(iout_sel_reset),.DATA_IN(DATA_IN),
									.ROW_ADDR(row_addr1),.LATCH_EN(LATCH_EN));
			


//************* DPULSE INTERFACE ***********************************************************************************************
DPulse_ctrl dpulse_b(.clk(ti_clk),.set_to_0(dp_set_to_0),.set_to_1(dp_set_to_1),.reset(dp_reset),.start_pulse(dp_start),
							.dpulse_lo(dp_length_lo),.dpulse_hi(dp_length_hi),.NN_DPULSE(DPULSE));

			
//************* ADC_AUTO INTERFACE ***********************************************************************************************
assign ITOV_RSEL = (forward_enable | auto_on | lock_rladder) ? adc_range_sel : hi_range_sel;
assign ITOV_INP_SEL = (forward_enable | auto_on) ? adc_iout_sel : hi_inp_sel;
assign adc_sc_iout = hi_adc_sc_iout | fwd_adc_sc_iout;
assign adc_sc_single = hi_adc_sc_single | dyb_adc_sc_single;

ADC7699_auto adc_auto_inst(.clk(ti_clk),.reset(adc_reset),.start_conv_1(adc_sc1),.start_conv_2(adc_sc2),.start_conv_3(adc_sc3),
							.start_conv_4(adc_sc4),.start_conv_5(adc_sc5),.start_conv_6(adc_sc6),.start_conv_7(adc_sc7),.start_conv_8(adc_sc8),
							.start_conv_iout(adc_sc_iout),.ioutp_val(adc_ioutp_val),.ioutm_val(adc_ioutm_val),.ioutp_range(adc_ioutp_range),
							.ioutm_range(adc_ioutm_range),.iout_done(adc_iout_done),.range_sel(adc_range_sel),.iout_sel(adc_iout_sel),
							.SDO(ADC_SDO),.out_reg(adc_out_reg),.SCLK(ADC_SCLK),.CNV(ADC_CNV),.DIN(ADC_DIN),.done(adc_done),
							.start_conv_single(adc_sc_single),.lock_rladder(lock_rladder));

//************* FORWARD CONTROL INTERFACE ***********************************************************************************************
Forward_Ctrl Forward_Ctrl_inst(.clk(ti_clk),.reset(frw_reset),.start_forward(start_frw),.num_of_vect(num_of_vec),.num_of_vin(num_of_vin),.fwd_prop_delay(fwd_prop_delay),
				.datainbuf_din(datainbuf_din),.datainbuf_wren(datainbuf_wren),.addrinbuf_din(addrinbuf_din[5:0]),.addrinbuf_wren(addrinbuf_wren),
				.dataoutfifo_pv_dout(dataoutfifo_pv_dout),.dataoutfifo_pv_rden(dataoutfifo_pv_rden),
				.dataoutfifo_mv_dout(dataoutfifo_mv_dout),.dataoutfifo_mv_rden(dataoutfifo_mv_rden),
				.dataoutfifo_range_dout(dataoutfifo_range_dout[5:0]),.dataoutfifo_range_rden(dataoutfifo_range_rden),
				.forward_done(forward_done),.cycles(),.dac_start(fwd_dac_start),.dac_din(fwd_dac_din),.dac_addr(fwd_dac_addr),
				.dac_reg(fwd_dac_reg),.adc_start_conv(fwd_adc_sc_iout),.pv_din(adc_ioutp_val),.mv_din(adc_ioutm_val),
				.p_range(adc_ioutp_range),.m_range(adc_ioutm_range),.adc_done(adc_iout_done),.forward_enabled(forward_enable));

//************* DYNAMIC INTERFACE ***********************************************************************************************
assign row_addr2 = (auto_on) ? dyn_row_addr : hi_row_addr;
assign ROW_ADDR = row_addr2 | row_addr1;
assign COL_ADDR = (auto_on) ? dyn_col_addr : hi_col_addr;
assign ADDR_EN = (auto_on) ? dyn_addr_en : hi_addr_en;

prog_dyn_mem prog_dyn_mem_inst(.clk(ti_clk),.reset(dyn_reset),.start_prog(dyn_start_prog),.start_readout(dyn_start_readout),.num_of_loc(num_of_loc),
					.dyn_prog_time(),.readout_time(readout_time),
					.fgaddrbuf_wren(fgaddrbuf_wren),.fgaddrbuf_din(fgaddrbuf_din),
					.idacvalbuf_lo_wren(idacvalbuf_lo_wren),.idacvalbuf_hi_wren(idacvalbuf_hi_wren),
					.idacvalbuf_lo_din(idacvalbuf_lo_din),.idacvalbuf_hi_din(idacvalbuf_hi_din[11:0]),
					.readoutbuf_rden(readoutbuf_rden),.readoutbuf_dout(readoutbuf_dout),
					.readoutbuf_rng_rden(readoutbuf_rng_rden),.readoutbuf_rng_dout(readoutbuf_rng_dout[2:0]),
					.idac_val(dyn_idac_val),.idac_start_conv(dyn_trig_val),
					.row(dyn_row_addr),.col(dyn_col_addr),.addr_en(dyn_addr_en),
					.start_conv_single(dyb_adc_sc_single),.conv_done(adc_iout_done),.ioutp_val(adc_ioutp_val),.ioutp_range(adc_ioutp_range),
					.auto_on(auto_on),.prog_dyn_done(prog_dyn_done),.readout_done(readout_done),
					.dynwaitram_wren(dynwaitram_wren),.dynwaitram_din(dynwaitram_din));
*/

endmodule 

