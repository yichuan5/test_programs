`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:22:10 11/01/2012 
// Design Name: 
// Module Name:    NN_IDAC_Interface 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module NN_IDAC_Interface(
// from FPGA
	input [7:0] range,
	input [23:0] val, // val1<11:0>, val2<11:0> combined in one word
	input [7:0] clk_div_ratio,
	input trig_range,
	input trig_val,
	input clk,
	input reset,
	output reg [23:0] so_reg, //val1/2 or range depending on context
	
// to NNFG chip
	output reg PORT_SEL,
	output reg CLK_RANGE,
	output reg CLK_VAL,
	output reg OE,
	output reg SCAN_IN,
	input SCAN_OUT	
    );

// state machine states
parameter s_ready = 						18'b000000000000000001;
parameter s_range1 = 					18'b000000000000000010;
parameter s_range2 = 					18'b000000000000000100;
parameter s_range3 =	   				18'b000000000000001000;
parameter s_range4 =						18'b000000000000010000;
parameter s_val1 =						18'b000000000000100000;
parameter s_val2 =						18'b000000000001000000;
parameter s_val3 =						18'b000000000010000000;
parameter s_val4 =						18'b000000000100000000;
parameter s_val5 =						18'b000000001000000000;


reg [7:0] wait_time;
reg [4:0] index;
reg [17:0] state;


always @(posedge clk) begin
	if (reset) begin
		state <= s_ready;
		PORT_SEL <= 1'b0;
		CLK_RANGE <= 1'b0;
		CLK_VAL <= 1'b0;
		OE <= 1'b0;
		SCAN_IN <= 1'b0;
		index <= 4'h0;
		so_reg <= 24'h000000;
	end
	
	else case (state)
	s_ready: begin
		PORT_SEL <= 1'b0;
		CLK_RANGE <= 1'b0;
		CLK_VAL <= 1'b0;
		OE <= 1'b0;
		SCAN_IN <= 1'b0;
		index <= 4'h0;
		if (trig_range) begin
			state <= s_range1;
		end
		else if (trig_val) begin
			state <= s_val1;
		end
		else begin
			state <= s_ready;
		end
	end
	
//********* write into range register ******************	
	s_range1: begin
		PORT_SEL <= 1'b1;
		CLK_RANGE <= 1'b0;
		CLK_VAL <= 1'b0;
		OE <= 1'b0;
		SCAN_IN <= range[index];
		wait_time <= clk_div_ratio;
		state <= s_range2;
	end

	s_range2: begin
		PORT_SEL <= 1'b1;
		CLK_RANGE <= 1'b0;
		CLK_VAL <= 1'b0;
		OE <= 1'b0;
		if (wait_time != 0) begin
			wait_time <= wait_time - 1;
			state <= s_range2;
		end
		else begin
			state <= s_range3;
			wait_time <= clk_div_ratio;
			so_reg[index] <= SCAN_OUT;
		end
	end
	
	s_range3: begin
		PORT_SEL <= 1'b1;
		CLK_RANGE <= 1'b1;
		CLK_VAL <= 1'b0;
		OE <= 1'b0;
		if (wait_time != 0) begin
			wait_time <= wait_time - 1;
			state <= s_range3;
		end
		else begin
			state <= s_range4;
		end
	end
	
	s_range4: begin
		PORT_SEL <= 1'b1;
		CLK_RANGE <= 1'b1;
		CLK_VAL <= 1'b0;
		OE <= 1'b0;
		if (index == 5'h7) begin
			state <= s_ready;
		end
		else begin
			state <= s_range1;
			index <= index + 1;
		end
	end
		
//********* write into val register ******************	
	s_val1: begin
		PORT_SEL <= 1'b0;
		CLK_RANGE <= 1'b0;
		CLK_VAL <= 1'b0;
		OE <= 1'b0;
		SCAN_IN <= val[index];
		wait_time <= clk_div_ratio;
		state <= s_val2;
	end

	s_val2: begin
		PORT_SEL <= 1'b0;
		CLK_RANGE <= 1'b0;
		CLK_VAL <= 1'b0;
		OE <= 1'b0;
		if (wait_time != 0) begin
			wait_time <= wait_time - 1;
			state <= s_val2;
		end
		else begin
			state <= s_val3;
			wait_time <= clk_div_ratio;
			so_reg[index] <= SCAN_OUT;
		end
	end
	
	s_val3: begin
		PORT_SEL <= 1'b0;
		CLK_RANGE <= 1'b0;
		CLK_VAL <= 1'b1;
		OE <= 1'b0;
		if (wait_time != 0) begin
			wait_time <= wait_time - 1;
			state <= s_val3;
		end
		else begin
			state <= s_val4;
		end
	end
	
	s_val4: begin
		PORT_SEL <= 1'b0;
		CLK_RANGE <= 1'b0;
		CLK_VAL <= 1'b1;
		if (index == 5'h17) begin
			state <= s_val5;
			wait_time <= clk_div_ratio;
			OE <= 1'b1;
		end
		else begin
			state <= s_val1;
			index <= index + 1;
			OE <= 1'b0;
		end
	end
	
	s_val5: begin
		PORT_SEL <= 1'b0;
		CLK_RANGE <= 1'b0;
		CLK_VAL <= 1'b0;
		OE <= 1'b1;
		if (wait_time != 0) begin
			wait_time <= wait_time - 1;
			state <= s_val5;
		end
		else begin
			state <= s_ready;
		end
	end	
	
	
	endcase	
end		


endmodule
