`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:21:17 08/27/2018
// Design Name:   DAC5380_Interface
// Module Name:   E:/NNFG2_auto_starting point/test.v
// Project Name:  NNFG2_auto
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: DAC5380_Interface
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test;

	// Inputs
	reg dac_clk;
	reg dac_reset1;
	reg dac_start;
	reg [1:0] hi_dac_reg;
	reg [13:0] hi_dac_db;
	reg [5:0] hi_dac_addr;

	// Outputs
	wire [13:0] DAC_DB;
	wire [1:0] DAC_REG;
	wire [5:0] DAC_ADDR;
	wire DAC_WR;
	wire DAC_A_B;
	wire DAC_LDAC;

	// Instantiate the Unit Under Test (UUT)
	DAC5380_Interface uut (
		.DAC_DB(DAC_DB), 
		.DAC_REG(DAC_REG), 
		.DAC_ADDR(DAC_ADDR), 
		.DAC_WR(DAC_WR), 
		.DAC_A_B(DAC_A_B), 
		.DAC_LDAC(DAC_LDAC), 
		.dac_clk(dac_clk), 
		.dac_reset1(dac_reset1), 
		.dac_start(dac_start), 
		.hi_dac_reg(hi_dac_reg), 
		.hi_dac_db(hi_dac_db), 
		.hi_dac_addr(hi_dac_addr)
	);

	initial begin
		// Initialize Inputs
		dac_clk = 0;
		dac_reset1 = 0;
		dac_start = 0;
		hi_dac_reg = 0;
		hi_dac_db = 0;
		hi_dac_addr = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

