`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:00:14 12/13/2012
// Design Name:   prog_dyn_mem
// Module Name:   C:/school/FALL12/xilinx_projects/NNFG2_auto/prog_dyn_mem_tb.v
// Project Name:  NNFG2_auto
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: prog_dyn_mem
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module prog_dyn_mem_tb;

	// Inputs
	reg clk;
	reg reset;
	reg start_readout;
	reg start_prog;
	reg [10:0] num_of_loc;
	reg [15:0] dyn_prog_time;
	reg [15:0] readout_time;
	reg fgaddrbuf_wren;
	reg [10:0] fgaddrbuf_din;
	reg idacvalbuf_lo_wren;
	reg idacvalbuf_hi_wren;
	reg [11:0] idacvalbuf_lo_din;
	reg [11:0] idacvalbuf_hi_din;
	reg readoutbuf_rden;
	reg readoutbuf_rng_rden;
	reg conv_done;
	reg [15:0] ioutp_val;
	reg [2:0] ioutp_range;

	// Outputs
	wire [23:0] idac_val;
	wire idac_start_conv;
	wire [4:0] row;
	wire [5:0] col;
	wire addr_en;
	wire auto_on;
	wire prog_dyn_done;
	wire [15:0] readoutbuf_dout;
	wire [15:0] readoutbuf_rng_dout;
	wire start_conv_single;
	wire readout_done;

	// Instantiate the Unit Under Test (UUT)
	prog_dyn_mem uut (
		.clk(clk), 
		.reset(reset), 
		.start_prog(start_prog),
		.start_readout(start_readout),
		.readout_done(readout_done),
		.readout_time(readout_time),
		.ioutp_range(ioutp_range),
		.num_of_loc(num_of_loc), 
		.conv_done(conv_done),
		.start_conv_single(start_conv_single),
		.readoutbuf_rden(readoutbuf_rden),
		.dyn_prog_time(dyn_prog_time), 
		.fgaddrbuf_wren(fgaddrbuf_wren), 
		.readoutbuf_dout(readoutbuf_dout),
		.readoutbuf_rng_dout(readoutbuf_rng_dout),
		.fgaddrbuf_din(fgaddrbuf_din), 
		.readoutbuf_rng_rden(readoutbuf_rng_rden),
		.idacvalbuf_lo_wren(idacvalbuf_lo_wren), 
		.idacvalbuf_hi_wren(idacvalbuf_hi_wren), 
		.idacvalbuf_lo_din(idacvalbuf_lo_din), 
		.idacvalbuf_hi_din(idacvalbuf_hi_din), 
		.idac_val(idac_val), 
		.idac_start_conv(idac_start_conv), 
		.row(row), 
		.col(col), 
		.addr_en(addr_en), 
		.ioutp_val(ioutp_val),
		.auto_on(auto_on), 
		.prog_dyn_done(prog_dyn_done)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		start_prog = 0;
		start_readout = 0;
		num_of_loc = 4;
		dyn_prog_time = 1;
		readout_time = 1;
		fgaddrbuf_wren = 0;
		fgaddrbuf_din = 0;
		idacvalbuf_lo_wren = 0;
		idacvalbuf_hi_wren = 0;
		idacvalbuf_lo_din = 0;
		idacvalbuf_hi_din = 0;
		readoutbuf_rden = 0;
		readoutbuf_rng_rden = 0;
		conv_done = 0;
		ioutp_val = 16'h4321;
		ioutp_range = 3'b111;

		// Wait 100 ns for global reset to finish
		#100; reset = 1;
		#20; reset = 0;
		
		//write to <row,col> locations to fgaddrbuf
		#100; fgaddrbuf_wren = 1;
		fgaddrbuf_din = 11'b00000100001;
		#20; fgaddrbuf_din = 11'b00001100011;
		#20; fgaddrbuf_din = 11'b00011100111;
		#20; fgaddrbuf_din = 11'b00111101111;
		#20; fgaddrbuf_wren = 0;
		
		//write idac_val_lo to idacvalbuf_lo
		#100; idacvalbuf_lo_wren = 1;
		idacvalbuf_lo_din = 12'hAAA;
		#20; idacvalbuf_lo_din = 12'hBBB;
		#20; idacvalbuf_lo_din = 12'hCCC;
		#20; idacvalbuf_lo_din = 12'hDDD;
		#20; idacvalbuf_lo_wren = 0;
		
		//write idac_val_hi to idacvalbuf_hi
		#100; idacvalbuf_hi_wren = 1;
		idacvalbuf_hi_din = 12'hAAA;
		#20; idacvalbuf_hi_din = 12'hBBB;
		#20; idacvalbuf_hi_din = 12'hCCC;
		#20; idacvalbuf_hi_din = 12'hDDD;
		#20; idacvalbuf_hi_wren = 0;
		
		//start dynamic memory write
		/*
		#100; start_prog = 1;
		#20; start_prog = 0;
		*/
		
		//start dynamic memory read
		#100; start_readout = 1;
		#20; start_readout = 0;		
		#6000; conv_done = 1; ioutp_val = 16'h1111; ioutp_range = 3'b001;
		#20; conv_done = 0;
		#6000; conv_done = 1; ioutp_val = 16'h2222; ioutp_range = 3'b010;
		#20; conv_done = 0;
		#6000; conv_done = 1; ioutp_val = 16'h3333; ioutp_range = 3'b100;
		#20; conv_done = 0;
		#6000; conv_done = 1; ioutp_val = 16'h4444; ioutp_range = 3'b111;
		#20; conv_done = 0;
		
		#200; readoutbuf_rng_rden = 1; readoutbuf_rden = 1;
		#80; readoutbuf_rng_rden = 0; readoutbuf_rden = 0;

	end
	
	always #10 clk = ~clk;
      
endmodule

