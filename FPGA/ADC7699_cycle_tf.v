`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:02:55 03/03/2013
// Design Name:   ADC7699_cycle
// Module Name:   C:/school/FALL12/xilinx_projects/NNFG2_auto/ADC7699_cycle_tf.v
// Project Name:  NNFG2_auto
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ADC7699_cycle
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ADC7699_cycle_tf;

	// Inputs
	reg clk;
	reg reset;
	reg start_conv;
	reg [15:0] cfg;
	reg SDO;

	// Outputs
	wire [15:0] out_reg;
	wire SCLK;
	wire CNV;
	wire DIN;
	wire done;

	// Instantiate the Unit Under Test (UUT)
	ADC7699_cycle uut (
		.clk(clk), 
		.reset(reset), 
		.start_conv(start_conv), 
		.cfg(cfg), 
		.SDO(SDO), 
		.out_reg(out_reg), 
		.SCLK(SCLK), 
		.CNV(CNV), 
		.DIN(DIN), 
		.done(done)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		start_conv = 0;
		cfg = 0;
		SDO = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

