`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:32:48 12/10/2012
// Design Name:   ADC7699_auto
// Module Name:   C:/school/FALL12/xilinx_projects/NNFG2_auto/ADC7699_auto_tb.v
// Project Name:  NNFG2_auto
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ADC7699_auto
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ADC7699_auto_tb;

	// Inputs
	reg clk;
	reg reset;
	reg start_conv_1;
	reg start_conv_2;
	reg start_conv_3;
	reg start_conv_4;
	reg start_conv_5;
	reg start_conv_6;
	reg start_conv_7;
	reg start_conv_8;
	reg start_conv_iout;
	reg start_conv_single;
	reg SDO;

	// Outputs
	wire [15:0] out_reg;
	wire [15:0] ioutp_val;
	wire [15:0] ioutm_val;
	wire done;
	wire iout_done;
	wire SCLK;
	wire CNV;
	wire DIN;
	wire [2:0] range_sel;
	wire [1:0] iout_sel;
	wire [2:0] ioutp_range;
	wire [2:0] ioutm_range;
	wire		  lock_rladder;

	// Instantiate the Unit Under Test (UUT)
	ADC7699_auto uut (
		.clk(clk), 
		.reset(reset), 
		.start_conv_1(start_conv_1), 
		.start_conv_2(start_conv_2), 
		.start_conv_3(start_conv_3), 
		.start_conv_4(start_conv_4), 
		.start_conv_5(start_conv_5), 
		.start_conv_6(start_conv_6), 
		.start_conv_7(start_conv_7), 
		.start_conv_8(start_conv_8), 
		.start_conv_iout(start_conv_iout),
		.start_conv_single(start_conv_single),
		.out_reg(out_reg), 
		.ioutp_val(ioutp_val), 
		.ioutm_val(ioutm_val), 
		.done(done), 
		.iout_done(iout_done), 
		.SDO(SDO), 
		.SCLK(SCLK), 
		.CNV(CNV), 
		.DIN(DIN), 
		.range_sel(range_sel), 
		.iout_sel(iout_sel),
		.ioutp_range(ioutp_range),
		.ioutm_range(ioutm_range),
		.lock_rladder
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		start_conv_1 = 0;
		start_conv_2 = 0;
		start_conv_3 = 0;
		start_conv_4 = 0;
		start_conv_5 = 0;
		start_conv_6 = 0;
		start_conv_7 = 0;
		start_conv_8 = 0;
		start_conv_iout = 0;
		start_conv_single = 0;
		SDO = 1;

		// Wait 100 ns for global reset to finish
		#100; reset = 1;
		#20; reset = 0;
		#100; start_conv_1 = 1;
		#20; start_conv_1 = 0;
		
		/*
		#10000;
		start_conv_iout = 1;
		#20; start_conv_iout = 0;
		*/
		
		#10000;
		start_conv_single = 1;
		#20; start_conv_single = 0;
        
		// Add stimulus here

	end
	
	always #10 clk=~clk;
      
endmodule

