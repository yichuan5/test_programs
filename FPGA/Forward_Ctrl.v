`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:32:42 04/19/2011 
// Design Name: 
// Module Name:    Forward_Ctrl 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Forward_Ctrl(

//Host interface: front-end
    input clk,
	 input reset,
    input start_forward,
    input [15:0] num_of_vect,
    input [7:0] num_of_vin,
	 input [15:0] fwd_prop_delay,
    input [13:0] datainbuf_din,
	 input datainbuf_wren,
    input [5:0] addrinbuf_din,
	 input addrinbuf_wren,
    output [15:0] dataoutfifo_pv_dout,
    input dataoutfifo_pv_rden,
	 output [15:0] dataoutfifo_mv_dout,
    input dataoutfifo_mv_rden,
	 output [5:0] dataoutfifo_range_dout,
    input dataoutfifo_range_rden,
    output reg forward_done,
	 output reg [15:0] cycles,
	 	 
// back-end: dac
	 output reg dac_start,
	 output reg [13:0] dac_din,
	 output reg [5:0] dac_addr,
	 output wire [1:0] dac_reg,
	 
// back-end: adc
	 output reg adc_start_conv,
	 input [15:0] pv_din,
	 input [15:0] mv_din,
	 input [2:0] p_range,
	 input [2:0] m_range,
	 input adc_done,
 
// misc
	 output reg forward_enabled
    );
//FIFO and RAM wires declaration
wire [5:0] addrinbuf_dout;
wire [13:0] datainbuf_dout;
reg [13:0] datainbuf_addr_a;
reg [13:0] datainbuf_addr_b;
reg [4:0] addrinbuf_addr_a;
reg [4:0] addrinbuf_addr_b;

reg [15:0] dataoutfifo_pv_din;
reg [15:0] dataoutfifo_mv_din;
reg [5:0] dataoutfifo_range_din;
reg dataoutfifo_range_wren;
reg dataoutfifo_pv_wren;
reg dataoutfifo_mv_wren;

//DAC address buffer
addrinbuf addrinbuf_inst (
	.clka(clk),
	.wea(addrinbuf_wren), // Bus [0 : 0] 
	.addra(addrinbuf_addr_a), // Bus [4 : 0] 
	.dina(addrinbuf_din), // Bus [5 : 0] 
	.clkb(clk),
	.addrb(addrinbuf_addr_b), // Bus [4 : 0] 
	.doutb(addrinbuf_dout)); // Bus [5 : 0] 

//Input vectors buffer
datainbuf datainbuf_inst (
	.clka(clk),
	.wea(datainbuf_wren), // Bus [0 : 0] 
	.addra(datainbuf_addr_a), // Bus [14 : 0] 
	.dina(datainbuf_din), // Bus [13 : 0] 
	.clkb(clk),
	.rstb(reset),
	.addrb(datainbuf_addr_b), // Bus [14 : 0] 
	.doutb(datainbuf_dout)); // Bus [13 : 0] 

//Output from IOUTP_VAL
dataoutfifo dataoutfifo_ioutp_inst (
	.clk(clk),
	.rst(reset),
	.din(dataoutfifo_pv_din), // Bus [15 : 0] 
	.wr_en(dataoutfifo_pv_wren),
	.rd_en(dataoutfifo_pv_rden),
	.dout(dataoutfifo_pv_dout), // Bus [15 : 0] 
	.full(),
	.empty());

/* Distributed RAM based
//Output from IOUTM_VAL	
dataoutfifo dataoutfifo_ioutm_inst (
	.clk(clk),
	.rst(reset),
	.din(dataoutfifo_mv_din), // Bus [15 : 0] 
	.wr_en(dataoutfifo_mv_wren),
	.rd_en(dataoutfifo_mv_rden),
	.dout(dataoutfifo_mv_dout), // Bus [15 : 0] 
	.full(),
	.empty());
*/	

//Output from IOUTM_VAL	
dataoutfifo_bram dataoutfifo_ioutm_inst (
	.clk(clk),
	.srst(reset),
	.din(dataoutfifo_mv_din), // Bus [15 : 0] 
	.wr_en(dataoutfifo_mv_wren),
	.rd_en(dataoutfifo_mv_rden),
	.dout(dataoutfifo_mv_dout), // Bus [15 : 0] 
	.full(),
	.empty());

/* Distributed RAM based
//Output from IOUTP_RANGE
dataoutfifo_range dataoutfifo_range_inst (
	.clk(clk),
	.rst(reset),
	.din(dataoutfifo_range_din), // Bus [5 : 0] 
	.wr_en(dataoutfifo_range_wren),
	.rd_en(dataoutfifo_range_rden),
	.dout(dataoutfifo_range_dout), // Bus [5 : 0] 
	.full(),
	.empty());
*/

//Output from IOUTP_RANGE
dataoutfifo_range_blk dataoutfifo_range_inst (
	.clk(clk),
	.srst(reset),
	.din(dataoutfifo_range_din), // Bus [5 : 0] 
	.wr_en(dataoutfifo_range_wren),
	.rd_en(dataoutfifo_range_rden),
	.dout(dataoutfifo_range_dout), // Bus [5 : 0] 
	.full(),
	.empty());

	
reg [15:0] curr_vect;
reg [7:0] curr_vin;
reg [31:0] wait_cntr;
reg [19:0] state;

parameter s_ready=		20'b00000000000000000001;
parameter s_vset1= 		20'b00000000000000000010;
parameter s_vset2= 		20'b00000000000000000100;
parameter s_vset3= 		20'b00000000000000001000;
parameter s_vset4=		20'b00000000000000010000;
parameter s_vset5=		20'b00000000000000100000;
parameter s_conv1=		20'b00000000000001000000;
parameter s_conv2= 		20'b00000000000010000000;
parameter s_conv3= 		20'b00000000000100000000;
parameter s_conv4= 		20'b00000000001000000000;
parameter s_conv5= 		20'b00000000010000000000;
parameter s_conv6= 		20'b00000000100000000000;

parameter FWD_DACS_DELAY = 16'hA;
parameter FORWARD_PROP_DELAY = 16'hA;


//parameter p_dac_settle = 32'd50000000;
//parameter p_wait_sigprop = 32'd50000;
//parameter p_wait_mux_settle = 32'd50000;

assign fwd_state = state[15:0];
assign dac_reg = 2'b11;

always @(posedge clk)
if (reset) begin
	state <= s_ready;
	datainbuf_addr_a <= 0;
	datainbuf_addr_b <= 0;
	addrinbuf_addr_a <= 0;
	addrinbuf_addr_b <= 0;
	forward_done <= 0;
	curr_vect <= 1;
	curr_vin <= 1;
	adc_start_conv <= 0;
	dataoutfifo_pv_wren <= 0;
	dataoutfifo_mv_wren <= 0;
	dataoutfifo_range_wren <= 0;
	forward_enabled <= 0;
	dac_start <= 0;
	dac_din <= 0;
	dac_addr <= 0;
	cycles <= 16'h0000;
end
else if (datainbuf_wren) begin
	datainbuf_addr_a <= datainbuf_addr_a + 1; // update input address for okPipeIn
end
else if (addrinbuf_wren) begin
	addrinbuf_addr_a <= addrinbuf_addr_a + 1;	// update input address for okPipeIn
end	
else case (state)
s_ready: begin
	datainbuf_addr_b <= 0; //address of the word to be sent to the dac
	addrinbuf_addr_b <= 0; //address of the DAC channel
	curr_vect <= 1; //current vector being processed
	curr_vin <= 1; //current feature in the vector
	forward_done <= 0;
	adc_start_conv <= 0;
	dataoutfifo_pv_wren <= 0;
	dataoutfifo_mv_wren <= 0;
	dataoutfifo_range_wren <= 0;	
	dac_start <= 0;
	cycles <= 16'h0000;
	if (start_forward) begin
		state <= s_vset1;
		forward_enabled <= 1;
	end
	else begin
		state <= s_ready;
		forward_enabled <= 1'b0;
	end
end	
	
/********* algorithm ***********************	
for curr_vect = 1:num_of_vect
	for curr_vin = 1:num_of_vin
		dac_din <- datainbuf[datainbuf_addr_b]
		dac_addr <- addrinbuf[addrinbuf_addr]
		dac_start
		wait for dac update
		datainbuf_addr_b += 1
		addrinbuf_addr += 1
	end
	addrinbuf_addr = 0
	wait until signals propagate through the network
	mux = 01
	adc_start_conv
	wait for adc_done
	dataoutbuf <- adc_dout
	mux = 00
	adc_start_conv
	wait for adc_done
	dataoutbuf <- adc_dout
end	
*/	
	
	
//first FOR loop	
s_vset1: begin //start setting dacs for the current input vector
	dataoutfifo_pv_wren <= 0;
	dataoutfifo_mv_wren <= 0;
	dataoutfifo_range_wren <= 0;	
	forward_done <= 0;
	if (curr_vect > num_of_vect) begin //used all vectors in ram
		state <= s_ready;
		forward_done <= 1;
	end	
	else begin
		state <= s_vset2;
		curr_vect <= curr_vect + 1;
	end
end
				//second FOR loop
				s_vset2: begin	
					dataoutfifo_pv_wren <= 0;
					dataoutfifo_mv_wren <= 0;
					dataoutfifo_range_wren <= 0;	
					forward_done <= 0;
					if (curr_vin > num_of_vin) begin //in the first loop set curr_vin <= 1 !!!
						state <= s_vset5;
					end
					else begin
						state <= s_vset3;
						curr_vin <= curr_vin + 1;
					end
				end

								s_vset3: begin
									state <= s_vset4;
									dac_din <= datainbuf_dout;
									dac_addr <= addrinbuf_dout;
									dac_start <= 1;
									datainbuf_addr_b <= datainbuf_addr_b + 1;
									addrinbuf_addr_b <= addrinbuf_addr_b + 1;
									wait_cntr[15:0] <= FWD_DACS_DELAY;
									wait_cntr[31:16] <= 16'h0000;
								end

								s_vset4: begin
									dac_start <= 0;
									if (wait_cntr == 0) begin
										state <= s_vset2;
									end
									else begin
										state <= s_vset4;
										wait_cntr <= wait_cntr - 1;
									end
								end	


s_vset5: begin
	state <= s_conv1;
	curr_vin <= 1;
	addrinbuf_addr_b <= 0;
	wait_cntr[7:0] <= 8'h00;
	wait_cntr[23:8] <= fwd_prop_delay; //ADC_auto waits for another 500us after start_adc
	wait_cntr[31:24] <= 8'h00;
end

s_conv1: begin
	if (wait_cntr == 0) begin
		state <= s_conv2;
		adc_start_conv <= 1;
	end
	else begin
		state <= s_conv1;
		wait_cntr <= wait_cntr - 1;
	end
end

s_conv2: begin
	adc_start_conv <= 0;
	if (adc_done) begin
		state <= s_conv3;
	end
	else begin
		state <= s_conv2;
	end
end	
	
s_conv3: begin
	state <= s_vset1;
	dataoutfifo_pv_din <= pv_din;
	dataoutfifo_pv_wren <= 1;
	dataoutfifo_mv_din <= mv_din;
	dataoutfifo_mv_wren <= 1;
	dataoutfifo_range_din[2:0] <= p_range;
	dataoutfifo_range_din[5:3] <= m_range;
	dataoutfifo_range_wren <= 1;
	cycles <= cycles + 1;
end	

endcase
endmodule

