`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:09:57 11/01/2012 
// Design Name: 
// Module Name:    IOUT_SEL_Interface 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module IOUT_SEL_Interface(
	input [30:0] iout_sel_word,
	input trig,
	input clk,
	input reset,
	
	output reg DATA_IN,
	output reg [4:0] ROW_ADDR,
	output reg LATCH_EN
    );

// state machine states
parameter s_ready = 						6'b000001;
parameter s_trig1 = 						6'b000010;
parameter s_trig2 = 						6'b000100;
parameter s_trig3 =	   				6'b001000;
parameter s_trig4 =						6'b010000;

parameter clk_div_ratio = 4'hF;


reg [5:0] state;
reg [3:0] wait_time;
reg [4:0] index;


always @(posedge clk) begin
	if (reset) begin
		state <= s_ready;
		DATA_IN <= 1'b0;
		ROW_ADDR <= 5'b00000;
		LATCH_EN <= 1'b0;
		index <= 5'b00000;
	end
	else case (state)
	s_ready: begin
		DATA_IN <= 1'b0;
		ROW_ADDR <= 5'b00000;
		LATCH_EN <= 1'b0;
		index <= 5'b00000;
		if (trig) begin
			state <= s_trig1;
		end
		else begin
			state <= s_ready;
		end
	end	
	
	s_trig1: begin
		DATA_IN <= iout_sel_word[index];
		ROW_ADDR <= index;
		LATCH_EN <= 1'b0;
		state <= s_trig2;
		wait_time <= clk_div_ratio;
	end

	s_trig2: begin
		LATCH_EN <= 1'b0;
		if (wait_time != 0) begin
			wait_time <= wait_time - 1;
			state <= s_trig2;
		end
		else begin
			state <= s_trig3;
			wait_time <= clk_div_ratio;
		end
	end

	s_trig3: begin
		if (wait_time != 0) begin
			wait_time <= wait_time - 1;
			LATCH_EN <= 1'b1;
			state <= s_trig3;
		end
		else begin
			state <= s_trig4;
			LATCH_EN <= 1'b0;
		end
	end

	s_trig4: begin
		if (index == 5'h1E) begin
			state <= s_ready;
		end
		else begin
			state <= s_trig1;
			index <= index + 1;
		end
	end	
	endcase

end
endmodule
