# README #
This git contains various files used to test the on-die calibration chip.

### Folder Description ###
FPGA: Xilinx project folder to drive the Opal Kelly FPGA, provide drivers for ADC and DAC, clock, and digital controls for digital logics.

MATLAB: MatLab files interface with programed FPGA for PC communication (data read/write, triggers), and various Testing setups.

Labview: all RF testing setups, with VNA, NF, and DC supplies.

