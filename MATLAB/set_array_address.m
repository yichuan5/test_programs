function set_array_address(row,col)
% set row and column address
global ports;
write_fpga_reg(ports.nn_address,bitor(bitshift(row,5),col));
end
