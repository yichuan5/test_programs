%%%%%%%%%%%%S/H accuracy test%%%%%%%%%%%%%%%
write_dac_v(ports.dac.sensor_sel, 3.3);
seq= 0.4:.025:3.3;
seqind= 1:length(seq);
x1= zeros(1,length(seq));
x2= zeros(1,length(seq));
x3= zeros(1,length(seq));
x1t=x1;
x2t=x2;
x3t=x3;
y= zeros(1,length(seq));

fileID = fopen('sha_accuracy2.txt','w');

for i= seqind
write_dac_v(ports.dac.sen_ref,seq(i));
pause(1)
y(i)=read_adc_v(6);
write_fpga_reg(ports.shc,bin2dec('111'))
% x1t(i)=read_adc_v(3);
% x2t(i)=read_adc_v(4);
% x3t(i)=read_adc_v(7);
%pause(0.1);
write_fpga_reg(ports.shc,bin2dec('000'))
%pause(0.1);
x1(i)=repeatADC(3,4); %Vread3
x2(i)=repeatADC(4,4); %Vread4 
x3(i)=repeatADC(7,4); %Vread5

fprintf(fileID,'%f ,%f ,%f , %f\r\n',repeatADC(6,4),repeatADC(3,4),repeatADC(4,4),repeatADC(7,4));
end
fclose(fileID);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%S/H retension time test%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_dac_v(ports.dac.sensor_sel, 3.3);
write_dac_v(ports.dac.sen_ref,1.2);
y=read_adc_v(6);
write_fpga_reg(ports.shc,bin2dec('000'))%111
write_fpga_reg(ports.shc,bin2dec('111'))%000
seq= 1:1000;
x1= zeros(1,length(seq));
x2= zeros(1,length(seq));
x3= zeros(1,length(seq));
fileID = fopen('sha_retension2.txt','w');
for i=seq
y(i)=read_adc_v(6);
x1(i)=repeatADC(3,3);
x2(i)=repeatADC(4,3);
x3(i)=repeatADC(7,3); 
pause(0.1)

fprintf(fileID,'%f ,%f ,%f , %f\r\n',read_adc_v(6),repeatADC(3,4),repeatADC(4,4),repeatADC(7,4));
end

fclose(fileID);

