function hv_set_avdd(v);
global ports;
write_dac_v(ports.dac.avdd,v/2);