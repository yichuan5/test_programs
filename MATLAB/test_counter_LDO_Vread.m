%test chain of counter, LDO , vread.

write_dac_v(ports.dac.comp0,0.4);
write_dac_v(ports.dac.comp1,0.4);
write_dac_v(ports.dac.comp2,0.4);
write_dac_v(ports.dac.LDOt_en,3.3);
write_dac_v(ports.dac.LDO_en,3.3);
write_dac_v(ports.dac.LDO_sel,1.2);



activatetriggerin(xem,ports.trigs,2) %reset_trig
write_fpga_reg(ports.n_count,1)
pause(1)
activatetriggerin(xem,ports.trigs,0)
pause(1)
repeatADC(4,8)/2%VR0
repeatADC(8,8)/2%VR1
repeatADC(5,8)/2%VR2
activatetriggerin(xem,ports.trigs,1) %out_trig

updatewireouts(xem);
getwireoutvalue(xem,ports.counterdone)
dec2bin(getwireoutvalue(xem,ports.counterout))


write_fpga_reg(ports.n_count,10000)
activatetriggerin(xem,ports.trigs,0)
activatetriggerin(xem,ports.trigs,1) %out_trig
updatewireouts(xem);
getwireoutvalue(xem,ports.counterdone)
dec2bin(getwireoutvalue(xem,ports.counterout))