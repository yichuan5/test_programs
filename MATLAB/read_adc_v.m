%function vout = read_adc_v(chan)
function [vout,code] = read_adc_v(chan)
% chan from 1 to 8
global xem;
activatetriggerin(xem,hex2dec('41'),chan); % same as activatetriggerin(xem,ports.tr.adc,6) , one shot conversion 
for j=1:100
    %pause(0.1);
    updatetriggerouts(xem);
    if istriggered(xem,hex2dec('60'),1)%same as istriggered(xem,ports.tro.adc,1) means is adc_done == 1
       updatewireouts(xem);
       code = getwireoutvalue(xem,hex2dec('20')); % same as getwireoutvalue(xem,ports.adc_out), first time used this wire here , we dont care about range
       vout = (code)*5/32767; %DAC
       %vout = (bitand(code,65024))*5/32767;
       break;
    end
    if (j==100)
        vout = -1;
    end
end