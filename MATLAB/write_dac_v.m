function write_dac_v(reg,val)
global xem;

setwireinvalue(xem,0,bin2dec('11'),int32(hex2dec('ffff'))); % reg1,2 = 0
setwireinvalue(xem,1,reg,int32(hex2dec('ffff'))); % addr: 001100
setwireinvalue(xem,2,floor(val*(-1+2^14)/5),int32(hex2dec('ffff'))); 
%setwireinvalue(xem,2,bitand(floor(val*(-1+2^14)/5),16128),int32(hex2dec('ffff')));  
updatewireins(xem);
activatetriggerin(xem,hex2dec('40'),0);