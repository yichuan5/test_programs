function [vr0,vr1,vr2]=counter2voltage(counter) %translate counter to gray analog value
seq= dec2bin(bin2gray(0:63,'psk',64));

r=seq(counter,:);

switch r(1:2)
    case '00'
        vr2=0.8;
    case '01'
        vr2=1.0;
    case '10'
        vr2=1.2;
    case '11'
        vr2=1.4;
end
switch r(3:4)
    case '00'
        vr1=0.8;
    case '01'
        vr1=1.0;
    case '10'
        vr1=1.2;
    case '11'
        vr1=1.4;
end
switch r(5:6)
    case '00'
        vr0=0.8;
    case '01'
        vr0=1.0;
    case '10'
        vr0=1.2;
    case '11'
        vr0=1.4;
end

end
