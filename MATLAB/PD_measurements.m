clear all;
addpath('C:\Users\yxl079200\Desktop\mono_FPGA\OpalKellyFunctions');
global xem;
global ports;

xem.ptr = 0;
xem.serial = '';
xem.deviceID = '';
xem.major = 0;
xem.minor = 0;
xem.brdModel = '';
if libisloaded('okFrontPanel')
    unloadlibrary('okFrontPanel');
end
loadlibrary('okFrontPanel','okFrontPanelDLL.h');
xem.ptr = calllib('okFrontPanel','okFrontPanel_Construct');
[ret,x] = calllib('okFrontPanel','okFrontPanel_OpenBySerial', xem.ptr, xem.serial);
pause(3)
xem.major = calllib('okFrontPanel','okFrontPanel_GetDeviceMajorVersion', xem.ptr);

configurefpga(xem,'top_auto.bit')

ports.dac_reg = 0;%2bit
ports.dac_addr = 1;%6bit
ports.dac_db = 2;%14bit
ports.idac_range = 3;%range[7:0]
ports.idac_val1 = 4;%[11:0]
ports.idac_val2 = 5;%[11:0]
ports.shc=hex2dec('17');%[3:0]

% XEM WireOut Ports
ports.adc_out = hex2dec('20'); %output of ADC conversion, 16b
ports.idac_val1_out = hex2dec('21'); %lower 12-bits from IDAC scan-out port
ports.idac_val2_out = hex2dec('22'); %upper 12-bits from IDAC scan-out port
ports.adc_ioutp_val = hex2dec('23'); %conversion result from IOUTP->ILAD->ADC
ports.adc_ioutm_val = hex2dec('24'); %conversion result from IOUTM->ILAD->ADC
ports.adc_ioutp_rng = hex2dec('25'); %what range of ILAD was used
ports.adc_ioutm_rng = hex2dec('26'); %what range of ILAD was used

% % XEM TriggerIn Ports
ports.tr.dac = hex2dec('40'); %DAC INTERFACE: 0-start,1-reset
ports.tr.adc = hex2dec('41'); %ADC AUTO:0-reset,1to8-start_conv_1to8,9-convert both IOUTP/M,10-single conversion, input selected by mux
ports.tr.idac = hex2dec('42'); %IDAC: 0-reset,1-trig range,2-trig val
ports.tr.greycounter = hex2dec('43'); %TOPOLOGY CONFIG:0-reset,1-trig
ports.tr.dpulse = hex2dec('44'); %DPULSE: 0-reset,1-set to 0,2-set to 1,3-start

% % XEM TriggerOut Ports: COUNTING STARTS FROM 1
ports.tro.adc = hex2dec('60'); %ADC: 1-adc_done,2-iout_conv_done


ports.dac.LDO_en = 4;
ports.dac.LDOt_en = 5;
ports.dac.LDO_sel = 6;
ports.dac.vbias = 7;
ports.dac.vneg = 8;
ports.dac.mode = 9;
ports.dac.threshold = 10;
ports.dac.vcasp_in = 11;
ports.dac.VCO_ctrl = 12;
ports.dac.vinjectSub = 13;
ports.dac.vcascn = 14;
ports.dac.col_buf = 15;%Gate2 signal
ports.dac.vtun = 16;
ports.dac.ibias2 = 17;  ports.dac.vgate2_ext = 18;
ports.dac.vgate1 = 19;
ports.dac.IO_drain = 20;
ports.dac.avdd = 21;
ports.dac.external = 22;
ports.dac.sw_bias = 23;
ports.dac.VCO_vt=28;
ports.dac.comp0 = 32;
ports.dac.comp1 = 33;
ports.dac.comp2 = 34;
ports.dac.comp3 = 35;
ports.dac.sen_ref = 36;
ports.dac.comp4 = 37;
ports.dac.comp5 = 38;
ports.dac.sensor_sel = 39;
ports.n_count=hex2dec('16');
ports.trigs=hex2dec('56');
ports.counterout=hex2dec('36');
ports.counterdone=hex2dec('37');

%FPGA Init: internal state machines
activatetriggerin(xem,ports.tr.dac,1); % dac interface block reset
activatetriggerin(xem,ports.tr.adc,0); % adc interface block reset
activatetriggerin(xem,ports.tr.dpulse,0); % dpulse interface block reset
activatetriggerin(xem,ports.tr.idac,0); % idac interface block reset
activatetriggerin(xem,ports.trigs,2) %reset_trig
%DAC5380 Init
write_fpga_reg(ports.dac_reg,bin2dec('0')); % reg 1,2 = 0
write_fpga_reg(ports.dac_addr,bin2dec('1100')); % addr: 001100
write_fpga_reg(ports.dac_db,bin2dec('00100000000000')); % boosted output current
activatetriggerin(xem,ports.tr.dac,0); % start writing to dac

write_dac_v(ports.dac.comp0,0.4);
write_dac_v(ports.dac.comp1,0.4);
write_dac_v(ports.dac.comp2,0.4);
write_dac_v(ports.dac.LDO_en,3.3);
write_dac_v(ports.dac.LDOt_en,3.3);
write_dac_v(ports.dac.LDO_sel,1.2);
write_dac_v(ports.dac.sen_ref,1.2);
write_dac_v(ports.dac.sensor_sel,3.3);
write_dac_v(ports.dac.VCO_vt,0);


write_fpga_reg(ports.n_count,1)
activatetriggerin(xem,ports.trigs,2) %reset_trig


seq= dec2bin(bin2gray(0:63,'psk',64)); %seq of our gray counter
PDmeas= zeros(length(seq),21);



for i=1:2
    write_dac_v(ports.dac.VCO_ctrl, 2.5);%%1.7GHz
    write_dac_v(ports.dac.sensor_sel,0); %% take non-intru 
    write_fpga_reg(ports.shc,bin2dec('111'))  %%%SAMPLE SH
    [PDmeas(i,1),PDmeas(i,2),PDmeas(i,3)]=counter2voltage(i);  %%write in ideal voltage
    PDmeas(i,4)=repeatADC(6,32)/2;%%write in actual voltage
    PDmeas(i,5)=repeatADC(8,32)/2;
    PDmeas(i,6)=repeatADC(5,32)/2;
    
    PDmeas(i,7)=repeatADC(3,32);  %VR3___NS1
    PDmeas(i,8)=repeatADC(4,32);  %VR4___NS2
    PDmeas(i,9)=repeatADC(7,32);  %VR5___NS3
    
    write_dac_v(ports.dac.sensor_sel,3.3); %% take intru 
    pause(1)
    PDmeas(i,10)= repeatADC(1,64);  %PD_p @1.7
    PDmeas(i,11)= repeatADC(2,64);  %PD_n @1.7
    write_fpga_reg(ports.shc,bin2dec('110')) %% SH hold
    
    write_dac_v(ports.dac.VCO_ctrl, 1.6);%%1.6GHz   set next frequency 
    PDmeas(i,12)=repeatADC(3,64);  %VR3_SH1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    pause(1)
    PDmeas(i,13)= repeatADC(1,64);  %PD_p @1.6
    PDmeas(i,14)= repeatADC(2,64);  %PD_n @1.6
    write_fpga_reg(ports.shc,bin2dec('100')) %% SH hold
    write_dac_v(ports.dac.VCO_ctrl, 1.4);%%1.4GHz   set next frequency 
    PDmeas(i,15)=repeatADC(4,64);  %VR4___SH2
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    pause(1)
    PDmeas(i,16)= repeatADC(1,64);  %PD_p @1.6
    PDmeas(i,17)= repeatADC(2,64);  %PD_n @1.6
    write_fpga_reg(ports.shc,bin2dec('000')) %% SH hold
    write_dac_v(ports.dac.VCO_ctrl, 0);%%0GHz   set next frequency 
    PDmeas(i,18)= repeatADC(7,64);  %VR5__SH3
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    pause(1)
    write_fpga_reg(ports.shc,bin2dec('111'))  %%%SAMPLE SH
    PDmeas(i,19)= repeatADC(1,64);  %PD_p @0
    PDmeas(i,20)= repeatADC(2,64);  %PD_n @0
    write_fpga_reg(ports.shc,bin2dec('001')) %% 1 SH hold
    activatetriggerin(xem,ports.trigs,0) %  count_trig 
    PDmeas(i,21)=repeatADC(3,64);  %VR3_NS1
end
    
    
    
    
    
    
%     pause(1)
%     %x1t(i)=repeatADC(6,8);%%
%     write_fpga_reg(ports.shc,bin2dec('110'))
%     write_dac_v(ports.dac.VCO_ctrl, 1.6);%%1.6GHz
%     pause(1)
%    % x2t(i)=repeatADC(6,8);%%
%     write_fpga_reg(ports.shc,bin2dec('100'))
%     write_dac_v(ports.dac.VCO_ctrl, 1.4);%%%%1.4GHz
%     pause(1)
%    % x3t(i)=repeatADC(6,8);%%
%     write_fpga_reg(ports.shc,bin2dec('000'))
%     pause(1)
%     x1(i)=repeatADC(3,32);
%     x2(i)=repeatADC(4,32);
%     x3(i)=repeatADC(7,32);
%     activatetriggerin(xem,ports.trigs,0)    %count_trig
% %     [v1,v2,v3]=counter2voltage(i);
% %     
% %     spec(i)=predict(s21,[v1,v2,v3 x1(i),x2(i),x3(i)]);
% %     if spec(i)>10
% %         break
% %     end   
% end


%%%%%%%%%%%%%%%%%%ADC Connection MAP%%%%%%%%%%%%%%%%%
% repeatADC(1,32)  %PD_p
% repeatADC(2,32)  %PD_n
% repeatADC(3,32)  %VR3__ SH1__NS1
% repeatADC(4,32)  %VR4___SH2__NS2
% repeatADC(5,32)  %V3
% repeatADC(6,64)  %V1
% repeatADC(7,32)  %VR5__SH3__NS3
% repeatADC(8,32)  %V2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


write_fpga_reg(ports.n_count,1)
activatetriggerin(xem,ports.trigs,0) %count_trig



write_dac_v(ports.dac.VCO_ctrl, 0)