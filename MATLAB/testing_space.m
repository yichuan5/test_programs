clear all;
addpath('C:\Users\yxl079200\Desktop\mono_FPGA\OpalKellyFunctions');
global xem;
global ports;

xem.ptr = 0;
xem.serial = '';
xem.deviceID = '';
xem.major = 0;
xem.minor = 0;
xem.brdModel = '';
if libisloaded('okFrontPanel')
    unloadlibrary('okFrontPanel');
end
loadlibrary('okFrontPanel','okFrontPanelDLL.h');
xem.ptr = calllib('okFrontPanel','okFrontPanel_Construct');
[ret,x] = calllib('okFrontPanel','okFrontPanel_OpenBySerial', xem.ptr, xem.serial);
pause(3)
xem.major = calllib('okFrontPanel','okFrontPanel_GetDeviceMajorVersion', xem.ptr);

configurefpga(xem,'top_auto.bit')

ports.dac_reg = 0;%2bit
ports.dac_addr = 1;%6bit
ports.dac_db = 2;%14bit
ports.idac_range = 3;%range[7:0]
ports.idac_val1 = 4;%[11:0]
ports.idac_val2 = 5;%[11:0]
ports.shc=hex2dec('17');%[3:0]
ports.div_clk=hex2dec('18');% div clk start

% XEM WireOut Ports
ports.adc_out = hex2dec('20'); %output of ADC conversion, 16b
ports.idac_val1_out = hex2dec('21'); %lower 12-bits from IDAC scan-out port
ports.idac_val2_out = hex2dec('22'); %upper 12-bits from IDAC scan-out port
ports.adc_ioutp_val = hex2dec('23'); %conversion result from IOUTP->ILAD->ADC
ports.adc_ioutm_val = hex2dec('24'); %conversion result from IOUTM->ILAD->ADC
ports.adc_ioutp_rng = hex2dec('25'); %what range of ILAD was used
ports.adc_ioutm_rng = hex2dec('26'); %what range of ILAD was used

% % XEM TriggerIn Ports
ports.tr.dac = hex2dec('40'); %DAC INTERFACE: 0-start,1-reset
ports.tr.adc = hex2dec('41'); %ADC AUTO:0-reset,1to8-start_conv_1to8,9-convert both IOUTP/M,10-single conversion, input selected by mux
ports.tr.idac = hex2dec('42'); %IDAC: 0-reset,1-trig range,2-trig val
ports.tr.greycounter = hex2dec('43'); %TOPOLOGY CONFIG:0-reset,1-trig
ports.tr.dpulse = hex2dec('44'); %DPULSE: 0-reset,1-set to 0,2-set to 1,3-start

% % XEM TriggerOut Ports: COUNTING STARTS FROM 1
ports.tro.adc = hex2dec('60'); %ADC: 1-adc_done,2-iout_conv_done


ports.dac.LDO_en = 4;
ports.dac.LDOt_en = 5;
ports.dac.LDO_sel = 6;
ports.dac.vbias = 7;
ports.dac.vneg = 8;
ports.dac.mode = 9;
ports.dac.threshold = 10;
ports.dac.vcasp_in = 11;
ports.dac.VCO_ctrl = 12;
ports.dac.vinjectSub = 13;
ports.dac.vcascn = 14;
ports.dac.col_buf = 15;%Gate2 signal
ports.dac.vtun = 16;
ports.dac.ibias2 = 17;  
ports.dac.vgate2_ext = 18;
ports.dac.vgate1 = 19;
ports.dac.IO_drain = 20;
ports.dac.avdd = 21;
ports.dac.external = 22;
ports.dac.sw_bias = 23;
ports.dac.VCO_vt=28;
ports.dac.comp0 = 32;
ports.dac.comp1 = 33;
ports.dac.comp2 = 34;
ports.dac.comp3 = 35;
ports.dac.sen_ref = 36;
ports.dac.comp4 = 37;
ports.dac.comp5 = 38;
ports.dac.sensor_sel = 39;



%FPGA Init: internal state machines
activatetriggerin(xem,ports.tr.dac,1); % dac interface block reset
activatetriggerin(xem,ports.tr.adc,0); % adc interface block reset
activatetriggerin(xem,ports.tr.dpulse,0); % dpulse interface block reset
activatetriggerin(xem,ports.tr.idac,0); % idac interface block reset
activatetriggerin(xem,ports.tr.greycounter,0); % grey counter interface block reset

%DAC5380 Init
write_fpga_reg(ports.dac_reg,bin2dec('0')); % reg 1,2 = 0
write_fpga_reg(ports.dac_addr,bin2dec('1100')); % addr: 001100
write_fpga_reg(ports.dac_db,bin2dec('00100000000000')); % boosted output current
activatetriggerin(xem,ports.tr.dac,0); % start writing to dac
write_fpga_reg(ports.div_clk,bin2dec('1')); %start div clk

write_dac_v(ports.dac.comp0,0.4);
write_dac_v(ports.dac.comp1,0.4);
write_dac_v(ports.dac.comp2,0.4);
write_dac_v(ports.dac.LDO_en,3.3);
write_dac_v(ports.dac.LDOt_en,3.3);
write_dac_v(ports.dac.LDO_sel,1.2);
write_dac_v(ports.dac.sen_ref,1.2);
write_dac_v(ports.dac.sensor_sel,3.3);
write_dac_v(ports.dac.VCO_vt,0);


ports.n_count=hex2dec('16');
ports.trigs=hex2dec('56');
ports.counterout=hex2dec('36');
ports.counterdone=hex2dec('37');
%%%%%%%%%%%%%%%%%%%% Program done%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
repeatADC(1,8)%VR2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
write_fpga_reg(ports.n_count,1)
activatetriggerin(xem,ports.trigs,0) %count_trig
activatetriggerin(xem,ports.trigs,1) %out_trig
activatetriggerin(xem,ports.trigs,2) %reset_trig

write_fpga_reg(ports.shc,bin2dec('111'))
write_dac_v(ports.dac.VCO_ctrl, 1);
write_fpga_reg(ports.shc,bin2dec('110'))
write_dac_v(ports.dac.VCO_ctrl, 1.2);
write_fpga_reg(ports.shc,bin2dec('100'))
write_dac_v(ports.dac.VCO_ctrl, 1.6);
write_fpga_reg(ports.shc,bin2dec('000'))
read_adc_v(3) %VR 3   SH1  
read_adc_v(4) %VR 4   SH2
read_adc_v(7) %VR 5   SH3


write_fpga_reg(ports.shc,bin2dec('110'))
write_fpga_reg(ports.shc,bin2dec('111'))
write_fpga_reg(ports.shc,bin2dec('110'))
pause(3)
read_adc_v(3) %VR 3   SH1  



write_fpga_reg(ports.shc,bin2dec('000'))
write_fpga_reg(ports.shc,bin2dec('111'))
pause(30)
write_fpga_reg(ports.shc,bin2dec('000'))
pause(3)
   %SH1  


write_fpga_reg(ports.n_count,1)
activatetriggerin(xem,ports.trigs,0) %count_trig
activatetriggerin(xem,ports.trigs,1) %out_trig


activatetriggerin(xem,ports.trigs,2) %reset_trig
write_fpga_reg(ports.n_count,1)
activatetriggerin(xem,ports.trigs,0) %count_trig
pause(1)
repeatADC(1,64)
repeatADC(2,64)
repeatADC(6,64)
read_adc_v(1)  
read_adc_v(2)

pause(1)
read_adc_v(6)/2
read_adc_v(8)/2
read_adc_v(5)/2
pause(0.3)
activatetriggerin(xem,ports.trigs,1) %out_trig
pause(0.3)
updatewireouts(xem);
getwireoutvalue(xem,ports.counterdone)
dec2bin(getwireoutvalue(xem,ports.counterout))



write_dac_v(ports.dac.sen_ref,1.6);
write_dac_v(ports.dac.col_buf, 0.82);
write_dac_v(ports.dac.vcascn, 0.8);
read_adc_v(6)



repeatADC(1,32)
repeatADC(2,32)
repeatADC(3,32)
repeatADC(4,32)
repeatADC(5,32)
repeatADC(6,64)
repeatADC(7,32)
repeatADC(8,32)



dec2bin(bin2gray(1:63,'psk',64)) %seq of our gray counter


%%%%%%%%%%%%S/H accuracy test%%%%%%%%%%%%%%%
write_dac_v(ports.dac.sensor_sel, 3.3);
seq= 0.4:.025:3.3;
seqind= 1:length(seq);
x1= zeros(1,length(seq));
x2= zeros(1,length(seq));
x3= zeros(1,length(seq));
x1t=x1;
x2t=x2;
x3t=x3;
y= zeros(1,length(seq));

fileID = fopen('v.txt','w');

for i= seqind
write_dac_v(ports.dac.sen_ref,seq(i));
pause(1)
y(i)=read_adc_v(6);
write_fpga_reg(ports.shc,bin2dec('111'))
% x1t(i)=read_adc_v(3);
% x2t(i)=read_adc_v(4);
% x3t(i)=read_adc_v(7);
%pause(0.1);
write_fpga_reg(ports.shc,bin2dec('000'))
%pause(0.1);
x1(i)=repeatADC(3,4);
x2(i)=repeatADC(4,4);
x3(i)=repeatADC(7,4);

fprintf(fileID,'%f ,%f ,%f , %f\r\n',i,repeatADC(3,4),repeatADC(4,4),repeatADC(7,4));
end
%%%%%%%%%%%%%%%%%%S/H retension time test%%%%%%%%%%%%%%%%

write_dac_v(ports.dac.sensor_sel, 3.3);
write_dac_v(ports.dac.sen_ref,1.2);
y=read_adc_v(6);
write_fpga_reg(ports.shc,bin2dec('111'))
write_fpga_reg(ports.shc,bin2dec('000'))
seq= 1:1000;
x1= zeros(1,length(seq));
x2= zeros(1,length(seq));
x3= zeros(1,length(seq));
for i=seq
%x1(i)=repeatADC(3,3);
x2(i)=repeatADC(4,3);

%x3(i)=read_adc_v(7); 
pause(0.1)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
write_dac_v(ports.dac.comp0,0.4);
write_dac_v(ports.dac.comp1,0.4);
write_dac_v(ports.dac.comp2,0.4);
write_dac_v(ports.dac.LDO_en,3.3);
write_dac_v(ports.dac.LDOt_en,3.3);
write_dac_v(ports.dac.LDO_sel,1.2);
write_dac_v(ports.dac.sen_ref,0.4);
write_dac_v(ports.dac.sensor_sel,3.3);



write_fpga_reg(ports.n_count,1)
activatetriggerin(xem,ports.trigs,2) %reset_trig

seq= dec2bin(bin2gray(0:63,'psk',64)); %seq of our gray counter
x1= zeros(1,length(seq));
x2= zeros(1,length(seq));
x3= zeros(1,length(seq));
x1t=x1;
x2t=x2;
x3t=x3;

for i=1:length(seq)
    write_dac_v(ports.dac.VCO_ctrl, 1.8);%%1.3GHz
    write_fpga_reg(ports.shc,bin2dec('111'))
    pause(1)
    x1t(i)=repeatADC(6,8);%%
    write_fpga_reg(ports.shc,bin2dec('110'))
    write_dac_v(ports.dac.VCO_ctrl, 1.6);%%1.6GHz
    pause(1)
    x2t(i)=repeatADC(6,8);%%
    write_fpga_reg(ports.shc,bin2dec('100'))
    write_dac_v(ports.dac.VCO_ctrl, 1.3);%%1.7GHz
    pause(1)
    x3t(i)=repeatADC(6,8);%%
    write_fpga_reg(ports.shc,bin2dec('000'))
    pause(1)
    x1(i)=repeatADC(3,32);
    x2(i)=repeatADC(4,32);
    x3(i)=repeatADC(7,32);
    activatetriggerin(xem,ports.trigs,0)    %count_trig
    
    [v1,v2,v3]=counter2voltage(i);
    
    spec(i)=predict(s21,[v1,v2,v3 x1(i),x2(i),x3(i)]);
    if spec(i)>10
        break
    end   
end

y1=x1;
y2=x2;
y3=x3;

result=vertcat(a(1:3,:),x1,x2,x3);
result2=vertcat(a(1:3,:),y1,y2,y3);

%%%%%%%%%%%%%%counter to LDO test%
a= zeros(6,64);
for i=1:64
[a(1,i),a(2,i),a(3,i)]=counter2voltage(i);
%a(4,i)=repeatADC(1,8)/2; %VR0
%a(5,i)=repeatADC(8,8)/2; %VR1
%a(6,i)=repeatADC(5,8)/2;  %VR2
%activatetriggerin(xem,ports.trigs,0)
%pause(1)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%
write_dac_v(ports.dac.comp0,0.4);
write_dac_v(ports.dac.comp1,0.4);
write_dac_v(ports.dac.comp2,0.4);
write_dac_v(ports.dac.LDO_en,3.3);
write_dac_v(ports.dac.LDOt_en,3.3);
write_dac_v(ports.dac.LDO_sel,1.2);


write_fpga_reg(ports.n_count,2)
activatetriggerin(xem,ports.trigs,2) %reset_trig
pause(1)
activatetriggerin(xem,ports.trigs,0)
pause(1)
repeatADC(1,8)/2%VR0
repeatADC(8,8)/2%VR1
repeatADC(5,8)/2%VR2
%%%%%%%%%%%%%%%pd mreasurement%%%%%%%%%%%%%%%%%%
write_dac_v(ports.dac.comp0,0.4);
write_dac_v(ports.dac.comp1,0.4);
write_dac_v(ports.dac.comp2,0.4);

num=10
x=zeros(1,num);
y=zeros(1,num);
z=zeros(1,num);
for i=1:num
%write_dac_v(ports.dac.VCO_ctrl, 0+i*0.1);%%1.6GHz
write_dac_v(ports.dac.comp0,0.6+i*0.04);
write_dac_v(ports.dac.comp1,0.6+i*0.04);
write_dac_v(ports.dac.comp2,0.6+i*0.04);

pause(.1)
x(i)=repeatADC_wDelay(6,64);
y(i)=repeatADC_wDelay(3,64);
z(i)=repeatADC_wDelay(7,64);
end


write_dac_v(ports.dac.VCO_ctrl, 1.2)

read_adc_v(6)
read_adc_v(3)
read_adc_v(7)
repeatADC(7,64)


write_dac_v(ports.dac.sen_ref,1.2);
repeatADC(6,64)
maxADC(6,200)
write_dac_v(ports.dac.VCO_ctrl, 1.4);%%1.6GHz
read_adc_v(6)
write_dac_v(ports.dac.VCO_ctrl, 1.2);%%1.6GHz
read_adc_v(6)

num=1000;
x=zeros(1,num);
for i=1:num
    x(i)=read_adc_v(6) ;
    pause(.5)
end


write_dac_v(ports.dac.comp0,1.2);
write_dac_v(ports.dac.comp1,1.2);
write_dac_v(ports.dac.comp2,1.2);
write_dac_v(ports.dac.LDO_en,3.3);
write_dac_v(ports.dac.LDOt_en,0);
write_dac_v(ports.dac.LDO_sel,1.2);
write_dac_v(ports.dac.sen_ref,1.2);
write_dac_v(ports.dac.sensor_sel,0);
write_dac_v(ports.dac.VCO_vt,0);



%%%%%%%%%%%%%%ENA labview set%%%%%%%%%%
write_dac_v(ports.dac.LDO_en,3.3);
write_dac_v(ports.dac.LDOt_en,0);
write_dac_v(ports.dac.LDO_sel,3.3);
repeatADC(1,8)
repeatADC(8,8)
repeatADC(5,8)
write_dac_v(ports.dac.comp0,0);
write_dac_v(ports.dac.comp1,0);
write_dac_v(ports.dac.comp2,0);

%%%%%%%%%%%%%%%combin measurement data and try regression%%%%%%%%%%
data=table2array(readtable('temp'));
data(:,1:3)=round(data(:,1:3)/0.2)*0.2;
trainingset=(vertcat(data(:,1:3).',result(4:6,:),data(:,7:10).')).';
testingset=(vertcat(data(:,1:3).',result2(4:6,:),data(:,7:10).')).';

modelfun = @(b,x)b(1)+b(2)*x(:,1).^b(3)+b(4)*x(:,2).^b(5)++b(6)*x(:,3).^b(7)+b(8)*x(:,4).^b(9)++b(10)*x(:,5).^b(11)++b(12)*x(:,5).^b(13)++b(14)*x(:,6).^b(15);
beta0 = [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1];

s11=fitnlm(trainingset(:,1:6), trainingset(:,7), modelfun, beta0)
s21=fitnlm(trainingset(:,1:6), trainingset(:,8), modelfun, beta0)
s12=fitnlm(trainingset(:,1:6), trainingset(:,9), modelfun, beta0)
s22=fitnlm(trainingset(:,1:6), trainingset(:,10), modelfun, beta0)
%%%%predict on training%%%
s11p=predict(s11,trainingset(:,1:6))
s21p=predict(s21,trainingset(:,1:6))
s12p=predict(s12,trainingset(:,1:6))
s22p=predict(s22,trainingset(:,1:6))

scatter(s11p,trainingset(:,7))
scatter(s21p,trainingset(:,8))
scatter(s12p,trainingset(:,9))
scatter(s22p,trainingset(:,10))
%%%predict on testing

s11p=predict(s11,testingset(:,1:6))
s21p=predict(s21,testingset(:,1:6))
s12p=predict(s12,testingset(:,1:6))
s22p=predict(s22,testingset(:,1:6))

scatter(s11p,trainingset(:,7))
scatter(s21p,trainingset(:,8))
scatter(s12p,trainingset(:,9))
scatter(s22p,trainingset(:,10))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[beta,Sigma,E,CovB,logL]=mvregress(trainingset(:,7:10),trainingset(:,1:6))

xx = linspace(.5,3.5)';
fits = [ones(size(xx)),xx]*beta;




write_dac_v(ports.dac.sen_ref,seq(i));
pause(1)
y(i)=read_adc_v(6);
write_fpga_reg(ports.shc,bin2dec('111'))
pause(1)
write_fpga_reg(ports.shc,bin2dec('000'))
pause(1)
x1(i)=read_adc_v(3);
x2(i)=read_adc_v(4);
x3(i)=read_adc_v(7);









read_adc_v(1)
read_adc_v(2)
read_adc_v(3)
read_adc_v(4)
read_adc_v(5)
read_adc_v(6)
read_adc_v(7)
read_adc_v(8)
