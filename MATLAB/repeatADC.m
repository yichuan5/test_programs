function y= repeatADC(channel,num)
x=zeros(1,num);
for i=1:num
    x(i)=read_adc_v(channel) ;
  
end
y=median(x);
end
