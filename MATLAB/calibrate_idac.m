% ****************************************************************
% ************ IDAC calibration *********************************
% ****************************************************************


range = 1;
i_branch = zeros(12,2);
write_fpga_reg(ports.idac_config,bin2dec('10'));
write_fpga_reg(ports.idac_range,bin2dec('00010000'));
activatetriggerin(xem,ports.tr.idac,1); % trig_range
pause(5);

write_dac_v(24,1.5) % idac drain vsource: imeas , was 31
Nmeas = 2; %was 100
res = cell(Nmeas,1);

% FOR KEITHLY RANGE UP TO 2nA
%%pause(20*60);
%%%pause(3*60*60);
for ind=1:Nmeas
    ind
    for i = 1:8
        write_fpga_reg(ports.idac_val1,2^(i-1));
        write_fpga_reg(ports.idac_val2,bin2dec('000000000000'));
        activatetriggerin(xem,ports.tr.idac,2); % trig_val
        pause(5);
        i_branch(i,1) = read6485(30);
        write_fpga_reg(ports.idac_val2,2^(i-1));
        write_fpga_reg(ports.idac_val1,bin2dec('000000000000'));
        activatetriggerin(xem,ports.tr.idac,2); % trig_val
        pause(5);
        i_branch(i,2) = read6485(30);
    end
    res{ind}(1:8,:)=i_branch(1:8,:);
    save('ID2_IDAC_CAL','res');
end
fwrite(mcc15,'CURR:RANG:UPPER 0.00000002');fwrite(mcc15,char(10));
% FOR KEITHLY RANGE UP TO 200nA
pause(20);
for ind=1:Nmeas
    ind
    for i = 9:12
        write_fpga_reg(ports.idac_val1,2^(i-1));
        write_fpga_reg(ports.idac_val2,bin2dec('000000000000'));
        activatetriggerin(xem,ports.tr.idac,2); % trig_val
        pause(5);
        i_branch(i,1) = read6485(30);
        write_fpga_reg(ports.idac_val2,2^(i-1));
        write_fpga_reg(ports.idac_val1,bin2dec('000000000000'));
        activatetriggerin(xem,ports.tr.idac,2); % trig_val
        pause(5);
        i_branch(i,2) = read6485(30);
    end
    res{ind}(9:12,:)=i_branch(9:12,:);
    save('ID2_IDAC_CAL','res');
end

% get stats: mean, spread
Nmeas = 100;
i1 = zeros(Nmeas,12);
i2 = zeros(Nmeas,12);
for i=1:Nmeas
    i1(i,:) = abs(res{i}(:,1));
    i2(i,:) = abs(res{i}(:,2));
end    
i1_b = mean(i1);
i2_b = mean(i2);


% plot(log10(mean(i1)),std(i1),'.-k');
% create current-codeword table
idac_tb = zeros(24,2);
idac_tb(1:12,2) = i1_b';
idac_tb(13:24,2) = i2_b';
for i=1:12
    idac_tb(i,1) = 2^(i-1);
    idac_tb(i+12,1) = bitshift(2^(i-1),16);
end
[a ind] = sort(idac_tb(:,2)); % ind 1-24
idac_tb1 = idac_tb(ind,:);
idac_tb = idac_tb1;

% find codeword for itar
%itar = 4.104e-9;
itar = 2.6391e-9;
codeword = 0;
jmax = 24;
while (itar>=min(idac_tb(:,2))) && (jmax>0)
    d1 = itar - idac_tb(1:jmax,2);
    d1(d1<0) = 1;
    [a ind] = min(d1);
    codeword = codeword + idac_tb(ind,1);
    itar = a;
    jmax = ind-1;
end



% test DNL
itest = linspace(log10(1e-12),log10(50e-9),1000);
for i = 1:1000
    itar = 10^itest(i);
    codeword = 0;
    jmax = 24;
    while (itar>=min(idac_tb(:,2))) && (jmax>0)
        d1 = itar - idac_tb(1:jmax,2);
        d1(d1<0) = 1;
        [a ind] = min(d1);
        codeword = codeword + idac_tb(ind,1);
        itar = a;
        jmax = ind-1;
    end
    ires(i) = itar;
end
plot(log10(10.^itest),log10(10.^itest-ires),'.-k'); grid on;
    
% test with picoamm
n = 512;
ires = [];
ipred = [];
itest = linspace(log10(10e-12),log10(50e-9),n);
pause(30*60);
for i = 1:n
    i
    itar = 10^itest(i);
    codeword = 0;
    jmax = 24;
    while (itar>=min(idac_tb(:,2))) && (jmax>0)
        d1 = itar - idac_tb(1:jmax,2);
        d1(d1<0) = 1;
        [a ind] = min(d1);
        codeword = codeword + idac_tb(ind,1);
        itar = a;
        jmax = ind-1;
    end
    ipred(i) = 10^itest(i) - itar;
    write_fpga_reg(ports.idac_val2,bitshift(codeword,-16));
    write_fpga_reg(ports.idac_val1,bitand(codeword,hex2dec('FFFF')));
    activatetriggerin(xem,ports.tr.idac,2); % trig_val
    pause(1);
    ires(i) = read6485(10);
end
plot(log10(10.^itest),log10(abs(ires)),'.-k'); grid on;
plot(10.^itest,abs(ires),'.-k'); grid on;
plot(log10(ipred),log10(abs(ires)),'.-k'); grid on;
plot(ipred,abs(ires),'.-k'); grid on;

% try combinations
write_fpga_reg(ports.idac_val1,bin2dec('000010010000'));
write_fpga_reg(ports.idac_val2,bin2dec('000000000100'));
activatetriggerin(xem,ports.tr.idac,2); % trig_val
i1_b(5)+i1_b(8)+i2_b(3)

%% test non-linearity
n = 2^11;
iin = linspace(0.02e-9,21e-9,n);
imeas = zeros(size(iin));
codes = zeros(size(iin));
pause(1);
write_fpga_reg(ports.idac_config,bin2dec('100'));
write_dac_v(31,1.5);
pause(3);
for i = 1:n
    i
    codeword = 0;
    jmax = 24;
    itar = iin(i);
    while (itar>=9.2206e-012) && (jmax>0)
        d1 = itar - idac_tb(1:jmax,2);
        d1(d1<0) = 1;
        [a ind] = min(d1);
        codeword = codeword + idac_tb(ind,1);
        itar = a;
        jmax = ind-1;
    end
    write_fpga_reg(ports.idac_val2,bitshift(codeword,-16));
    write_fpga_reg(ports.idac_val1,bitand(codeword,hex2dec('FFFF')));
    activatetriggerin(xem,ports.tr.idac,2); % trig_val
    pause(0.5);
    imeas(i) = read6485(20);
    codes(i) = codeword;
end
plot(iin,abs(imeas),'.-k');
hist(iin-abs(imeas));
save('ID4_IDAC_SORT','imeas','codes');
load ID4_IDAC_SORT

[imeas ind] = sort(abs(imeas));
icodes = codes(ind);
plot(imeas,'.-k');
global idac_imeas;
global idac_codes;
idac_imeas = imeas;
idac_codes = codes;

% local weighted linear regression to remove outliers

%% test write_idac1
% tabulated method is not better than the original

row = 23;
col = 3;
Inject_fast(row,col,30e-9);
set_array_address(row,col);
set_array_mode([2 0 0 1 1 1]);
write_fpga_reg(ports.idac_config,bin2dec('000'));

iin = linspace(0.02e-9,20e-9,100);
iout = zeros(size(iin));
for i = 1:length(iin)
    i
    write_idac1(iin(i));
    set_array_mode([0 1 1 1 1 0]);
    pause(0.01);
    set_array_mode([2 0 0 1 1 0]);
    pause(0.01);
    iout(i) = 0;
    for k = 1:10
        iout(i) = iout(i) + itov_lad_conv_fast_new(0);
    end
    iout(i) = iout(i)/10;
end
plot(iin,iout,'.-k'); grid on;
hist(iout(2:end)-iout(1:end-1),50);

% same experiment but using normal write_dac: compare histograms
iout1 = zeros(size(iin));
for i = 1:length(iin)
    i
    write_idac(iin(i));
    set_array_mode([0 1 1 1 1 0]);
    pause(0.01);
    set_array_mode([2 0 0 1 1 0]);
    pause(0.01);
    iout1(i) = 0;
    for k = 1:10
        iout1(i) = iout1(i) + itov_lad_conv_fast_new(0);
    end
    iout1(i) = iout1(i)/10;
end
plot(iin,iout1,'.-k'); grid on;
figure;
hist(iout1(2:end)-iout1(1:end-1),50);

% same experiment but itov_lad_conv_fast: compare histograms
iout2 = zeros(size(iin));
for i = 1:length(iin)
    i
    write_idac(iin(i));
    set_array_mode([0 1 1 1 1 0]);
    pause(0.01);
    set_array_mode([2 0 0 1 1 0]);
    pause(0.01);
    iout2(i) = 0;
    for k = 1:10
        iout2(i) = iout2(i) + itov_lad_conv_fast(0);
    end
    iout2(i) = iout2(i)/10;
end
plot(iin,iout2,'.-k'); grid on;
hist(iout2(2:end)-iout2(1:end-1),50);
hist(iout2-iin,50);




