function I = read6485(average,settle)
global mcc15;
%read6485    Obtains a current measurement from the Keithley 6485
%            ammeter through the RS-232 interface.
%
%  CAUTION:  Run init6485.m once before using this script!
%
%  usage:  I = read6485([samples[,settle]])

if nargin == 0
    average = 1;
    settle = 0;
elseif nargin == 1
    settle = 0;
elseif nargin == 2
    
else
    help read6485;
end

try
    if settle > 0
        for m=1:settle
        fwrite(mcc15,'READ?');fwrite(mcc15,char(10));
        fscanf(mcc15);
        end
    end

    II(1:average) = 0;
    for index=1:average
        fwrite(mcc15,'READ?');fwrite(mcc15,char(10));
        pause(0.05);
        tmp = fscanf(mcc15,'%s');
        II(index) = str2double(tmp(1:13));
    end
    I=mean(II);
catch exception
    fclose(mcc15);
    mcc15=serial('COM6', 'BaudRate',  9600); 
    fopen(mcc15);
    II(1:average) = 0;
    for index=1:average
        fwrite(mcc15,'READ?');fwrite(mcc15,char(10));
        pause(0.05);
        tmp = fscanf(mcc15,'%s');
        II(index) = str2double(tmp(1:13));
    end
    I=mean(II);
end