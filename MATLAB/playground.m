i% program IDAC to generate specific currents
write_fpga_reg(ports.idac_config,bin2dec('100')); % chose IDAC output mux
write_dac_v(25,2.5);
write_idac(9.8e-9);

% test ITOV by selecting a random floating gate transistor and sweeping its
% gate voltage
row = 9;
col = 0;
set_array_address(row,col);
write_fpga_reg(ports.nn_config,bin2dec('1100111')); % connect to io_drain
write_dac_v(31,2) % connect vgate1 to DAC31 ,think its vgate2
write_dac_v(25,1) % connect black terminal of picoamp to DAC25 & red terminal is conncted "drain" terminal on pcb
vgate = linspace(3,0,20);
ikeith = [];
iitov = [];
for i = 1:length(vgate)
    i
    write_dac_v(31,vgate(i));
    write_fpga_reg(ports.nn_config,bin2dec('1100111'));
    pause(0.1);
    ikeith(i) = abs(read6485(1));
    write_fpga_reg(ports.nn_config,bin2dec('1000111'));
    pause(0.1);
    iitov(i) = itov_lad_conv_fast(0);
end
figure; hold on;
plot(vgate,log10(ikeith),'.-r');
plot(vgate,log10(iitov),'.-b');
plot(vgate,iitov,'.-b');

%%new part
write_dac_v(31,1); % connect vgate1 to DAC31 ,think its vgate2
v_gate = linspace(3,0,20);
iitov = [];
for i = 1:length(v_gate)
    i
    hv_set_vgate(v_gate(i))
    %write_fpga_reg(ports.nn_config,bin2dec('1100111'));
   % pause(0.1);
    %ikeith(i) = abs(read6485(1));
    write_fpga_reg(ports.nn_config,bin2dec('1000111'));
    pause(0.1);
    iitov(i) = itov_lad_conv_fast(0);
end

plot(v_gate,log10(iitov),'.-b');
plot(v_gate,iitov,'.-b');


write_dac_v(25,2);

% lets inject some FGT
row = 4;
col = 0;
set_array_address(row,col);
write_fpga_reg(ports.nn_config,bin2dec('1100101'));
%set_array_mode([3 0 0 1 0 1]);%enallaktika xrhsh sunarthshs set_array_mode
Inject_fast(row,col,30e-9)
write_fpga_reg(ports.nn_config,bin2dec('1000111'));
%set_array_mode([2 0 0 1 1 1]);%enallaktika xrhsh sunarthshs set_array_mode
itov_lad_conv_fast(0)


write_fpga_reg(ports.nn_config,bin2dec('1100111'));
pause(0.1);
ikeith = abs(read6485(1));


% erase selected FGTs
row_addr = [];
col_addr = [];
row_addr = [row_addr; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0;];
col_addr = [col_addr; 2; 4; 6; 8; 10; 12; 14; 16; 18; 20; 22; 24; 26; 28; 30; 32; 34; 36; 38; 40;];
col_addr = [col_addr; 1; 3; 5; 7; 9; 11; 13; 15; 17; 19; 21; 23; 25; 27; 29; 31; 33; 35; 37; 39;];
fgt_list2 = [row_addr col_addr];
fgt_list2 = [23 23];
%curr_eps = [1e-9 1e-9];

tunnel_array1(fgt_list2);
%tunnel_array2(fgt_list2,curr_eps)
write_fpga_reg(ports.nn_config,bin2dec('1000111'));
itov_lad_conv_fast(0)

%% dynamic programming
% 1. inject for maximum anticipated current
row = 0;
col = 3;
set_array_address(row,col);
write_fpga_reg(ports.nn_config,bin2dec('1100101'));
Inject_fast(row,col,20e-9)
write_fpga_reg(ports.nn_config,bin2dec('1000111'));
itov_lad_conv_fast(0)

% 2. connect this FGT into dynamic programming loop
write_fpga_reg(ports.idac_config,bin2dec('000'));

write_fpga_reg(ports.nn_config,bin2dec('0010110')); % configure feedback loop, switch is open

write_idac(1e-9);
write_fpga_reg(ports.nn_config,bin2dec('0011110'));  % switch is closed

 

% 3. disconnect the switch and measure the stored current
write_fpga_reg(ports.nn_config,bin2dec('1000110'));
itov_lad_conv_fast(0)

%% idac
write_fpga_reg(ports.idac_config,bin2dec('100'));

write_fpga_reg(ports.idac_range,bin2dec('00001000')); % set IDAC range
activatetriggerin(xem,ports.tr.idac,1); % trig_range

codeword1 = bitshift(1,11)
write_fpga_reg(ports.idac_val2,bin2dec('001000000000'));
write_fpga_reg(ports.idac_val1,bin2dec('000000000000'));
activatetriggerin(xem,ports.tr.idac,2); % trig_val
write_dac_v(31,2) % idac drain vsource: imeas
read6485(30)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pause(60*60);
Nmeas = 1230;
res = cell(30,42);
pfit = cell(30,42);
car = cell(1,42);
vgate = linspace(3.3,1,30);
ikeith = [];
write_fpga_reg(ports.nn_config,bin2dec('1100111'));
write_dac_v(25,1);

for i = 1:30
    for j = 1:42
        set_array_address(i,j-1);
        for k = 1:length(vgate)
           write_dac_v(31,vgate(k));    
          % write_fpga_reg(ports.nn_config,bin2dec('1100111'));
           pause(0.1);
           ikeith(k) = abs(read6485(5));
        end
        res{i,j}=ikeith;

    end 
end
plot(vgate,res{i,j},'.-k')
save('VTH_chip7','res')

for i = 1:25
    for j = 1:42
       pfit{i,j} = polyfit(vgate(:)',res{i,j},20);     
%        plot(vgate,res{i,j},'.-k');  hold on;
%        plot(vgate,polyval(pfit{i,j},vgate),'.-r')
      
%        pause(0.02);
    end
end


for i = 1:25
    for j = 1:42
 car{i,j} = roots([pfit{i,j}(1:end-1) pfit{i,j}(end)-1e-6]);
     end
end

for i = 1:25
    for j = 1:42
        for p = 1:20
            
            if ((car{i,j}(p,1)>1)&&(car{i,j}(p,1)<3.3)&&(isreal(car{i,j}(p,1))))
                ThresV(i,j) = car{i,j}(p,1);
            end
    
        end
    end
end   

colormap('gray')
hist(ThresV(:),100)
imagesc(ThresV)
HeatMap(ThresV)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% MLP training

% First step: create dataset (XOR3)
X = [-1 -1 -1;
    -1 -1 1;
    -1 1 -1;
    -1 1 1;
    1 -1 -1;
    1 -1 1;
    1 1 -1;
    1 1 1];

Y = [-1; 1; 1; -1; 1; -1; -1; 1]; %desired output

% Creating MLP data structure
nnet = [];%structure that includes other matrixes inside
nnet.bias_layer = [1];
nnet.input_layer = [2 3 4]; % > bias_layer
nnet.hidden_layer = [5 6 7 8]; % > input_layer
nnet.output_layer = [9]; % > hidden_layer <= 20
nnet.NI = 4; % # of input_layer + bias
nnet.NH = length(nnet.hidden_layer); % # of hidden neurons
nnet.ineur = repmat(20e-9,nnet.NH+1,1); % nnet.NH + 1 output neuron , Ineur = 20nA for all neurons
nnet.gm_iadj = repmat(3e-9,nnet.NI,1); % offset current of the BGOTAs
nnet.gm_itail = repmat(30e-9,nnet.NI,1); % biasing current for the BGOTAs
nnet.oswitch = nnet.output_layer*2-2; % the column from which we measure network output
nnet.iscale = [8.*ones(1,length(nnet.hidden_layer)) 10].*10^-9; % output neuron has a different value for iscale compared to hidden    

nnet = InitFG_MLNet(nnet); % this programs the hardware network according to nnet
Preinject_MLNet(nnet);% this injects initial currents to Itail,Iadj,Iscale and weights
itar = CheckPreinject_MLNet1(nnet); % just checking the values of the currents programmed above

Tunnel_MLNet1(nnet) % use cautiosly because it erases the entire array


% training the network
nnet.wnorm{1} = 0.3.*(rand(nnet.NH,nnet.NI)-0.5);
nnet.wnorm{2} = 0.3.*(rand(1,nnet.NH+1)-0.5);
nnet = Train_MLNet_init_iRPROPn(nnet,X,Y,300);

nnet.wnorm{1} = [-0.02261602 0.09700051 0.09664792]
nnet.wnorm{2} = [-0.02418967 -0.1291021]
% test converged network response
LoadVect_MLNet(nnet,X); % download our training set into FPGA memory
LoadWeightsDYN(nnet); % download weight values into FPGA memory
RefreshDYN_MLNet(); % executes dynamic programming
%%Yhat = Forward_MLNet(length(Y),20*2-2);

LoadVect_MLNet(nnet,X);
LoadWeightsDYN(nnet);
RefreshDYN_MLNet();
Yhat = Forward_MLNet(size(X,1),nnet.oswitch);
Yhat = (Yhat(:,1) - Yhat(:,2));
Yhat(Yhat >= 0) = 1;
Yhat(Yhat < 0) = -1;
sum(Y ~= Yhat)/size(X,1)


%% ONN training
% Creating ONN data structure
onnet.input_layer = [1 2 3 4]; % bias is first element then inputs
onnet.output_layer = [10]; % > hidden_layer
onnet.hidden_layer = [5 6 7 8 9]; % > input_layer: preallocate resources that will be used during training
onnet.NI = 4; % number of inputs (including the bias)
onnet.NH = 0;
%output layer goes last in weight array and elsewhere
onnet.w{1} = 0.4*(rand(onnet.NI,1)-0.5); %initial length of output weight vector
onnet.ineur = repmat(20e-9,length(onnet.hidden_layer)+1,1); %for all potential hidden layers
onnet.iscale = [2 4 6 8 10 12].*10^-9;
onnet.gm_iadj = repmat(3e-9,onnet.NI,1);
onnet.gm_itail = repmat(25e-9,onnet.NI,1);
onnet.oswitch = onnet.output_layer*2-2;
onnet = InitFG_ONNet(onnet);

Preinject_ONNet(onnet);
itar = CheckPreinject_ONNet1(onnet);

Tunnel_ONNet1(onnet);

% train ONN
onnet.w{1} = 0.4*(rand(onnet.NI,1)-0.5);
onnet = Train_ONNet_CC_hd(onnet,X,Y,0);

% test converged network response
ind = 2;
onnet.w = onnet.whist{ind};% select configuration when ONN had 1 hidden neuron
onnet.iscale(end) = onnet.iscale(ind+1);
onnet = InitFG_ONNet(onnet);
LoadVect_ONNet(onnet,X);
LoadWeightsDYN_ONN(onnet);
RefreshDYN_MLNet();
Yhat = Forward_ONNet(size(X,1),1,onnet.oswitch);
Yhat = (Yhat(:,1)-Yhat(:,2))./(Yhat(:,1)+Yhat(:,2));
Yhat(Yhat>0) = 1;
Yhat(Yhat<=0) = -1;
sum(Yhat ~= Y)/length(Y)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ONN-XOR2%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

X = [-1 -1;
     -1 1;
     1 -1;
     1 1 ];

Y = [-1; 1; 1; -1]; 

onnet = [];
onnet.input_layer = [1 2 3]; % bias is first element then inputs
onnet.hidden_layer = [4 5];% > input_layer: preallocate resources that will be used during training
onnet.output_layer = [6]; % > hidden_layer
onnet.NI = 3; % input_layer
onnet.NH = 0;
onnet.w{1} = 0.4*(rand(onnet.NI,1)-0.5);
onnet.ineur = repmat(20e-9,length(onnet.hidden_layer)+1,1);
onnet.iscale = [2 4 6].*10^-9;
onnet.gm_iadj = repmat(3e-9,onnet.NI,1);
onnet.gm_itail = repmat(25e-9,onnet.NI,1);
onnet.oswitch = onnet.output_layer*2-2;
onnet = InitFG_ONNet(onnet);
Preinject_ONNet(onnet);
itar = CheckPreinject_ONNet1(onnet);

onnet.w{1} = 0.4*(rand(onnet.NI,1)-0.5);
onnet = Train_ONNet_CC_hd(onnet,X,Y,0);

Tunnel_ONNet1(onnet);

% test converged network response
ind = 2;
onnet.w = onnet.whist{ind};% select configuration when ONN had 1 hidden neuron
onnet.iscale(end) = onnet.iscale(ind+1);
onnet = InitFG_ONNet(onnet);
LoadVect_ONNet(onnet,X);
LoadWeightsDYN_ONN(onnet);
RefreshDYN_MLNet();
%RefreshDYN_ONNet(onnet);
Yhat = Forward_ONNet(size(X,1),1,onnet.oswitch);
Yhat = (Yhat(:,1)-Yhat(:,2))./(Yhat(:,1)+Yhat(:,2));
Yhat(Yhat>0) = 1;
Yhat(Yhat<=0) = -1; 
sum(Yhat ~= Y)/length(Y)


%%%%%%%%%%%%%%%%%%%%%%%%floating gate programming accuracy%%%%%%%%%%%%%%%%%
results = zeros(6,64);
for j=1:6
    j
    for ind=1:64
    
        coords = {[1 2], [1 3], [1 4], [1 5], [1 6], [1 7], [1 8], [1 9], [1 10], [1 11], [1 12], [1 13], [1 14], [1 15], [1 16], [1 17], [1 18], [1 19], [1 20], [1 21], [1 22], [1 23], [1 24], [1 25], [1 26], [1 27], [1 28], [1 29], [1 30], [1 31], [1 32], [1 33], [1 34], [1 35], [1 36], [1 37], [1 38], [1 39], [1 40], [1 41], [2 0], [2 1], [2 4], [2 5], [2 6], [2 7], [2 8], [2 9], [2 10], [2 11], [2 12], [2 13], [2 14], [2 15], [2 16], [2 17], [2 18], [2 19], [2 20], [2 21], [2 22], [2 23], [2 24], [2 25], [2 26], [2 27], [2 28], [2 29], [2 30], [2 31], [2 32], [2 33], [2 34], [2 35], [2 36], [2 37], [2 38], [2 39], [2 40], [2 41], [3 0], [3 1], [3 2], [3 3], [3 6], [3 7], [3 8], [3 9], [3 10], [3 11], [3 12], [3 13], [3 14], [3 15], [3 16], [3 17], [3 18], [3 19], [3 20], [3 21], [3 22], [3 23], [3 24], [3 25], [3 26], [3 27], [3 28], [3 29], [3 30], [3 31], [3 32], [3 33], [3 34], [3 35], [3 36], [3 37], [3 38], [3 39], [3 40], [3 41], [4 0], [4 1], [4 2], [4 3], [4 4], [4 5], [4 8], [4 9], [4 10], [4 11], [4 12], [4 13], [4 14], [4 15], [4 16], [4 17], [4 18], [4 19], [4 20], [4 21], [4 22], [4 23], [4 24], [4 25], [4 26], [4 27], [4 28], [4 29], [4 30], [4 31], [4 32], [4 33], [4 34], [4 35], [4 36], [4 37], [4 38], [4 39], [4 40], [4 41], [5 0], [5 1], [5 2], [5 3], [5 4], [5 5], [5 6], [5 7], [5 10], [5 11], [5 12], [5 13], [5 14], [5 15], [5 16], [5 17], [5 18], [5 19], [5 20], [5 21], [5 22], [5 23], [5 24], [5 25], [5 26], [5 27], [5 28], [5 29], [5 30], [5 31], [5 32], [5 33], [5 34], [5 35], [5 36], [5 37], [5 38], [5 39], [5 40], [5 41], [6 0], [6 1], [6 2], [6 3], [6 4], [6 5], [6 6], [6 7], [6 8], [6 9], [6 12], [6 13], [6 14], [6 15], [6 16], [6 17], [6 18], [6 19], [6 20], [6 21], [6 22], [6 23], [6 24], [6 25], [6 26], [6 27], [6 28], [6 29], [6 30], [6 31], [6 32], [6 33], [6 34], [6 35], [6 36], [6 37], [6 38], [6 39], [6 40], [6 41], [7 0], [7 1], [7 2], [7 3], [7 4], [7 5], [7 6], [7 7], [7 8], [7 9], [7 10], [7 11], [7 14], [7 15], [7 16], [7 17], [7 18], [7 19], [7 20], [7 21], [7 22], [7 23], [7 24], [7 25], [7 26], [7 27], [7 28], [7 29], [7 30], [7 31], [7 32], [7 33], [7 34], [7 35], [7 36], [7 37], [7 38], [7 39], [7 40], [7 41], [8 0], [8 1], [8 2], [8 3], [8 4], [8 5], [8 6], [8 7], [8 8], [8 9], [8 10], [8 11], [8 12], [8 13], [8 16], [8 17], [8 18], [8 19], [8 20], [8 21], [8 22], [8 23], [8 24], [8 25], [8 26], [8 27], [8 28], [8 29], [8 30], [8 31], [8 32], [8 33], [8 34], [8 35], [8 36], [8 37], [8 38], [8 39], [8 40], [8 41], [9 0], [9 1], [9 2], [9 3], [9 4], [9 5], [9 6], [9 7], [9 8], [9 9], [9 10], [9 11], [9 12], [9 13], [9 14], [9 15], [9 18], [9 19], [9 20], [9 21], [9 22], [9 23], [9 24], [9 25], [9 26], [9 27], [9 28], [9 29], [9 30], [9 31], [9 32], [9 33], [9 34], [9 35], [9 36], [9 37], [9 38], [9 39], [9 40], [9 41], [10 0], [10 1], [10 2], [10 3], [10 4], [10 5], [10 6], [10 7], [10 8], [10 9], [10 10], [10 11], [10 12], [10 13], [10 14], [10 15], [10 16], [10 17], [10 20], [10 21], [10 22], [10 23], [10 24], [10 25], [10 26], [10 27], [10 28], [10 29], [10 30], [10 31], [10 32], [10 33], [10 34], [10 35], [10 36], [10 37], [10 38], [10 39], [10 40], [10 41], [11 0], [11 1], [11 2], [11 3], [11 4], [11 5], [11 6], [11 7], [11 8], [11 9], [11 10], [11 11], [11 12], [11 13], [11 14], [11 15], [11 16], [11 17], [11 18], [11 19], [11 22], [11 23], [11 24], [11 25], [11 26], [11 27], [11 28], [11 29], [11 30], [11 31], [11 32], [11 33], [11 34], [11 35], [11 36], [11 37], [11 38], [11 39], [11 40], [11 41], [12 0], [12 1], [12 2], [12 3], [12 4], [12 5], [12 6], [12 7], [12 8], [12 9], [12 10], [12 11], [12 12], [12 13], [12 14], [12 15], [12 16], [12 17], [12 18], [12 19], [12 20], [12 21], [12 24], [12 25], [12 26], [12 27], [12 28], [12 29], [12 30], [12 31], [12 32], [12 33], [12 34], [12 35], [12 36], [12 37], [12 38], [12 39], [12 40], [12 41], [13 0], [13 1], [13 2], [13 3], [13 4], [13 5], [13 6], [13 7], [13 8], [13 9], [13 10], [13 11], [13 12], [13 13], [13 14], [13 15], [13 16], [13 17], [13 18], [13 19], [13 20], [13 21], [13 22], [13 23], [13 26], [13 27], [13 28], [13 29], [13 30], [13 31], [13 32], [13 33], [13 34], [13 35], [13 36], [13 37], [13 38], [13 39], [13 40], [13 41], [14 0], [14 1], [14 2], [14 3], [14 4], [14 5], [14 6], [14 7], [14 8], [14 9], [14 10], [14 11], [14 12], [14 13], [14 14], [14 15], [14 16], [14 17], [14 18], [14 19], [14 20], [14 21], [14 22], [14 23], [14 24], [14 25], [14 28], [14 29], [14 30], [14 31], [14 32], [14 33], [14 34], [14 35], [14 36], [14 37], [14 38], [14 39], [14 40], [14 41], [15 0], [15 1], [15 2], [15 3], [15 4], [15 5], [15 6], [15 7], [15 8], [15 9], [15 10], [15 11], [15 12], [15 13], [15 14], [15 15], [15 16], [15 17], [15 18], [15 19], [15 20], [15 21], [15 22], [15 23], [15 24], [15 25], [15 26], [15 27], [15 30], [15 31], [15 32], [15 33], [15 34], [15 35], [15 36], [15 37], [15 38], [15 39], [15 40], [15 41], [16 0], [16 1], [16 2], [16 3], [16 4], [16 5], [16 6], [16 7], [16 8], [16 9], [16 10], [16 11], [16 12], [16 13], [16 14], [16 15], [16 16], [16 17], [16 18], [16 19], [16 20], [16 21], [16 22], [16 23], [16 24], [16 25], [16 26], [16 27], [16 28], [16 29], [16 32], [16 33], [16 34], [16 35], [16 36], [16 37], [16 38], [16 39], [16 40], [16 41], [17 0], [17 1], [17 2], [17 3], [17 4], [17 5], [17 6], [17 7], [17 8], [17 9], [17 10], [17 11], [17 12], [17 13], [17 14], [17 15], [17 16], [17 17], [17 18], [17 19], [17 20], [17 21], [17 22], [17 23], [17 24], [17 25], [17 26], [17 27], [17 28], [17 29], [17 30], [17 31], [17 34], [17 35], [17 36], [17 37], [17 38], [17 39], [17 40], [17 41], [18 0], [18 1], [18 2], [18 3], [18 4], [18 5], [18 6], [18 7], [18 8], [18 9], [18 10], [18 11], [18 12], [18 13], [18 14], [18 15], [18 16], [18 17], [18 18], [18 19], [18 20], [18 21], [18 22], [18 23], [18 24], [18 25], [18 26], [18 27], [18 28], [18 29], [18 30], [18 31], [18 32], [18 33], [18 36], [18 37], [18 38], [18 39], [18 40], [18 41], [19 0], [19 1], [19 2], [19 3], [19 4], [19 5], [19 6], [19 7], [19 8], [19 9], [19 10], [19 11], [19 12], [19 13], [19 14], [19 15], [19 16], [19 17], [19 18], [19 19], [19 20], [19 21], [19 22], [19 23], [19 24], [19 25], [19 26], [19 27], [19 28], [19 29], [19 30], [19 31], [19 32], [19 33], [19 34], [19 35], [19 38], [19 39], [19 40], [19 41], [20 0], [20 1], [20 2], [20 3], [20 4], [20 5], [20 6], [20 7], [20 8], [20 9], [20 10], [20 11], [20 12], [20 13], [20 14], [20 15], [20 16], [20 17], [20 18], [20 19], [20 20], [20 21], [20 22], [20 23], [20 24], [20 25], [20 26], [20 27], [20 28], [20 29], [20 30], [20 31], [20 32], [20 33], [20 34], [20 35], [20 36], [20 37], [20 40], [20 41]};
        coord = coords{ind};
        row = coord(1);
        col = coord(2);
     
% set_array_address(row,col);
% set_array_mode([2 0 0 1 1 1]);
% initidrain(ind) = itov_lad_conv_fast(0);  

% fgt_list2 = [row col];
% tunnel_array1(fgt_list2);
        Inject_fast(row,col,10e-9);
        set_array_address(row,col);
        set_array_mode([2 0 0 1 1 1]); 
        %results(j,ind) =  itov_lad_conv_fast(0); 
        imeas(ind) = 0;
        
        for im=1:10
            [iloc irng] =  itov_lad_conv_fast(0); 
            imeas(ind) = imeas(ind) + iloc;
        end
        results(j,ind) = imeas(ind)/10; 
 
    end
    
    fgi_list = [];
    for i = 1:2
        % add all synapses from the row
        for j = 0:41
            fgi_list = [fgi_list; [i j 8e-9]]; % down to 5e-9
        end
    end

    set_array_mode([2 0 0 1 1 1]); % meas on
    for ind = 1:size(fgi_list,1)
        sprintf('Tunneling FG #%d',ind)
        set_array_address(fgi_list(ind,1),fgi_list(ind,2));
        idrain = itov_lad_conv_fast(0);
        while idrain >= fgi_list(ind,3)      
            hv_set_vgate(0.1);
            hv_set_vtun(8.5);
            pause(0.1);
            hv_set_vtun(2);
            hv_set_vgate(2.5);      
            pause(0.1);
            idrain = itov_lad_conv_fast(0);
        end
    end  
    
end

for i = 1:100
res1{i}(12,1:2) = res2{i}(12,1:2)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gscatter(vint1(1:378,2),vint1(1:378,1),vint1(1:378,3));hold on;
gscatter(vint1(22:42,3),vint1(1:21,3),vint1(1:21,4));hold on;
gscatter(vint1(22:42,5),vint1(1:21,5),vint1(1:21,6));hold on;
gscatter(vint1(22:42,7),vint1(1:21,7),vint1(1:21,8));hold on;
gscatter(vint1(22:42,9),vint1(1:21,9),vint1(1:21,10));hold on;
gscatter(vint1(22:42,11),vint1(1:21,11),vint1(1:21,12));hold on;
gscatter(vint1(22:42,13),vint1(1:21,13),vint1(1:21,14));hold on;
gscatter(vint1(22:42,15),vint1(1:21,15),vint1(1:21,16));hold on;
gscatter(vint1(22:42,17),vint1(1:21,17),vint1(1:21,18));

gscatter(vint3(43:84,1),vint3(1:42,1),vint3(1:42,2));hold on;
gscatter(vint3(43:84,3),vint3(1:42,3),vint3(1:42,4));hold on;
gscatter(vint3(43:84,3),vint3(1:42,3),vint3(1:42,4));hold on;
gscatter(vint3(43:84,3),vint3(1:42,3),vint3(1:42,4));hold on;
gscatter(vint3(43:84,3),vint3(1:42,3),vint3(1:42,4));hold on;
gscatter(vint3(43:84,3),vint3(1:42,3),vint3(1:42,4));hold on;
gscatter(vint3(43:84,3),vint3(1:42,3),vint3(1:42,4));hold on;
gscatter(vint3(43:84,3),vint3(1:42,3),vint3(1:42,4));hold on;
gscatter(vint3(43:84,3),vint3(1:42,3),vint3(1:42,4));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%create a big matrix of weights%%%%%%%%%%%%%%%

george = [];
for i = 1:20
george(i,:) = horzcat(The_Final_mlp_XOR3{i}.wnorm{1}(1,:),The_Final_mlp_XOR3{i}.wnorm{1}(2,:),The_Final_mlp_XOR3{i}.wnorm{1}(3,:),The_Final_mlp_XOR3{i}.wnorm{1}(4,:),The_Final_mlp_XOR3{i}.wnorm{2}(1,:))
end

for i = 1:21
george2(i) = The_Final_mlp_XOR3{i}.E_hist(1)
end

mean(george2)

result(1,:) = mean(george)


george(:,3)
boxplot(george)
hold on;
boxplot(george(:,2))

%%%%%%%%%%%%%%%%%%%%%%%%MLP without hidden and bias%%%%%%%%%%%%%%%%%%%%%%%%

X = [-1 -1;
     -1 1;
     1 -1;
     1 1 ];

Y = [-1; 1; 1; -1]; 

% Creating MLP data structure
nnet = [];%structure that includes other matrixes inside
nnet.bias_layer = [];
nnet.input_layer = [2]; % > bias_layer
nnet.hidden_layer = []; % > input_layer
nnet.output_layer = [10]; % > hidden_layer <= 20
nnet.NI = 1; % # of input_layer 
nnet.NH = length(nnet.hidden_layer); % # of hidden neurons
nnet.ineur = repmat(20e-9,nnet.NH+1,1); % nnet.NH + 1 output neuron , Ineur = 20nA for all neurons
nnet.gm_iadj = repmat(3e-9,nnet.NI,1); % offset current of the BGOTAs
nnet.gm_itail = repmat(30e-9,nnet.NI,1); % biasing current for the BGOTAs
nnet.oswitch = nnet.output_layer*2-2; % the column from which we measure network output
%nnet.iscale = [8.*ones(1,length(nnet.hidden_layer)) 10].*10^-9; % output neuron has a different value for iscale compared to hidden
nnet.iscale = [8.*ones(1,length(nnet.hidden_layer)) 1].*10^-9; % output neuron has a different value for iscale compared to hidden

nnet = InitFG_MLNet_one_Output_nobiasNeuron(nnet); % this programs the hardware network according to nnet
Preinject_MLNet_one_OutputNeuron(nnet);% this injects initial currents to Itail,Iadj,Iscale and weights
itar = CheckPreinject_MLNet1_one_OutputNeuron(nnet); % just checking the values of the currents programmed above

Tunnel_MLNet1(nnet) % use cautiosly because it erases the entire array

% training the network
nnet.wnorm{1} = 0.3.*(rand(1,nnet.NI)-0.5);
nnet = Train_MLNet_iRPROPn_one_OutputNeuron(nnet,X,Y,500)%remember to change the function LoadVect_MLNet inside!

% test converged network response
LoadVect_MLNet_nobias(nnet,X(:,1)); % download our training set into FPGA memory
LoadWeightsDYN_one_OutputNeuron(nnet); % download weight values into FPGA memory
RefreshDYN_MLNet(); % executes dynamic programming
Yhat = Forward_MLNet(size(X,1),nnet.oswitch);
Yhat = (Yhat(:,1) - Yhat(:,2));
Yhat(Yhat >= 0) = 1;
Yhat(Yhat < 0) = -1;

%%%%%%%%%%%%%%construct characteristic of neuron indirectly, presense of
%%%%%%%%%%%%%%synapses

% nnet.wnorm{1} = -0.7:0.1:0.7;
nnet.wnorm{1} = 0.3;

results = cell(1,1);
results2 = cell(1,1);
hx = 1;
bf = 1;

%for wbias = -0.7:0.05:0.3
%%%%%for wnor = 0.4
%%%for wnor = 0.4 

results = cell(1,1);
hx = 1;

%%% wbias = -0.7:0.1:0.7
%%%for wbias = -0.3:0.025:0
%for wbias = -0.3:0.1:0.0.3
%%%for wbias = -0.2
    %%nnet.wnorm{1}(1) = wbias;

%for wnor = 0.1; 
%for wnor = 0:0.1:0.7 
for wnor = 0.2:0.1:0.6  
    %%%%%wnor
    pause(2)
   nnet.wnorm{1}(1) = wnor;
    i=1;
    Yhat = [];
    Yhat2 = [];
    %for X=-1:0.02:1
    for X=-1:0.05:1    
        pause(2);
        LoadVect_MLNet_nobias(nnet,X); % download our training set into FPGA memory
        %LoadVect_MLNet(nnet,X);
        %%%%%LoadVect_MLNet(nnet,X); % download our training set into FPGA memory
        LoadWeightsDYN_one_OutputNeuron(nnet); % download weight values into FPGA memory
        RefreshDYN_MLNet(); % executes dynamic programming
        Yhat = Forward_MLNet(size(X,1),nnet.oswitch);
        Yhat2(i) = (Yhat(:,1) - Yhat(:,2))
        %Yhat2(i) = (Yhat(:,1)-Yhat(:,2))./(Yhat(:,1)+Yhat(:,2))
        i=i+1;
    end
    results{hx} = Yhat2;
    hx=hx+1;
end

%%results2{bf} =  results ;
%%bf = bf +1;
%%end


X=-1:0.05:1
plot(X,Yhat2,'r')
plot(X,results2{1,1}{1,2})
Yhat2 = results{1,10};
hold on;
    
save('input2output5Yhat_15','results');

for iii = 1:4
    plot(X,results{1,iii});hold on;
end
hold off;

for iii = 1:4
    plot(X,results2{1,1}{1,iii});hold on;
    %plot(X,results2{iii}{1,1});hold on;
end
hold off;


% To check when sober

k = min(Yhat2)+abs((max(Yhat2)-min(Yhat2)))/2
t1 = max(find(Yhat2 < k))
t2 = min(find(Yhat2 > k))
dist1 = Yhat2(t2)-Yhat2(t1);
dist2 = X(t2)-X(t1);
w = 1-(dist1 - (k-Yhat2(t1)))/dist1
l = X(t1)+w*dist2

% Plotting
X=-1:0.05:1
hold on;
plot(X,Yhat2,'r')
vline(l)
hold on;
x=get(gca,'xlim');
plot(x,[k k]);

max(Yhat2)
min(Yhat2)


% loop 
X=-1:0.05:1;
mid_point_array = cell(1,1);
for i=1:15
    k = min(results{1,i})+abs((max(results{1,i})-min(results{1,i})))/2;
    t1 = max(find(results{1,i} < k));
    t2 = min(find(results{1,i} > k));
    dist1 = results{1,i}(t2)-results{1,i}(t1);
    dist2 = X(t2)-X(t1);
    w = 1-(dist1 - (k-results{1,i}(t1)))/dist1;
    l = X(t1)+w*dist2;
    mid_point_array{i,1} = l;
    mid_point_array{i,2} = k;
end    


save('input14output15mid_array','mid_point_array');
% Plotting
X=-1:0.05:1
hold on;
plot(X,results{1,5},'r')
vline(mid_point_array{5,1})
hold on;
x=get(gca,'xlim');
plot(x,[mid_point_array{5,2} mid_point_array{5,2}]);

max(Yhat2)
min(Yhat2)

%%%%%%%%%%%%%%%%%%%%%%%MLP without hidden%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nnet = [];
nnet.bias_layer = [1];
nnet.input_layer = [2 3]; % > bias_layer
nnet.hidden_layer = [];  %%%%%%%%%%%%%before was 4567
nnet.output_layer = [7]; % > CARE!!
nnet.NI = 3; % input_layer + bias
nnet.NH = length(nnet.hidden_layer);
nnet.gm_iadj = repmat(3e-9,nnet.NI,1);
nnet.gm_itail = repmat(30e-9,nnet.NI,1);
nnet.oswitch = nnet.output_layer*2-2;
%nnet.hidden_layer = hidden_layer_vect{8};
%nnet.NH = length(hidden_layer_vect{8});
nnet.ineur = repmat(20e-9,nnet.NH+1,1);
nnet.iscale = [8.*ones(1,length(nnet.hidden_layer)) 1].*10^-9;
% nnet.iscale = iscale_vect{8};
%%nnet = InitFG_MLNet(nnet);
nnet = InitFG_MLNet_one_OutputNeuron(nnet);

%%Preinject_MLNet(nnet);% this injects initial currents to Itail,Iadj,Iscale and weights
Preinject_MLNet_one_OutputNeuron(nnet)
%%itar = CheckPreinject_MLNet1(nnet); % just checking the values of the currents programmed above
itar = CheckPreinject_MLNet1_one_OutputNeuron(nnet); % just checking the values of the currents programmed above
Tunnel_MLNet1(nnet) % use cautiosly because it erases the entire array
 
%%nnet.wnorm{1} = 0.3.*(rand(nnet.NH,nnet.NI)-0.5);
%%nnet.wnorm{2} = 0.3.*(rand(1,nnet.NH+1)-0.5);
%%nnet = Train_MLNet_init_iRPROPn(nnet,X,Y,300);
nnet.wnorm{1} = 0.3.*(rand(1,nnet.NI)-0.5);
nnet = Train_MLNet_iRPROPn_one_OutputNeuron(nnet,X,Y,500)
%Y = vint1(:,3)

% test converged network response
LoadVect_MLNet(nnet,X);
%%LoadWeightsDYN(nnet);
LoadWeightsDYN_one_OutputNeuron(nnet);
RefreshDYN_MLNet();
% set_array_address(15,7);
% write_fpga_reg(ports.nn_config,bin2dec('1000110'));
% itov_lad_conv_fast(0)    
Yhat = Forward_MLNet(size(X,1),nnet.oswitch);
Yhat = (Yhat(:,1) - Yhat(:,2));
Yhat(Yhat >= 0) = 1;
Yhat(Yhat < 0) = -1;
sum(Y ~= Yhat)/size(X,1)


%%%%%%%%%%%%todelete%%%%%%%%%%%%%%%%
%X=-1:0.05:1;
% X=[-1:0.01:1;-1:0.01:1];
% X = X';
%%X = (X.*1.1+2.2);
% x = linspace(-1,1,15)';
% X = [];
% for i = 1:length(x)
%     X = [X; x repmat(x(i),length(x),1)];
% end
% % x = linspace(-1,1,15)';
x = [-1:0.01:1]'
X = [];
%for i = 1:length(x)
    X = [X; x x];
%end
results = cell(1,1);
results2 = cell(1,1);
hx = 1;
bf = 1;

%for wbias = -0.7:0.05:0.3
for wnor = 0.4
%%%for wnor = 0.4 

results = cell(1,1);
hx = 1;

w3 = 0.2
nnet.wnorm{1}(3) = w3;

%%%for wbias = -0.3:0.025:0
%for wbias = -0.3:0.1:0.0.3
for wbias = -0.2
    nnet.wnorm{1}(1) = wbias; 
    wnor
    pause(2)
    nnet.wnorm{1}(2) = wnor;
%     w3 = -0.015
%     nnet.wnorm{1}(3) = w3;
    %i=1;
    Yhat = [];
    Yhat2 = [];
    %for X=-1:0.02:1
    %for X=-1:0.01:1 
        
        pause(2);
        %LoadVect_MLNet_nobias(nnet,X); % download our training set into FPGA memory
        LoadVect_MLNet(nnet,X); % download our training set into FPGA memory
        LoadWeightsDYN_one_OutputNeuron(nnet); % download weight values into FPGA memory
        RefreshDYN_MLNet(); % executes dynamic programming
        Yhat = Forward_MLNet(size(X,1),nnet.oswitch);
        Yhat2 = (Yhat(:,1) - Yhat(:,2))
        %Yhat2 = (Yhat(:,1)-Yhat(:,2))./(Yhat(:,1)+Yhat(:,2))
%         Yhat(Yhat >= 0) = 1;
%         Yhat(Yhat < 0) = -1;
        %i=i+1;
    %end
    results{hx} = Yhat2;
    
%     for k=1:size(xx,1)
%         for m=1:size(xx,2)
%             zz(m,k) = Yhat((k-1)*size(xx,1)+m,1) - Yhat((k-1)*size(xx,1)+m,2);
%         end
%     end
    
    hx=hx+1;
end

results2{bf} =  results ;
bf = bf +1;
end

save('input2output5Yhat_15','results');

for iii = 1:1
    plot(X(:,1),results{1,iii});hold on;
    %plot(george',results{1,iii});hold on;
%     results{1,iii}(results{1,iii} >= 0) = 1;
%     results{1,iii}(results{1,iii} < 0) = -1;
%     plot(X,results{1,iii});hold on;
end
hold off;

for iii = 1:11
    plot(X,results2{1}{1,iii});hold on;
    %plot(X,results2{iii}{1,1});hold on;
end
hold off;

for iii = 1:1
    plot3(X(:,1),X(:,2),results{1,iii});hold on;
    %plot(george',results{1,iii});hold on;
%     results{1,iii}(results{1,iii} >= 0) = 1;
%     results{1,iii}(results{1,iii} < 0) = -1;
%     plot(X,results{1,iii});hold on;
end
hold off;
surf(xx,yy,zz);

plot(results{1,1})

%------------------- draw surface -----------------------------
x = [-1:0.01:1]';
Xtrain = [];
for i = 1:length(x)
    Xtrain = [Xtrain; x x];
end

x = linspace(-1,1,15)';
Xtrain = [];
for i = 1:length(x)
    Xtrain = [Xtrain; x repmat(x(i),length(x),1)];
end

LoadVect_MLNet(nnet,Xtrain);
[xx yy] = meshgrid(x,x);
zz = zeros(size(xx));

LoadWeightsDYN_one_OutputNeuron(nnet);
RefreshDYN_MLNet();
Yhat = Forward_MLNet(size(Xtrain,1),1,onnet.oswitch);

for k=1:size(xx,1)
    for m=1:size(xx,2)
        zz(m,k) = Yhat((k-1)*size(xx,1)+m,1) - Yhat((k-1)*size(xx,1)+m,2);
    end
end
figure; hold on;
surf(xx,yy,zz);
scatter(X(:,1),X(:,2),50,Y,'filled');
hold off;

%------------------------------------------------------------

scatter(X(:,2),X(:,2),50,results{1,1},'filled'); grid on; hold on;


% To check when sober
k = min(results{1,iii})+abs((max(results{1,iii})-min(results{1,iii})))/2
t1 = max(find(results{1,iii} < k))
t2 = min(find(results{1,iii} > k))
dist1 = results{1,iii}(t2)-results{1,iii}(t1);
dist2 = X(t2)-X(t1);
w = 1-(dist1 - (k-results{1,iii}(t1)))/dist1
l = X(t1)+w*dist2

% Plotting
X=-1:0.01:1
hold on;
plot(X,results{1,iii},'r')
vline(l)
hold on;
x=get(gca,'xlim');
plot(x,[k k]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%XOR2%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

X = [-1 -1 -1 -1;
     1 1 1 1;
     -1 -1 -1 -1;
     1 1 1 1;
     -1 -1 -1 -1;
     1  1 1 1;
     -1 -1 -1 -1;
     1 1 1 1;
     -1 -1 -1 -1;
     1 1 1 1;
     -1 -1 -1 -1;
     1  1 1 1;
     -1 -1 -1 -1;
     1 1 1 1;
     -1 -1 -1 -1;
     1 1 1 1;
     -1 1 -1 -1;
     1-1 1 1];
  
Y = [-1; 1; 1; -1; -1; 1; 1; -1; -1; 1; 1; -1; -1; 1; 1; -1; 1; -1]; 


X = [-1 -1;
    -1 1;
    1 -1;
    1 1];
  
Y = [-1; 1; -1; 1]; 

nnet = [];%structure that includes other matrixes inside
nnet.bias_layer = [1];
nnet.input_layer = [2 3]; % > bias_layer
nnet.hidden_layer = [4 5]; % > input_layer
nnet.output_layer = [6]; % > hidden_layer <= 20
nnet.NI = 3; % # of input_layer + bias
nnet.NH = length(nnet.hidden_layer); % # of hidden neurons
nnet.ineur = repmat(20e-9,nnet.NH+1,1); % nnet.NH + 1 output neuron , Ineur = 20nA for all neurons
nnet.gm_iadj = repmat(3e-9,nnet.NI,1); % offset current of the BGOTAs
nnet.gm_itail = repmat(30e-9,nnet.NI,1); % biasing current for the BGOTAs
nnet.oswitch = nnet.output_layer*2-2; % the column from which we measure network output
nnet.iscale = [8.*ones(1,length(nnet.hidden_layer)) 10].*10^-9; % output neuron has a different value for iscale compared to hidden      

nnet = InitFG_MLNet(nnet); % this programs the hardware network according to nnet
Preinject_MLNet(nnet);% this injects initial currents to Itail,Iadj,Iscale and weights
itar = CheckPreinject_MLNet1(nnet); % just checking the values of the currents programmed above
%itar = CheckPreinject_MLNet(nnet)
Tunnel_MLNet1(nnet) % use cautiosly because it erases the entire array

nnet.wnorm{1} = 0.3.*(rand(nnet.NH,nnet.NI)-0.5);
nnet.wnorm{2} = 0.3.*(rand(1,nnet.NH+1)-0.5);
nnet = Train_MLNet_init_iRPROPn(nnet,X,Y,300);

nnet.wnorm{1} = 0.3.*(rand(nnet.NH,nnet.NI)-0.5);
nnet.wnorm{2} = 0.3.*(rand(1,nnet.NH+1)-0.5);
Train_MLNet_init_iRPROPnDeterministic(nnet,X,Y,300,test)

% test converged network response
% x = linspace(-1,1,15)';
% X = [];
% for i = 1:length(x)
%     X = [X; x repmat(x(i),length(x),1)];
% end
LoadVect_MLNet(nnet,X);
% [xx yy] = meshgrid(x,x);
% zz = zeros(size(xx));
LoadWeightsDYN(nnet);
RefreshDYN_MLNet();
%activatetriggerin(xem,ports.tr.forward,1);
Yhat = Forward_MLNet(size(X,1),nnet.oswitch);
%Yhat = Forward_MLNet(size(X,1),nnet.oswitch);
Yhat = (Yhat(:,1) - Yhat(:,2));
Yhat(Yhat >= 0) = 1;
Yhat(Yhat < 0) = -1;
sum(Y ~= Yhat)/size(X,1)
%     for k=1:size(xx,1)
%         for m=1:size(xx,2)
%             zz(m,k) = Yhat((k-1)*size(xx,1)+m,1) - Yhat((k-1)*size(xx,1)+m,2);
%         end
%     end
%  surf(xx,yy,zz);   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save('todelete','nnet');  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GM transfer characteristic

% programm 0s to all muxes
set_array_address(0,50); %!!!! because row_addr = hi_row_addr | this_row_addr
% set to 1 where input needs to be passed
iout_word = hex2dec('FFFFFFFF');
write_fpga_reg(ports.iout_sel_lo,bitand(iout_word,hex2dec('FFFF')));
write_fpga_reg(ports.iout_sel_hi,bitshift(bitand(iout_word,hex2dec('FFFF0000')),-16));
activatetriggerin(xem,ports.tr.iout_sel,1); % iout_sel interface block program
inp_dacs = [ports.dac.vin0 ports.dac.vin1 ports.dac.vin2 ports.dac.vin3 ...
            ports.dac.vin4 ports.dac.vin5 ports.dac.vin6 ports.dac.vin7 ...
            ports.dac.vin8 ports.dac.vin9 ports.dac.vin10 ports.dac.vin11 ...
            ports.dac.vin12 ports.dac.vin13 ports.dac.vin14 ports.dac.vin15 ...
            ports.dac.vin16 ports.dac.vin17 ports.dac.vin18 ports.dac.vin19];

for i = 1:3
    i
    Inject_fast(0,2*i,30e-9); % itail
    Inject_fast(0,2*i+1,3e-9); % iadjust
end
      
npoints = 201;
vinp = linspace(1.1,3.3,npoints);

% X=[-1:0.01:1];
% X = X';
% vinp = (X.*1.1+2.2);

vinn = 2.2;
set_array_mode([2 0 0 0 0 1]);
ioutp = cell(1,1);
ioutn = cell(1,1);
for i = 4:4
    oswitch = 2*i-2;
    set_array_address(31,oswitch);
    for j = 1:npoints
        write_dac_v(inp_dacs(i),vinp(j));
        write_dac_v(ports.dac.vneg,vinn);
        pause(0.02);
        ioutp{i}(j) = itov_lad_conv_fast(1);
        ioutn{i}(j) = itov_lad_conv_fast(2);
    end
end

figure; hold on;
for i = 4:4
    pause(0.5);
    plot(vinp-vinn,ioutn{i},'.-k')
    plot(vinp-vinn,ioutp{i},'.-k')
    plot(vinp-vinn,(ioutp{i}-ioutn{i})./(ioutp{i}+ioutn{i}),'.-k')
end  

% erase selected FGTs
row_addr = [];
col_addr = [];
row_addr = [row_addr; 0; 0; 0; 0];
col_addr = [col_addr; 2; 3; 4; 5];
fgt_list2 = [row_addr col_addr];
tunnel_array1(fgt_list2);
write_fpga_reg(ports.nn_config,bin2dec('1000111'));
itov_lad_conv_fast(0)

set_array_address(0,4);
write_fpga_reg(ports.nn_config,bin2dec('1000111'));
itov_lad_conv_fast(0) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%xor2_weight_prediction%%%%%%%%%%%%%%%%%%%%
X = [-1 -1;
     -1 1;
     1 -1;
     1 1 ];
 
Y = [-1; 1; 1; -1]; 


nnet = [];%structure that includes other matrixes inside
nnet.bias_layer = [1];
nnet.input_layer = [2 3]; % > bias_layer
nnet.hidden_layer = [4 5]; % > input_layer
nnet.output_layer = [6]; % > hidden_layer <= 20
nnet.NI = 3; % # of input_layer + bias
nnet.NH = length(nnet.hidden_layer); % # of hidden neurons
nnet.ineur = repmat(20e-9,nnet.NH+1,1); % nnet.NH + 1 output neuron , Ineur = 20nA for all neurons
nnet.gm_iadj = repmat(3e-9,nnet.NI,1); % offset current of the BGOTAs
nnet.gm_itail = repmat(30e-9,nnet.NI,1); % biasing current for the BGOTAs
nnet.oswitch = nnet.output_layer*2-2; % the column from which we measure network output
nnet.iscale = [8.*ones(1,length(nnet.hidden_layer)) 10].*10^-9; % output neuron has a different value for iscale compared to hidden      

nnet = InitFG_MLNet(nnet); % this programs the hardware network according to nnet
Preinject_MLNet(nnet);% this injects initial currents to Itail,Iadj,Iscale and weights
itar = CheckPreinject_MLNet1(nnet); % just checking the values of the currents programmed above
%Tunnel_MLNet1(nnet) % use cautiosly because it erases the entire array

nnet.wnorm{1} = 0.3.*(rand(nnet.NH,nnet.NI)-0.5);
nnet.wnorm{2} = 0.3.*(rand(1,nnet.NH+1)-0.5);
nnet = Train_MLNet_init_iRPROPn(nnet,X,Y,300);

R = normrnd(0,0.28,50,5)
%save('weigths_matrixand','R');
load weigths_matrixand.mat;

% % medavgresults = cell(1,1);
% % avgresults = cell(1,1);
results = cell(1,1);
results2 = cell(1,1);
for i = 1:50
% % results = cell(1,1);
% % results2 = cell(1,1);
% %     
%     pause(0.2)
    Yhat = [];
    Yhat2 = [];
%%for k = 1:10    
    pause(0.5)
    nnet.wnorm{1} = R(i,1:3);
    nnet.wnorm{2} = R(i,4:5); 
    LoadVect_MLNet(nnet,X);
    LoadWeightsDYN(nnet);
    RefreshDYN_MLNet();
    pause(0.2)
    Yhat = Forward_MLNet(size(X,1),nnet.oswitch);
    %Yhat = (Yhat(:,1)-Yhat(:,2))./(Yhat(:,1)+Yhat(:,2));
    Yhat2 = (Yhat(:,1) - Yhat(:,2));
% %     results{k} = Yhat;
% %     results2{k} = Yhat2;
    results{i} = Yhat
    results2{i} = Yhat2
    % Yhat(Yhat >= 0) = 1;
    % Yhat(Yhat < 0) = -1;
    % sum(Y ~= Yhat)/size(X,1)
end
%avgresults{i} = results2;
%medavgresults{i} = median(results2);
%end

save('NN12378_12Outputsand','results','results2')

rescurr = [];
res2 = cell(50,1);
george = cell(1,1);
george = results2';
for i = 1:50
    res2{i,1} = george{i,1}';
end
rescurr = cell2mat(res2) ;   
outputcurrmatrix = horzcat(R,rescurr);
save('outputrndweightschip12_12378','outputcurrmatrix');
csvwrite('conx3outputsRndchip12_12378.csv',outputcurrmatrix)


nnet.wnorm{1} = 0.3.*(rand(nnet.NH,nnet.NI)-0.5);
nnet.wnorm{2} = 0.3.*(rand(1,nnet.NH+1)-0.5);
nnet = Train_MLNet_init_iRPROPn(nnet,X,Y,300);

resultsaftertraining = cell(1,1);
results2aftertraining = cell(1,1);
Yhat = [];
Yhat2 = [];
LoadVect_MLNet(nnet,X);
LoadWeightsDYN(nnet);
RefreshDYN_MLNet();
Yhat = Forward_MLNet(size(X,1),nnet.oswitch);
Yhat2 = (Yhat(:,1) - Yhat(:,2));
%Yhat = (Yhat(:,1)-Yhat(:,2))./(Yhat(:,1)+Yhat(:,2));
resultsaftertraining = Yhat;
results2aftertraining = Yhat2;
save('NN456131415_15TrWeights','resultsaftertraining','results2aftertraining','nnet')

%%%%%%%%%%%%%%%%%%%%%%%puting data in a big matrix%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load weigths_matrix.mat;

    
files = dir;
filenames = [];
for i = 1:length(files)
    name = files(i).name;
    r = regexp(name, '[N]{2}([0-9_]+)[A-Za-z]+[.]mat', 'tokens');
    if length(r) > 0
        r = r{1,1};
        filenames = [filenames r];
    end
end
filenames = unique(filenames);


MARS_t = [];
for i = 1:length(filenames)
    setup = filenames(i);
    tmp_t = cell(50,27);
    fn1 = strcat('NN', setup{1}, 'TrWeights.mat');
    fn2 = strcat('NN', setup{1}, 'Outputs.mat');
    load(fn1);
    load(fn2);
    finalresults2 = [];
    for k = 1:50
        finalresults2(k,:) =  horzcat(results2{1,k}(1),results2{1,k}(2),results2{1,k}(3),results2{1,k}(4));
    end    
    tmp_t(1:50,1) = repmat({setup}, 50, 1);
    tmp_t(1:50,2:10) = num2cell(R);
    tmp_t(1:50,11:14) = num2cell(finalresults2);
    weights = horzcat(nnet.wnorm{1,1}(1,:),nnet.wnorm{1,1}(2,:),nnet.wnorm{1,2});
    weights = num2cell(weights);
    tmp_t(1:50,15:23) = repmat(weights, 50, 1);
    finalresults2aftertraining = [];
    finalresults2aftertraining = results2aftertraining';
    tmp_t(1:50,24:27) = num2cell(repmat(finalresults2aftertraining, 50,1));
    MARS_t = vertcat(MARS_t, tmp_t);
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Thefinaltable = cell(1,1)
for i=1:50
medavgresults = []    
for p = 1:4
for t = 1:10    
   medavgresults(p,t) = avgresults{1,i}{1,t}(p);

end    
end  

finaltable = [];
for k = 1:4
finaltable(k) = median(medavgresults(k,:))  
end    
    
Thefinaltable{i} = finaltable;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%and%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
   
matrixofweights = cell(size(The_Final_mlp_XOR3,2),1);
%matrixofweights = zeros(size(The_Final_mlp_XOR3,2),5);
finalmatrixofweights11 = [];

%for j=1:12
%     matrixofweights = cell(size(The_Final_mlp_XOR3,2),1);    
    for i=1:length(The_Final_mlp_XOR3)
        matrixofweights{i} = horzcat(The_Final_mlp_XOR3{1,i}.wnorm{1,1},The_Final_mlp_XOR3{1,i}.wnorm{1,2})   
    end
    finalmatrixofweights11 = cell2mat(matrixofweights)
%end    

thefinalmatrixofweigths = [];
thefinalmatrixofweigths = vertcat(finalmatrixofweights1,finalmatrixofweights2,finalmatrixofweights3,finalmatrixofweights4,finalmatrixofweights5,finalmatrixofweights6,finalmatrixofweights7,finalmatrixofweights8,finalmatrixofweights9,finalmatrixofweights10,finalmatrixofweights11)
save('weightsofandchip15_12389','thefinalmatrixofweigths')

finalmatrixofYhat11 = [];
inYhat = cell(length(The_Final_mlp_XOR3),1);
for i=1:length(The_Final_mlp_XOR3)   
    pause(0.5)  
    nnet.wnorm{1} = The_Final_mlp_XOR3{1,i}.wnorm{1,1};
    nnet.wnorm{2} = The_Final_mlp_XOR3{1,i}.wnorm{1,2};
    nnet = InitFG_MLNet(nnet);
    LoadVect_MLNet(nnet,X);
    LoadWeightsDYN(nnet);
    RefreshDYN_MLNet();
    Yhat = Forward_MLNet(size(X,1),nnet.oswitch);
    Yhat = (Yhat(:,1) - Yhat(:,2));
    inYhat{i}=Yhat';
%     Yhat(Yhat >= 0) = 1;
%     Yhat(Yhat < 0) = -1;
%inCERR1{i}(1,iii) = sum(Y ~= Yhat)/length(Y);
end

finalmatrixofYhat11 = cell2mat(inYhat)

thefinalmatrixofYhat = [];
thefinalmatrixofYhat = vertcat(finalmatrixofYhat1,finalmatrixofYhat2,finalmatrixofYhat3,finalmatrixofYhat4,finalmatrixofYhat5,finalmatrixofYhat6,finalmatrixofYhat7,finalmatrixofYhat8,finalmatrixofYhat9,finalmatrixofYhat10,finalmatrixofYhat11)
save('outputsofandchip15_12389','thefinalmatrixofYhat')


finalmatrix = [];
finalmatrix = horzcat(thefinalmatrixofweigths,thefinalmatrixofYhat)
save('conx','finalmatrix')

csvwrite('conx2.csv',finalmatrix)

finalmatrixofsteps10 = [];
matrixofsteps = [];
for i=1:length(The_Final_mlp_XOR3)
    matrixofsteps(i) = length(The_Final_mlp_XOR3{1,i}.cerr_hist)   
end

finalmatrixofsteps10 = matrixofsteps'

thefinalmatrixofsteps = [];
thefinalmatrixofsteps = vertcat(finalmatrixofsteps1,finalmatrixofsteps2,finalmatrixofsteps3,finalmatrixofsteps4,finalmatrixofsteps5,finalmatrixofsteps6,finalmatrixofsteps7,finalmatrixofsteps8,finalmatrixofsteps9,finalmatrixofsteps10)
save('numberofstepschip12_12378','thefinalmatrixofsteps')

finalmatrix2 = [];
finalmatrix2 = horzcat(finalmatrix,thefinalmatrixofsteps )
save('conx3chip15_12389','finalmatrix2')

csvwrite('conx3chip15_12389.csv',finalmatrix2)
csvwrite('numberofsteps_12378.csv',thefinalmatrixofsteps)


george = [];
for i = 1:length(The_Final_mlp_XOR3)
    george(i)=(The_Final_mlp_XOR3{1,i}.cerr==0)
end

sum(george)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
AllWeightsMatrix = [];
for i = 1:5
    for j = 1: length(The_Final_mlp_XOR3{1,i}.w_norm_all)
      AllWeightsMatrix(j,i) = horzcat(The_Final_mlp_XOR3{1,i}.w_norm_all{1,j}{1,1},The_Final_mlp_XOR3{1,i}.w_norm_all{1,j}{1,2});
    end
end    
        
%%%%%%%%%%%%%%%%%%%%%%%%%% many weights%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
count = 1;
results = [];
nnet.wnorm = cell(1,1);
%for i = -0.2:0.05:0.2
for i = 0.2
    i
    nnet.wnorm{1,1}(1) = i;
    for j = -0.2:0.05:0.2
        nnet.wnorm{1,1}(2) = j;
        for k = -0.2:0.05:0.2
            nnet.wnorm{1,1}(3) = k;
            for l = -0.2:0.05:0.2
                nnet.wnorm{1,2}(1) = l;
                for m = -0.2:0.05:0.2
                    nnet.wnorm{1,2}(2) = m;
                    pause(0.4)
                    LoadVect_MLNet(nnet,X);
                    LoadWeightsDYN(nnet);
                    RefreshDYN_MLNet();
                    Yhat = Forward_MLNet(size(X,1),nnet.oswitch);
                    Yhat = (Yhat(:,1) - Yhat(:,2));
                    Yhat(Yhat >= 0) = 1;
                    Yhat(Yhat < 0) = -1;
                    results(count) = sum(Y ~= Yhat)/size(X,1)
                    count = count + 1;
                end
            end
        end
    end
end    
    
allWeightsArea = vertcat(results1,results2,results3,results4,results5,results6,results7,results8,results9)    
csvwrite('manyweightsarea.csv',allWeightsArea)    
%%%%%%%%%%%%%%%%%%%%%%%%%LETSGO%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    
write_dac_v(24,5)   
%write_fpga_reg(ports.dac_addr,bin2dec('111100')); % addr: 001100   
%write_fpga_reg(ports.dac_db,bin2dec('00000000000000')); % addr: 001100    
%write_fpga_reg(ports.dac_reg,bin2dec('00')); % addr: 001100    
write_dac_v(28,3.3)  
write_dac_v(4,3.3)  
write_dac_v(5,3.3)
write_dac_v(6,3.3)
write_dac_v(9,3.3)
write_dac_v(10,1.2)
write_dac_v(36,1.2)

read_adc_v(1)


%%%%%

disp('range=1');
write_fpga_reg(ports.itov_config, bin2dec('00001011'));
write_dac_v(24,0.40);
%vin2 = linspace(0.40,0.80,50);
vin2 = 0.5;
iin2 = [];
vout2 = [];
pause(5);
for i = 1:length(vin2)
    i
    write_dac_v(24,vin2(i));
    pause(0.5);
    iin2(i) = read6485(10);
    vout2(i) = 0;
    for j = 1:30
        vout2(i) = vout2(i) + read_adc_v(1);
        pause(0.02);
    end
    vout2(i) = vout2(i)/30;
end
    
    
    
NNout = 0;
for i=1:1;
for j = 1:30
        NNout(i) = NNout(i) + read_adc_v(1);
        pause(0.02);
    end
    NNout(i) = NNout(i)/30
end
   

write_dac_v(ports.dac.threshold,1.2);


CMPout = 0;
for i=1:1;
for j = 1:30
        CMPout(i) = CMPout(i) + read_adc_v(2);
        pause(0.02);
    end
    CMPout(i) = CMPout(i)/30
end


SH1out = 0;
for i=1:1;
for j = 1:30
        SH1out(i) = SH1out(i) + read_adc_v(3);
        pause(0.02);
    end
    SH1out(i) = SH1out(i)/30
end
  
write_fpga_reg(ports.itov_config, bin2dec('11001011'));
write_fpga_reg(ports.itov_config, bin2dec('00001011'));

SH2out = 0;
for i=1:1;
for j = 1:30
        SH2out(i) = SH2out(i) + read_adc_v(4);
        pause(0.02);
    end
    SH2out(i) = SH2out(i)/30
end
