function InitIC4
global xem;
global ports;

%FPGA Init: internal state machines
activatetriggerin(xem,ports.tr.dac,1); % dac interface block reset
activatetriggerin(xem,ports.tr.adc,0); % adc interface block reset
activatetriggerin(xem,ports.tr.dpulse,0); % dpulse interface block reset
activatetriggerin(xem,ports.tr.idac,0); % idac interface block reset
activatetriggerin(xem,ports.tr.greycounter,0); % grey counter interface block reset

% activatetriggerin(xem,ports.tr.iout_sel,0); % iout_sel interface block reset
% activatetriggerin(xem,ports.tr.dyn_prog,0);
% activatetriggerin(xem,ports.tr.forward,0);


%DAC5380 Init
write_fpga_reg(ports.dac_reg,bin2dec('0')); % reg 1,2 = 0
write_fpga_reg(ports.dac_addr,bin2dec('1100')); % addr: 001100
write_fpga_reg(ports.dac_db,bin2dec('00100000000000')); % boosted output current
activatetriggerin(xem,ports.tr.dac,0); % start writing to dac

%Initialize NN chip
% row = 31;
% col = 63;
row = 10;
col = 31;
set_array_address(row,col);
set_array_mode([2 0 0 0 0 1]); % off mode
%write_fpga_reg(ports.idac_config,fliplr([0 0]));
write_fpga_reg(ports.idac_config,bin2dec('00'));
%write_fpga_reg(ports.itov_config,fliplr([1 1 0 0 0 0 0 0]));
write_fpga_reg(ports.itov_config,bin2dec('00000000'));


%write_fpga_reg(ports.ydig33,[1 1 1 1]);
%write_fpga_reg(ports.ydig12,[1 1 1 1 1 1]);
write_fpga_reg(ports.ydig33,bin2dec('0000'));
write_fpga_reg(ports.ydig12,bin2dec('000000'));
%write_fpga_reg(ports.tb,bin2dec('1111111111111111'));
%write_fpga_reg(ports.tb2,bin2dec('1111111111111111'));


% %Initialize delay parameters
% write_fpga_reg(ports.num_of_dyn_loc,320);
% write_fpga_reg(ports.dyn_prog_time,500); %not used
% write_fpga_reg(ports.readout_time,5);
% write_fpga_reg(ports.fwd_prop_delay,5);
write_fpga_reg(ports.gc_clk_div_ration,100000); 
write_fpga_reg(ports.idac_clk_div_ration,1); % 6 seems ok was 1

%            
% 
% %-----------------------------------------------------------------------
% %------------- Set Operating Point Before Chip PluggedIn ---------------
% %-----------------------------------------------------------------------
% 
hv_set_avdd(3.3);
hv_set_vtun(2);
hv_set_vgate(2.5);
hv_set_sw_bias(2.5);%used to be provided by the dac in the old design
hv_set_vgate2_ext(1);%used to be provided by the dac in the old design
hv_set_IO_drain(2);%used to be provided by the dac in the old design
hv_set_external(3.3); %new signal

write_dac_v(ports.dac.vinjectSub,0.65);%for 26.84nA
write_dac_v(ports.dac.vcasp_in,2.3);%was 2.3 volts
write_dac_v(ports.dac.col_buf,0.5); % for desired current = 10 uA, used to be 0.65V in the old design!!!
write_dac_v(ports.dac.vcascn,1.5);
write_dac_v(ports.dac.ibias2,2.829); % for 3 uA!!!
%write_dac_v(ports.dac.lad_ibias,2.38); % for 10 uA!!!, around 20uA for me
write_dac_v(ports.dac.vneg,2.2);%%%%%%%%%%%%%%%%%%%%was 2.2, vm -> vneg 
write_dac_v(ports.dac.comp5,0.4);write_dac_v(ports.dac.comp4,0.4); % comp5 -> vp0 , comp0 -> vp5 
write_dac_v(ports.dac.comp3,0.4);write_dac_v(ports.dac.comp2,0.4);
write_dac_v(ports.dac.comp1,0.4);write_dac_v(ports.dac.comp0,0.4);
write_dac_v(ports.dac.vbias,2.2);% vp6
%%%%%%%%%%%yichuan%%%%%%%%%%%%%%%%%%%%%%%%%%%
write_dac_v(ports.dac.mode,0);
write_dac_v(ports.dac.threshold,1.2);
write_dac_v(ports.dac.sensor_sel,0);
write_dac_v(ports.dac.sen_ref,0);
write_dac_v(ports.dac.LDO_en,3.3);
write_dac_v(ports.dac.LDOt_en,3.3);
write_dac_v(ports.dac.LDO_sel,1.2);
write_dac_v(ports.dac.VCO_ctrl,1.2);


% write_dac_v(31,1); %connected to vgate2
% write_dac_v(30,2); %connected to io_drain
% write_dac_v(ports.dac.vcasp_in,2.3);%was 2.3 volts
% write_dac_v(ports.dac.sw_bias,2.5);
% write_dac_v(ports.dac.vin2,2.2);write_dac_v(ports.dac.vin3,2.2);
% write_dac_v(ports.dac.vin4,2.2);write_dac_v(ports.dac.vin5,2.2);
% write_dac_v(ports.dac.vin6,2.2);write_dac_v(ports.dac.vin7,2.2);
% write_dac_v(ports.dac.vin8,2.2);write_dac_v(ports.dac.vin9,2.2);
% write_dac_v(ports.dac.vin10,2.2);write_dac_v(ports.dac.vin11,2.2);
% write_dac_v(ports.dac.vin12,2.2);write_dac_v(ports.dac.vin13,2.2);
% write_dac_v(ports.dac.vin14,2.2);write_dac_v(ports.dac.vin15,2.2);
% write_dac_v(ports.dac.vin16,2.2);write_dac_v(ports.dac.vin17,2.2);
% write_dac_v(ports.dac.vin18,2.2);write_dac_v(ports.dac.vin19,2.2);
% write_dac_v(ports.dac.col_buf,0.65); % for desired current = 10 uA!!!
% write_dac_v(ports.dac.dio_ibias,1); % for desired current!!!%ITOV Diode-based signal (Not working)
% write_dac_v(ports.dac.vsh_lt,3);%ITOV Diode-based signal (Not working)
% write_dac_v(ports.dac.vsh_rt,3);%ITOV Diode-based signal (Not working)
% write_dac_v(ports.dac.lad_ibias,2.38); % for 10 uA!!!
% write_dac_v(ports.dac.ibias2,1.58); % for 3 uA!!!
% write_dac_v(ports.dac.vcascn,1.5);
% write_dac_v(ports.dac.dio_ibias,0.7); % for 10uA?%ITOV Diode-based signal (Not working)
% 
% %-------HARD STOP: PLUG YOUR CHIP INTO THE SOCKET -----------------
% %----- after plugging in the CHIP -------------------------
write_fpga_reg(ports.idac_range,bin2dec('00001000')); % set IDAC range
activatetriggerin(xem,ports.tr.idac,1); % trig_range

% updatewireouts(xem);
% code = getwireoutvalue(xem,ports.idac_val1_out)
% code2 = getwireoutvalue(xem,ports.idac_val2_out)
% 
%  write_fpga_reg(ports.idac_val1,bin2dec('100000000000'));
%  write_fpga_reg(ports.idac_val2,bin2dec('000000000001'));
%  activatetriggerin(xem,ports.tr.idac,2); % trig_val
%  updatewireouts(xem);
%  code = getwireoutvalue(xem,ports.idac_val1_out)
%  code2 = getwireoutvalue(xem,ports.idac_val2_out)

%%%grey counter
 activatetriggerin(xem,ports.tr.greycounter,0);
 updatewireouts(xem);
 counter = getwireoutvalue(xem,ports.greycounter_out)

activatetriggerin(xem,ports.tr.greycounter,1); 
for j=1:100
    %pause(0.1);
    updatetriggerouts(xem);
    if istriggered(xem, ports.tro.grey_counter,1)
       updatewireouts(xem);
       counter = getwireoutvalue(xem,ports.greycounter_out) 
       break;
    end
    if (j==100)
        counter = -1
    end
end


% %-----------------------------------------------------------------------
% %------------- Load Calibration Data for ITOV ------------------------------
% %-----------------------------------------------------------------------
% %load ID4_ILAD_CAL.mat;
% global pfit;
% global pfita;
% 
% %load ID3_ILAD_CAL2.mat
% load ID15_ILAD_CAL.mat
% %load ID7_ILAD_CAL.mat
% %load ID8_ILAD_CAL.mat;
% %load ID9_ILAD_CAL.mat;
% %load ID10_ILAD_CAL.mat;
% %load ID11_ILAD_CAL.mat;
% %load ID12_ILAD_CAL.mat;
% %load ID13_ILAD_CAL.mat;
% %load ID14_ILAD_CAL.mat 
% %load ID6_ILAD_CAL.mat
% %load ID16_ILAD_CAL.mat
% %load ID17_ILAD_CAL.mat
% %load ID18_ILAD_CAL.mat
% %load ID19_ILAD_CAL.mat
% %load ID20_ILAD_CAL.mat
% %load ID5b_ILAD_CAL.mat
% %load ID7_ILAD_CAL.mat;
% %load ID9_ILAD_CAL.mat;
% 
% %-----------------------------------------------------------------------
% %------------- Load FG Injection Data ------------------------------
% %-----------------------------------------------------------------------
% global fgfit1;
% global pfitlast;
% load fgfit1;
% 
% 
% %-----------------------------------------------------------------------
% %------------- Load IDAC Data ------------------------------
% %-----------------------------------------------------------------------
% %load ID4_IDAC_CAL.mat
% %load ID3_IDAC_CAL2.mat
% load ID15_IDAC_CAL.mat 
% %load ID7_IDAC_CAL.mat
% %load ID8_IDAC_CAL.mat
% %load ID9_IDAC_CAL.mat
% %load ID10_IDAC_CAL.mat
% %load ID11_IDAC_CAL.mat
% %load ID12_IDAC_CAL.mat
% %load ID13_IDAC_CAL.mat
% %load ID14_IDAC_CAL.mat 
% %load ID6_IDAC_CAL.mat
% %load ID16_IDAC_CAL.mat
% %load ID17_IDAC_CAL.mat
% %load ID18_IDAC_CAL.mat
% %load ID19_IDAC_CAL.mat
% %load ID20_IDAC_CAL.mat
% %load ID5b_IDAC_CAL.mat
% %load ID7_IDAC_CAL.mat
% 
% %The currents generated by each of the 2^24 combinations are measured using
% %external meter and stored in a table along with their codes.The entries
% %are sorted by arranging the currents in increasing order, see also
% %function write_idac.m
global idac_tb;
Nmeas = 2; %was 100
i1 = zeros(Nmeas,12);
i2 = zeros(Nmeas,12);
for i=1:Nmeas
    i1(i,:) = abs(res{i}(:,1));
    i2(i,:) = abs(res{i}(:,2));
end    
idac_tb = zeros(24,2);
idac_tb(1:12,2) = mean(i1)';
idac_tb(13:24,2) = mean(i2)';
for i=1:12
    idac_tb(i,1) = 2^(i-1);
    idac_tb(i+12,1) = bitshift(2^(i-1),16);
end
[a ind] = sort(idac_tb(:,2));
idac_tb1 = idac_tb(ind,:);
idac_tb = idac_tb1;
 
 
% % load ID4_IDACHI_CAL.mat
 %%global idac_tb_hi;
% % Nmeas = 5;
% % i1 = zeros(Nmeas,12);
% % i2 = zeros(Nmeas,12);
% % for i=1:Nmeas
% %     i1(i,:) = abs(res{i}(:,1));
% %     i2(i,:) = abs(res{i}(:,2));
% % end    
% % idac_tb_hi = zeros(24,2);
% % idac_tb_hi(1:12,2) = mean(i1)';
% % idac_tb_hi(13:24,2) = mean(i2)';
% % for i=1:12
% %     idac_tb_hi(i,1) = 2^(i-1);
% %     idac_tb_hi(i+12,1) = bitshift(2^(i-1),16);
% % end
% % [a ind] = sort(idac_tb_hi(:,2));
% % idac_tb1 = idac_tb_hi(ind,:);
% % idac_tb_hi = idac_tb1;
% 
% %Initialize Dynamic Prog Lookup Table
% global dp_lookup;
% %wait_t = code*2^8/48e+6 -> code=wait_t*48Mhz/2^8 why 2^8
% dp_lookup = [0.25; 0.5; 0.75; 2.5; 5; 10; 15; 80].*1e-9;
% % dp_table = [0.25e-9      floor(20e-3*48e+6/2^8);
% %             0.5e-9      floor(10e-3*48e+6/2^8);
% %             0.75e-9     floor(5e-3*48e+6/2^8);
% %             2.5e-9     floor(4e-3*48e+6/2^8);
% %             5e-9     floor(2e-3*48e+6/2^8);
% %             10e-9     floor(1e-3*48e+6/2^8);
% %             15e-9     floor(0.5e-3*48e+6/2^8);
% %             30e-9     floor(0.35e-3*48e+6/2^8)];
% % this one is more conservative
% dp_table = [0.25e-9      floor(20e-3*48e+6/2^8);
%             0.5e-9      floor(10e-3*48e+6/2^8);
%             0.75e-9     floor(5e-3*48e+6/2^8);
%             2.5e-9     floor(4e-3*48e+6/2^8);
%             5e-9     floor(3e-3*48e+6/2^8);
%             10e-9     floor(2e-3*48e+6/2^8);
%             15e-9     floor(1.5e-3*48e+6/2^8);
%             80e-9     floor(1e-3*48e+6/2^8)];
% buf16 = zeros(length(dp_lookup),1);
% for i=1:length(dp_lookup)
%     buf16(i) = bitand(dp_table(i,2),hex2dec('FFFF'));%hex2dec('FFFF')->"11..1"
% end
% buf8 = [];
% for i=1:length(buf16)
%     buf8 = [buf8; uint8(bitand(buf16(i),uint16(255))); uint8(bitshift(buf16(i),-8))];
% end
% activatetriggerin(xem,ports.tr.dyn_prog,0); % reset first
% writetopipein(xem,ports.pipein.dynamic_waittimeram,buf8)



