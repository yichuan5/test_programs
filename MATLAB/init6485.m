function init6485;
global mcc15;
if isobject(mcc15)
    try
        fclose(mcc15);
    end
    delete(mcc15);
end

delete(instrfindall);
instrreset;

mcc15=serial('COM6', 'BaudRate',  9600)
fopen(mcc15);
fwrite(mcc15,'*RST','char');fwrite(mcc15,char(10));
fwrite(mcc15, 'SYST:ZCH ON','char');fwrite(mcc15,char(10));
fwrite(mcc15, 'CURR:RANG 2e-9','char');fwrite(mcc15,char(10));
fwrite(mcc15, 'INIT','char');fwrite(mcc15,char(10));
fwrite(mcc15,'SYST:ZCOR:ACQ');fwrite(mcc15,char(10));
fwrite(mcc15,'SYST:ZCOR ON');fwrite(mcc15,char(10));
fwrite(mcc15,'CURR:RANG:AUTO OFF');fwrite(mcc15,char(10));

% connect picoamm to the circuit
fwrite(mcc15,'SYST:ZCH OFF');fwrite(mcc15,char(10));
fwrite(mcc15,'DISP:ENAB ON');fwrite(mcc15,char(10)); 