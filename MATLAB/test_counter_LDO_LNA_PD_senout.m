write_dac_v(ports.dac.comp0,0.4);
write_dac_v(ports.dac.comp1,0.4);
write_dac_v(ports.dac.comp2,0.4);
write_dac_v(ports.dac.LDO_en,3.3);
write_dac_v(ports.dac.LDOt_en,3.3);
write_dac_v(ports.dac.LDO_sel,1.2);
write_dac_v(ports.dac.sen_ref,1.2);
write_dac_v(ports.dac.sensor_sel,3.3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
repeatADC(6,32) %pd_p
repeatADC(2,32) %pd_n

write_fpga_reg(ports.n_count,1)
activatetriggerin(xem,ports.trigs,0) %count_trig
activatetriggerin(xem,ports.trigs,1) %out_trig
activatetriggerin(xem,ports.trigs,2) %reset_trig

seq= dec2bin(bin2gray(0:63,'psk',64)); %seq of our gray counter
SHA_out= zeros(3,length(seq));
SEN_out=zeros(3,length(seq));
LDO= zeros(3,length(seq));

pd_p =zeros(3,length(seq));
pd_n =zeros(3,length(seq));
for i = 1:length(seq)

    
LDO(1,i)=repeatADC(4,8)/2;%VR0
LDO(2,i)=repeatADC(8,8)/2;%VR1
LDO(3,i)=repeatADC(5,8)/2;%VR2

write_dac_v(ports.dac.VCO_ctrl, 1.8);%%1.3GHz    
pause(1)
pd_p(1,i)=repeatADC(6,32); %pd_p
pd_n(1,i)=repeatADC(2,1); %pd_n
SEN_out(1,i)=repeatADC(1,32);

write_dac_v(ports.dac.VCO_ctrl, 1.6);%%1.6GHz
pause(1)
pd_p(2,i)=repeatADC(6,32); %pd_p
pd_n(2,i)=repeatADC(2,1); %pd_n 
SEN_out(2,i)=repeatADC(1,32);

write_dac_v(ports.dac.VCO_ctrl, 1.1);%%1.7GHz
pause(1)
pd_p(3,i)=repeatADC(6,32); %pd_p
pd_n(3,i)=repeatADC(2,1); %pd_n  
SEN_out(3,i)=repeatADC(1,32);

activatetriggerin(xem,ports.trigs,0)
end

activatetriggerin(xem,ports.trigs,0)

write_dac_v(ports.dac.VCO_ctrl, 1.6);%%1.6GHz
repeatADC(4,32)

write_dac_v(ports.dac.VCO_ctrl, 1.8);%%1.3GHz  
repeatADC(4,32)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






write_fpga_reg(ports.n_count,1)
activatetriggerin(xem,ports.trigs,2) %reset_trig

seq= dec2bin(bin2gray(0:63,'psk',64)); %seq of our gray counter
SHA_out= zeros(3,length(seq));
SEN_out=zeros(3,length(seq));
LDO= zeros(3,length(seq));
for i=1:length(seq)
    
    
    LDO(1,i)=repeatADC(1,8)/2;%VR0
    LDO(2,i)=repeatADC(8,8)/2;%VR1
    LDO(3,i)=repeatADC(5,8)/2;%VR2
    
    write_dac_v(ports.dac.VCO_ctrl, 1.8);%%1.3GHz
%    write_fpga_reg(ports.shc,bin2dec('111'))
%    pause(1)
    SEN_out(1,i)=repeatADC(6,8);%%
    
    
 %   write_fpga_reg(ports.shc,bin2dec('110'))
    write_dac_v(ports.dac.VCO_ctrl, 1.6);%%1.6GHz
%    pause(1)
    SEN_out(2,i)=repeatADC(6,8);%%
 %   write_fpga_reg(ports.shc,bin2dec('100'))
    write_dac_v(ports.dac.VCO_ctrl, 1.3);%%1.7GHz
    pause(1)
    SEN_out(3,i)=repeatADC(6,8);%%
%    write_fpga_reg(ports.shc,bin2dec('000'))
%    pause(1)
%    SHA_out(1,i)=repeatADC(3,32);
%    SHA_out(2,i)=repeatADC(4,32);
%    SHA_out(3,i)=repeatADC(7,32);
    activatetriggerin(xem,ports.trigs,0)    %count_trig
    
    [v1,v2,v3]=counter2voltage(i);
    
   % spec(i)=predict(s21,[v1,v2,v3 x1(i),x2(i),x3(i)]);
   % if spec(i)>10
   %     break
   % end   
end



result=vertcat(a(1:3,:),SHA_out,x2,x3);
result2=vertcat(a(1:3,:),y1,y2,y3);