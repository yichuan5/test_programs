% vsource is connected to vdac25

global pfit;
pfit = cell(5,1);

pause(3600*10);
%% range=0
disp('range=0');
write_fpga_reg(ports.itov_config, bin2dec('00000011'));%first three bits range
write_dac_v(24,0.40);
vin1 = linspace(0.40,0.80,50);
iin1 = [];
vout1 = [];
pause(10);
for i = 1:length(vin1)
    i
    write_dac_v(24,vin1(i));
    pause(1);
    iin1(i) = read6485(10);
    vout1(i) = 0;
    for j = 1:30 % mean value due to noise
        vout1(i) = vout1(i) + read_adc_v(1);
        pause(0.02);
    end
    vout1(i) = vout1(i)/30;
end
 %write_dac_v(25,0.5);
 %figure
 
 %plot(log10(abs(iin1)),log(vout1),'.-k');
 %plot(abs(iin1),vout1,'.-k');
 %grid on;
%%plot(vout1./abs(iin1),'.-k');
iin1 = abs(iin1);     
[iin1 ind] = sort(iin1);
vout1 = vout1(ind);


% fit model (cftool)
% f(x) = p1*x^5 + p2*x^4 + p3*x^3 + p4*x^2 + p5*x + p6
%ind = find(iin < 0.7e-9);
pfit{1} = polyfit(abs(iin1(:)),vout1(:),7); 
plot(abs(iin1),vout1,'.-k'); hold on;
plot(abs(iin1),polyval(pfit{1},abs(iin1)),'.-r'); hold off;

%% range=1
disp('range=1');
write_fpga_reg(ports.itov_config, bin2dec('00001011'));
write_dac_v(24,0.40);
vin2 = linspace(0.40,0.80,50);
iin2 = [];
vout2 = [];
pause(5);
for i = 1:length(vin2)
    i
    write_dac_v(24,vin2(i));
    pause(0.5);
    iin2(i) = read6485(10);
    vout2(i) = 0;
    for j = 1:30
        vout2(i) = vout2(i) + read_adc_v(1);
        pause(0.02);
    end
    vout2(i) = vout2(i)/30;
end
% write_dac_v(25,0.5);
% figure
% plot(log10(abs(iin2)),log(vout2),'.-k');
% plot(abs(iin2),vout2,'.-k');
% grid on;
%plot(vout2./abs(iin2),'.-k');

% fit model (cftool)
% f(x) = p1*x^5 + p2*x^4 + p3*x^3 + p4*x^2 + p5*x + p6
pfit{2} = polyfit(abs(iin2(:)),vout2(:),7);
plot(abs(iin2),vout2,'.-k'); hold on;
plot(abs(iin2),polyval(pfit{2},abs(iin2)),'.-r'); hold off;


%% range=2
disp('range=2');
write_fpga_reg(ports.itov_config, bin2dec('00010011'));
write_dac_v(24,0.4);
vin3 = linspace(0.4,0.8,50);
iin3 = [];
vout3 = [];
pause(5);
for i = 1:length(vin3)
    i
    write_dac_v(24,vin3(i));
    pause(0.5);
    iin3(i) = read6485(10);
    vout3(i) = 0;
    %code3(i) = 0;
    for j = 1:30
        %[vout,code] = read_adc_v(6)
        vout3(i) = vout3(i) + read_adc_v(1);
        %code3(i) = code3(i) + code;
        pause(0.02);
    end
    vout3(i) = vout3(i)/30;
    %code3(i) = code3(i)/30;
end
write_dac_v(25,0.5);
% figure
% plot(log10(abs(iin3)),log(vout3),'.-k');
% plot(abs(iin3),vout3,'.-k');
% grid on;
% plot(vout3./abs(iin3),'.-k');

% fit model (cftool)
% f(x) = p1*x^5 + p2*x^4 + p3*x^3 + p4*x^2 + p5*x + p6
pfit{3} = polyfit(abs(iin3(:)),vout3(:),7);
plot(abs(iin3),vout3,'.-k'); hold on;
plot(abs(iin3),polyval(pfit{3},abs(iin3)),'.-r'); hold off;

%% range=3
disp('range=3');
write_fpga_reg(ports.itov_config, bin2dec('00011011'));
write_dac_v(24,0.5);
vin4 = linspace(0.4,0.8,50);
iin4 = [];
vout4 = [];
pause(5);
for i = 1:length(vin4)
    i
    write_dac_v(24,vin4(i));
    pause(0.5);
    iin4(i) = read6485(10);
    vout4(i) = 0;
    for j = 1:30
        vout4(i) = vout4(i) + read_adc_v(1);
        pause(0.02);
    end
    vout4(i) = vout4(i)/30;
end
%write_dac_v(25,0.5);
% figure
% plot(log10(abs(iin4)),log(vout4),'.-k');
% plot(abs(iin4),vout4,'.-k');
% grid on;
% plot(vout4./abs(iin4),'.-k');

% fit model (cftool)
% f(x) = p1*x^5 + p2*x^4 + p3*x^3 + p4*x^2 + p5*x + p6
pfit{4} = polyfit(abs(iin4(:)),vout4(:),7);
plot(abs(iin4),vout4,'.-k'); hold on;
plot(abs(iin4),polyval(pfit{4},abs(iin4)),'.-r'); hold off;


%% range=4
disp('range=4');
write_fpga_reg(ports.itov_config, bin2dec('00100011'));
write_dac_v(24,0.4);
vin5 = linspace(0.4,0.80,50);
iin5 = [];
vout5 = [];
pause(5);
for i = 1:length(vin5)
    i
    write_dac_v(24,vin5(i));
    pause(1);
    iin5(i) = read6485(5);
    vout5(i) = 0;
    for j = 1:30
        vout5(i) = vout5(i) + read_adc_v(1);
        pause(0.02);
    end
    vout5(i) = vout5(i)/30;
end
%write_dac_v(25,0.5);
% figure
% plot(log10(abs(iin5)),log(vout5),'.-k');
% plot(abs(iin5),vout5,'.-k');
% grid on;
% plot(vout5./abs(iin5),'.-k');

% fit model (cftool)
% f(x) = p1*x^5 + p2*x^4 + p3*x^3 + p4*x^2 + p5*x + p6
pfit{5} = polyfit(abs(iin5(:)),vout5(:),7); %5 not 7?! I should change to 7.
plot(abs(iin5),vout5,'.-k'); hold on;
plot(abs(iin5),polyval(pfit{5},abs(iin5)),'.-r'); hold off;

pfit = pfit';

save('ID1_ILAD_CAL','iin1','iin2','iin3','iin4','iin5','vout1','vout2','vout3','vout4','vout5','pfit');
load('ID4_ILAD_CAL')

% plot all curves together
plot(log10(abs(iin1)),log10(vout1),'.-k'); grid on; hold on
plot(log10(abs(iin2)),log10(vout2),'.-k');
plot(log10(abs(iin3)),log10(vout3),'.-k');
plot(log10(abs(iin4)),log10(vout4),'.-k');
plot(log10(abs(iin5)),log10(vout5),'.-k'); hold off;
set(gca,'YLim',[-0.6 0.6])
set(gca,'YTick',[-0.3979 -0.301 -0.2218 0 0.3802 0.4472])
set(gca,'YTickLabel',{'0.4';'0.5';'0.6';'1';'2.4';'2.8'})
%csvwrite('nnls_ilad_fit.csv',[abs([iin1 iin2 iin3 iin4 iin5]') [vout1 vout2 vout3 vout4 vout5]' [1.*ones(50,1);2.*ones(50,1);3.*ones(50,1);4.*ones(50,1);5.*ones(50,1)]]);

%% alternative fit: no inverse function calculation
pfita{1} = polyfit(vout1(:),abs(iin1(:)),12);
plot(abs(iin1),vout1,'.-k'); hold on;
plot(polyval(pfita{1},vout1),vout1,'.-r'); hold off;

pfita{2} = polyfit(vout2(:),abs(iin2(:)),12);
plot(abs(iin2),vout2,'.-k'); hold on;
plot(polyval(pfita{2},vout2),vout2,'.-r'); hold off;

pfita{3} = polyfit(vout3(:),abs(iin3(:)),12);
plot(abs(iin3),vout3,'.-k'); hold on;
plot(polyval(pfita{3},vout3),vout3,'.-r'); hold off;

pfita{4} = polyfit(vout4(:),abs(iin4(:)),12);
plot(abs(iin4),vout4,'.-k'); hold on;
plot(polyval(pfita{4},vout4),vout4,'.-r'); hold off;

pfita{5} = polyfit(vout5(:),abs(iin5(:)),12);
plot(abs(iin5),vout5,'.-k'); hold on;
plot(polyval(pfita{5},vout5),vout5,'.-r'); hold off;

plot(log10(polyval(pfita{1},vout1)),log10(vout1),'.-k'); grid on; hold on
plot(log10(polyval(pfita{2},vout2)),log10(vout2),'.-k');
plot(log10(polyval(pfita{3},vout3)),log10(vout3),'.-k');
plot(log10(polyval(pfita{4},vout4)),log10(vout4),'.-k');
plot(log10(polyval(pfita{5},vout5)),log10(vout5),'.-k');
set(gca,'YLim',[-0.6 0.6])
set(gca,'YTick',[-0.3979 -0.301 -0.2218 0 0.3802 0.4472])
set(gca,'YTickLabel',{'0.4';'0.5';'0.6';'1';'2.4';'2.8'})

%% test fitted model: choose FG, sweep its vgate and record drain current
row = 23;
col = 3;
set_array_address(row,col);
Inject_slow([row col 40e-9]);
set_array_mode([2 0 0 1 1 1]);
itov_lad_conv(3,0)
itov_lad_conv_new(3,0)
itov_lad_conv_fast(0)
itov_lad_conv_fast_new(0)

write_dac_v(30,0.7);
n = 30;
vin = linspace(10^-3,10^-1,n);
vin = -log10(vin);
imeas = zeros(n,3);
pause(0.1);
for i = 1:n
    i
    write_dac_v(31,vin(i));
    set_array_mode([3 0 0 1 1 1]);
    pause(0.2);
    imeas(i,1) = abs(read6485(3));
    set_array_mode([2 0 0 1 1 1]);
    pause(0.1);
    imeas(i,2) = itov_lad_conv_fast(0);
    pause(0.01);
    imeas(i,3) = itov_lad_conv_fast_new(0);
end

plot(imeas(:,1),'.-k'); grid on; hold on;
plot(imeas(:,2),'.-b');
plot(imeas(:,3),'.-r');

plot(imeas(:,1),imeas(:,2),'.-k'); grid on; hold on;
plot(imeas(:,1),imeas(:,3),'.-r');
plot(log10(imeas(:,1)),log10(imeas(:,2)),'.-k'); grid on; hold on;
plot(log10(imeas(:,1)),log10(imeas(:,3)),'.-r');

plot(imeas(:,1)-imeas(:,2),'.-k');
plot(imeas(:,1)-imeas(:,3),'.-k');

%% write using IDAC into dynamic cap and read
row = 4;
col = 7;
set_array_address(row,col);
set_array_mode([2 0 0 1 1 1]);
write_fpga_reg(ports.idac_config,bin2dec('000'));

iin = linspace(0.02e-9,21e-9,500);
iout = zeros(size(iin));
for i = 1:length(iin)
    i
    write_idac(iin(i));
    set_array_mode([0 1 1 1 1 0]);
    pause(0.01);
    set_array_mode([2 0 0 1 1 0]);
    pause(0.01);
    iout(i) = itov_lad_conv_fast(0);
end
plot(iin,iout,'.-k'); grid on;
hist(iout-iin,50);


%% evaluate model
v = 0.6;
iin = roots([pfit{1}(1:end-1) pfit{1}(end)-v]);
ihat = 0;
for i=1:length(iin)
    if ((iin(i)>1e-12)&(iin(i)<0.7e-9)&isreal(iin(i)))
        ihat = iin(i);
    end
end
v = 2.4;
iin = roots([pfit{2}(1:end-1) pfit{2}(end)-v]);
ihat = 0;
for i=1:length(iin)
    if ((iin(i)>400e-12)&(iin(i)<3e-9)&isreal(iin(i)))
        ihat = iin(i);
    end
end
v = 0.5;
iin = roots([pfit{3}(1:end-1) pfit{3}(end)-v]);
ihat = 0;
for i=1:length(iin)
    if ((iin(i)>1e-9)&(iin(i)<9e-9)&isreal(iin(i)))
        ihat = iin(i);
    end
end
v = 2.4;
iin = roots([pfit{4}(1:end-1) pfit{4}(end)-v]);
ihat = 0;
for i=1:length(iin)
    if ((iin(i)>5e-9)&(iin(i)<35e-9)&isreal(iin(i)))
        ihat = iin(i);
    end
end
v = 0.5;
iin = roots([pfit{5}(1:end-1) pfit{5}(end)-v]);
ihat = 0;
for i=1:length(iin)
    if ((iin(i)>15e-9)&(iin(i)<200e-9)&isreal(iin(i)))
        ihat = iin(i);
    end
end

%% test itov_lad_conv function
% choose a fg source, sweep its vgate measuring its drain both ways: fom
% external idrain pin and internally.
% vgate1 is connected to vdac25
% vgate2 is connected to vdac26

row = 23;
col = 3;
set_array_address(row,col);
write_fpga_reg(ports.nn_config,bin2dec('1100111')); % connect to io_drain
hv_set_vgate(0.2);
write_dac_v(25,0.5);
write_dac_v(26,1);

iin = [];
ihat = [];
range = []
vgate2 = linspace(3,0.7,100);
pause(3);
for i = 1:length(vgate2)
    i
    write_dac_v(26,vgate2(i));
    write_fpga_reg(ports.nn_config,bin2dec('1100111')); % connect to io_drain
    pause(0.3);
    iin(i) = read6485(3);
    write_fpga_reg(ports.nn_config,bin2dec('1000111')); % connect to ITOV
    pause(0.3);
    [ihat(i) range(i)] = itov_lad_conv(3,0);
end
write_dac_v(26,2);
plot(iin,ihat,'.-k'); grid on
plot(log10(iin),log10(ihat),'.-k'); grid on
plot(iin,range); grid on
    
write_fpga_reg(ports.itov_config, bin2dec('000000'));
write_fpga_reg(ports.nn_config,bin2dec('1000111')); % connect to ITOV
write_fpga_reg(ports.nn_config,bin2dec('1100111')); % connect to io_drain
write_dac_v(26,3);
pause(1);
write_dac_v(26,2.5);
for i=1:500
    read_adc_v(8)
end

%% measure rise and fall times of itov output with external cap
% tfall = 0.9s, trise = 0.8s
vgate2_lo = 2;
vgate2_hi = 1.45;
trise = [];
tfall = [];
vrise = [];
vfall = [];
write_dac_v(31,vgate2_lo);
pause(2);
write_dac_v(31,vgate2_hi);
tic;
for i=1:100
    if i==10
        write_dac_v(31,vgate2_hi);
    end
    vrise(i) = read_adc_v(6);
    trise(i) = toc;
end
plot(trise,vrise); grid on;
    
write_dac_v(31,vgate2_hi);
pause(2);
write_dac_v(31,vgate2_lo);
tic;
for i=1:300
    vfall(i) = read_adc_v(6);
    tfall(i) = toc;
end    
plot(tfall,vfall); grid on;

vrise = [];
trise = [];
write_fpga_reg(ports.itov_config,bin2dec('010000'));
pause(0.3);
tic;
for i=1:100
    if i==10
        write_fpga_reg(ports.itov_config,bin2dec('011000'));
    end
    vrise(i) = read_adc_v(6);
    trise(i) = toc;
end
plot(trise,vrise); grid on;
write_fpga_reg(ports.nn_config,bin2dec('1100111')); % connect to io_drain
write_fpga_reg(ports.nn_config,bin2dec('1000111')); % connect to ITOV
write_dac_v(31,2.7)
    
%test new conversion function
write_dac_v(25,0.7);
vgate2 = linspace(3,0.7,100);
pause(10);
iout1 = [];
iout2 = [];
iout3 = [];
range2 = [];
range3 = [];
for i=1:length(vgate2)
    i
    write_dac_v(31,vgate2(i));
    write_fpga_reg(ports.nn_config,bin2dec('1100111')); % connect to io_drain
    pause(0.1);
    iout1(i) = read6485(3);
    write_fpga_reg(ports.nn_config,bin2dec('1000111')); % connect to ITOV
    pause(0.1);
    [iout2(i) iout3(i) ioutp_v ioutm_v range2(i) range3(i)] = read_adc_iout();
end
plot(iout1,iout3,'.-k'); grid on; hold on;
plot(iout1,iout1,'.-r');
plot(iout1,-log2(abs(iout3-iout1)./iout1));
hist((iout2-iout1)./iout1,100)
plot(log10(iout2),'.-k')











