function BoardInit()
% FPGA: fast version

clear all;
addpath('C:\Users\yxl079200\Desktop\mono_FPGA\OpalKellyFunctions');


% % if libisloaded('okFrontPanel')
% %     unloadlibrary('okFrontPanel');
% % end
% % loadlibrary('okFrontPanel','okFrontPanelDLL.h');
% % % libfunctionsview('okFrontPanel')
% % 
% % global xem;
% % global ports;
% % 
% % %okptr = calllib('okFrontPanel','okUsbFrontPanel_Construct')
% % okptr = calllib('okFrontPanel','okFrontPanel_Construct')%1.Create an instance of okFrontPanel.
% % xem = okusbfrontpanel(okptr)%2.Using the Device Interaction methods, find an appropriate XEM with which to communicate and open that device.
% % xem = openbyserial(xem,'')
% % loaddefaultpllconfiguration(xem)%3.Configure the device PLL (for devices with an on-board PLL).
% % 
% % % Configure FPGA
% % configurefpga(xem,'top_auto.bit')%load bitfile to fpga(interface PC NN),4. Download a configuration file to the FPGA using ConfigureFPGA(...).

%first step
global xem;
global ports;

xem.ptr = 0;
xem.serial = '';
xem.deviceID = '';
xem.major = 0;
xem.minor = 0;
xem.brdModel = '';

%second step
if libisloaded('okFrontPanel')
    unloadlibrary('okFrontPanel');
end
loadlibrary('okFrontPanel','okFrontPanelDLL.h');

%third step
xem.ptr = calllib('okFrontPanel','okFrontPanel_Construct');

%4 step
[ret,x] = calllib('okFrontPanel','okFrontPanel_OpenBySerial', xem.ptr, xem.serial);
pause(3)
%5 step
xem.major = calllib('okFrontPanel','okFrontPanel_GetDeviceMajorVersion', xem.ptr);
%6 step
%xem.minor = calllib('okFrontPanel','okFrontPanel_GetDeviceMinorVersion', xem.ptr);
% xem.serial = calllib('okFrontPanel', 'okFrontPanel_GetSerialNumber', xem.ptr, ' ');  % dont run
% xem.deviceID = calllib('okFrontPanel', 'okFrontPanel_GetDeviceID', xem.ptr, ' ');   % sont run
%7 step
%xem.brdModel = calllib('okFrontPanel', 'okFrontPanel_GetDeviceListModel', xem.ptr,1);
%8 step might pop up an error
%xem = okusbfrontpanel(xem.ptr); 
%loaddefaultpllconfiguration(xem);

%9 step
% Configure FPGA
configurefpga(xem,'top_auto.bit')
% 
% ports.n_count=hex2dec('00');
% ports.trigs=hex2dec('40');
% ports.counterout=hex2dec('20');
% ports.counterdone=hex2dec('21');
% 
% updatewireouts(xem)
% 
% write_fpga_reg(ports.n_count,4)
% activatetriggerin(xem,ports.trigs,0)
% activatetriggerin(xem,ports.trigs,1)
% activatetriggerin(xem,ports.trigs,2)
% 
% updatewireouts(xem)
% getwireoutvalue(xem,ports.counterdone)
% getwireoutvalue(xem,ports.counterout)



%********** Port Configuration Regs **********************
%the ones with ('number') are connected to the design the rest are matlab
%variables
% XEM WireIn Ports!!for some reason these are dec instead of HEX!!
ports.dac_reg = 0;%2bit
ports.dac_addr = 1;%6bit
ports.dac_db = 2;%14bit
ports.idac_range = 3;%range[7:0]
ports.idac_val1 = 4;%[11:0]
ports.idac_val2 = 5;%[11:0]
ports.ydig33 = 6;%yichuan3.3 NN_sel,SH3_C,SH2_C,SH1_C
ports.ydig12 = 7;%C_CLK,C_RST,RST_REG,WRITE_REG,REG_CLK,SREG_CLK     
% ports.dp_length_lo = 8;%dpulse_control length, 1bit
ports.nn_address = 9;%ROW_ADDR<3:0>,COL_ADDR<4:0> , Format: row[8:5],col[4:0]9 bits
ports.nn_config = 10;%DSEL<1:0>,GSEL0,ADDR_EN,PROG,MEAS,LOCK
ports.idac_config = 11;%IDAC_SEL,IMASTER_SEL
ports.itov_config = 12;%RSHCLOCK<1:0>,ITOV_RSEL<2:0>,ITOV_INP_SEL<2:0>
% ports.num_of_dyn_loc = 13; %2048 locations max,number of weights, 11 bits
% ports.dyn_prog_time = 14; %16-bit word: [8b this 8b] at 48 MHz %!!!!!!!!!!this seems not to have been declared!!!!!!!In top_auto.v its commented
% ports.readout_time = 15; %16-bit word. delay before measuring FG current
% ports.num_of_vec = 16;%number of vectors in the training set,FORWARD CONTROL
% ports.num_of_vin = 17; %number of features per pattern,FORWARD CONTROL
% ports.fwd_prop_delay = 18; %delay before ITOV reads the output,FORWARD CONTROL
ports.idac_clk_div_ration = 19;%IDAC INTERFACE , 8 bits
% ports.dp_length_hi = 20;%dpulse_control length_hi, 8 bits
ports.gc_clk_div_ration = 29;%Grey Counter INTERFACE, 8 bits
ports.tb2 = 30;
% ports.tb = 31;

 % XEM WireOut Ports
 ports.adc_out = hex2dec('20'); %output of ADC conversion, 16b
 ports.idac_val1_out = hex2dec('21'); %lower 12-bits from IDAC scan-out port
 ports.idac_val2_out = hex2dec('22'); %upper 12-bits from IDAC scan-out port
 ports.adc_ioutp_val = hex2dec('23'); %conversion result from IOUTP->ILAD->ADC
 ports.adc_ioutm_val = hex2dec('24'); %conversion result from IOUTM->ILAD->ADC
 ports.adc_ioutp_rng = hex2dec('25'); %what range of ILAD was used
 ports.adc_ioutm_rng = hex2dec('26'); %what range of ILAD was used
 ports.greycounter_out = hex2dec('27'); % 6-bit grey counter output
 
 
% % XEM TriggerIn Ports
 ports.tr.dac = hex2dec('40'); %DAC INTERFACE: 0-start,1-reset
 ports.tr.adc = hex2dec('41'); %ADC AUTO:0-reset,1to8-start_conv_1to8,9-convert both IOUTP/M,10-single conversion, input selected by mux
 ports.tr.idac = hex2dec('42'); %IDAC: 0-reset,1-trig range,2-trig val
 ports.tr.greycounter = hex2dec('43'); %TOPOLOGY CONFIG:0-reset,1-trig
 ports.tr.dpulse = hex2dec('44'); %DPULSE: 0-reset,1-set to 0,2-set to 1,3-start
% ports.tr.dyn_prog = hex2dec('45'); %DYNAMIC PROG: 0-reset,1-start_prog,2-start_readout
% ports.tr.forward = hex2dec('46'); %FORWARD: 0-reset,1-start_forward


% % XEM TriggerOut Ports: COUNTING STARTS FROM 1
 ports.tro.adc = hex2dec('60'); %ADC: 1-adc_done,2-iout_conv_done
 ports.tro.grey_counter = hex2dec('61'); % 1-done, most prob delete this
% ports.tro.forward = hex2dec('61'); %FORWARD: 1-done
% ports.tro.dynamic = hex2dec('62'); %DYNAMIC: 1-prog_done,2-readout_done






% 
% 
% % XEM PipeIn Ports
% ports.pipein.forward_data = hex2dec('80'); %16K of 14b words (DAC codes). 16K/(num_of_vin) - maximum number of patterns
% ports.pipein.forward_addr = hex2dec('81'); %20 locations. COntains addresses (6bit) of input feature DAC channels
% ports.pipein.dynamic_fgaddr = hex2dec('82'); %2K locations: addresses of FGs to be programmed. Format: row[10:6],col[5:0]
% ports.pipein.dynamic_idacval_lo = hex2dec('83'); %2K locations: 12 bit words for IDAC lower part
% ports.pipein.dynamic_idacval_hi = hex2dec('84'); %2K locations: 12 bit words for IDAC upper part
% ports.pipein.dynamic_waittimeram = hex2dec('85'); %16 locations: 16 bit wait-time look-up table
% 
% 
% % XEM PipeOut Ports
% ports.pipeout.forward_pval = hex2dec('A0'); %4K locations for ADC codes of IOUTP
% ports.pipeout.forward_mval = hex2dec('A1'); %4K locations for ADC codes of IOUTM
% ports.pipeout.forward_range = hex2dec('A2'); %4K locations for ILAD range. Format: [MRANGE[5:3] PRANGE[2:0]]
% ports.pipeout.dynamic_val = hex2dec('A3'); %2K locations. Contrains FG currents readouts (16 bit ADC codes)
% ports.pipeout.dynamic_range = hex2dec('A4'); %2K locations. Contrains ranges of FG currents.
% 
% 
% % DAC channels
% ports.dac.sw_bias = 0;%part of the sample and hold circuit in the CSC
% ports.dac.vcasp_in = 1;
% ports.dac.vneg = 2;
% ports.dac.vin2 = 3;
  ports.dac.LDO_en = 4;
% ports.dac.vin1 = 4;
  ports.dac.LDOt_en = 5;
% ports.dac.avdd = 5;
  ports.dac.LDO_sel = 6;
% ports.dac.vgate1 = 6;
  ports.dac.vbias = 7;
% ports.dac.vin8 = 7;
  ports.dac.vneg = 8;
% ports.dac.vin13 = 8;
  ports.dac.mode = 9;
% ports.dac.vin12 = 9;
  ports.dac.threshold = 10;
% ports.dac.vin15 = 10;
  ports.dac.vcasp_in = 11;
% ports.dac.vin16 = 11;
  ports.dac.VCO_ctrl = 12;
% ports.dac.vin14 = 12;
  ports.dac.vinjectSub = 13;
% ports.dac.vin18 = 13;
  ports.dac.vcascn = 14;
% ports.dac.vin17 = 14;
  ports.dac.col_buf = 15;%Gate2 signal
% ports.dac.col_buf = 15;%Gate2 signal
  ports.dac.vtun = 16;
% ports.dac.vin19 = 16;
  ports.dac.ibias2 = 17;
% ports.dac.dio_ibias = 17;%ITOV Diode-based signal (Not working)
  ports.dac.vgate2_ext = 18;
% ports.dac.vsh_lt = 18;%ITOV Diode-based signal (Not working)
  ports.dac.vgate1 = 19;
% ports.dac.vsh_rt = 19;%ITOV Diode-based signal (Not working)
  ports.dac.IO_drain = 20;
% ports.dac.lad_ibias = 20;%ITOV Ladder-based signal
  ports.dac.avdd = 21;
% ports.dac.vcascn = 21;%IDAC signal
  ports.dac.external = 22;
% ports.dac.ibias2 = 22;%IDAC signal
  ports.dac.sw_bias = 23;
% ports.dac.vtun = 24;
% ports.dac.vin3 = 28;
% ports.dac.vin0 = 29;
  ports.dac.comp0 = 32;
% ports.dac.vin4 = 32;
  ports.dac.comp1 = 33;
% ports.dac.vin6 = 33;
  ports.dac.comp2 = 34;
% ports.dac.vin7 = 34;
  ports.dac.comp3 = 35;
% ports.dac.vin5 = 35;
  ports.dac.sen_ref = 36;
% ports.dac.vin9 = 36;
  ports.dac.comp4 = 37;
% ports.dac.vin10 = 37;
  ports.dac.comp5 = 38;
% ports.dac.vin11 = 38;
  ports.dac.sensor_sel = 39;


end