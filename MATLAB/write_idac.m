function write_idac(itar)
global idac_tb;
global ports;
global xem;
%To program a target current ,its value is used to find an entry from the
%table with the closest current value.The code of the found entry and the
%range are then written into the idac_register
%-new- To generate a target current, this table is used to find a subset of
%branches whose combined current closely matches the target value
codeword = 0;
jmax = 24;
while (itar>=9.2206e-012) && (jmax>0)
    d1 = itar - idac_tb(1:jmax,2);
    d1(d1<0) = 1;%ignore the currents that are larger than the itar
    [a ind] = min(d1);%a is the current value whereas ind is the position(1-24) in the array
    codeword = codeword + idac_tb(ind,1);
    itar = a;
    jmax = ind-1;
end
%32 bit registers.First 12 bits for splitter1, next 4 dummy and then next
%12, thats why bitshift(16)
write_fpga_reg(ports.idac_val2,bitshift(codeword,-16));
write_fpga_reg(ports.idac_val1,bitand(codeword,hex2dec('FFFF')));
activatetriggerin(xem,ports.tr.idac,2); % trig_val
%Algorithm is not restricted to the 24 initial codeworks but it dynamically
%creates new ones
%Also note the ports.idac_range has been initialised in InitIC4 along with
%activatetriggerin(xem,ports.tr.idac,1); % trig_range

