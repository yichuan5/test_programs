clear all;
addpath('C:\Users\yxl079200\Desktop\mono_FPGA\OpalKellyFunctions');
global xem;
global ports;

xem.ptr = 0;
xem.serial = '';
xem.deviceID = '';
xem.major = 0;
xem.minor = 0;
xem.brdModel = '';
if libisloaded('okFrontPanel')
    unloadlibrary('okFrontPanel');
end
loadlibrary('okFrontPanel','okFrontPanelDLL.h');
xem.ptr = calllib('okFrontPanel','okFrontPanel_Construct');
[ret,x] = calllib('okFrontPanel','okFrontPanel_OpenBySerial', xem.ptr, xem.serial);
pause(3)
xem.major = calllib('okFrontPanel','okFrontPanel_GetDeviceMajorVersion', xem.ptr);

configurefpga(xem,'top_auto.bit')

ports.dac_reg = 0;%2bit
ports.dac_addr = 1;%6bit
ports.dac_db = 2;%14bit
ports.idac_range = 3;%range[7:0]
ports.idac_val1 = 4;%[11:0]
ports.idac_val2 = 5;%[11:0]
ports.shc=hex2dec('17');%[3:0]

% XEM WireOut Ports
ports.adc_out = hex2dec('20'); %output of ADC conversion, 16b
ports.idac_val1_out = hex2dec('21'); %lower 12-bits from IDAC scan-out port
ports.idac_val2_out = hex2dec('22'); %upper 12-bits from IDAC scan-out port
ports.adc_ioutp_val = hex2dec('23'); %conversion result from IOUTP->ILAD->ADC
ports.adc_ioutm_val = hex2dec('24'); %conversion result from IOUTM->ILAD->ADC
ports.adc_ioutp_rng = hex2dec('25'); %what range of ILAD was used
ports.adc_ioutm_rng = hex2dec('26'); %what range of ILAD was used

% % XEM TriggerIn Ports
ports.tr.dac = hex2dec('40'); %DAC INTERFACE: 0-start,1-reset
ports.tr.adc = hex2dec('41'); %ADC AUTO:0-reset,1to8-start_conv_1to8,9-convert both IOUTP/M,10-single conversion, input selected by mux
ports.tr.idac = hex2dec('42'); %IDAC: 0-reset,1-trig range,2-trig val
ports.tr.greycounter = hex2dec('43'); %TOPOLOGY CONFIG:0-reset,1-trig
ports.tr.dpulse = hex2dec('44'); %DPULSE: 0-reset,1-set to 0,2-set to 1,3-start

% % XEM TriggerOut Ports: COUNTING STARTS FROM 1
ports.tro.adc = hex2dec('60'); %ADC: 1-adc_done,2-iout_conv_done


ports.dac.LDO_en = 4;
ports.dac.LDOt_en = 5;
ports.dac.LDO_sel = 6;
ports.dac.vbias = 7;
ports.dac.vneg = 8;
ports.dac.mode = 9;
ports.dac.threshold = 10;
ports.dac.vcasp_in = 11;
ports.dac.VCO_ctrl = 12;
ports.dac.vinjectSub = 13;
ports.dac.vcascn = 14;
ports.dac.col_buf = 15;%Gate2 signal
ports.dac.vtun = 16;
ports.dac.ibias2 = 17;  ports.dac.vgate2_ext = 18;
ports.dac.vgate1 = 19;
ports.dac.IO_drain = 20;
ports.dac.avdd = 21;
ports.dac.external = 22;
ports.dac.sw_bias = 23;
ports.dac.VCO_vt=28;
ports.dac.comp0 = 32;
ports.dac.comp1 = 33;
ports.dac.comp2 = 34;
ports.dac.comp3 = 35;
ports.dac.sen_ref = 36;
ports.dac.comp4 = 37;
ports.dac.comp5 = 38;
ports.dac.sensor_sel = 39;
ports.n_count=hex2dec('16');
ports.trigs=hex2dec('56');
ports.counterout=hex2dec('36');
ports.counterdone=hex2dec('37');

%FPGA Init: internal state machines
activatetriggerin(xem,ports.tr.dac,1); % dac interface block reset
activatetriggerin(xem,ports.tr.adc,0); % adc interface block reset
activatetriggerin(xem,ports.tr.dpulse,0); % dpulse interface block reset
activatetriggerin(xem,ports.tr.idac,0); % idac interface block reset
activatetriggerin(xem,ports.trigs,2) %reset graycounter trig
%DAC5380 Init
write_fpga_reg(ports.dac_reg,bin2dec('0')); % reg 1,2 = 0
write_fpga_reg(ports.dac_addr,bin2dec('1100')); % addr: 001100
write_fpga_reg(ports.dac_db,bin2dec('00100000000000')); % boosted output current
activatetriggerin(xem,ports.tr.dac,0); % start writing to dac

%%%%%%%%%Setup Start%%%%%%%%%%%%%%%%%%%%%
write_dac_v(ports.dac.comp0,0);
write_dac_v(ports.dac.comp1,0);
write_dac_v(ports.dac.comp2,0);
write_dac_v(ports.dac.LDO_en,3.3);
write_dac_v(ports.dac.LDOt_en,0);
write_dac_v(ports.dac.LDO_sel,0);
write_dac_v(ports.dac.sensor_sel,0);
write_dac_v(ports.dac.VCO_vt,0);

