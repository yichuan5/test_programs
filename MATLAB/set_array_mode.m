function [ output_args ] = set_array_mode(iv)
% configure NN array into various syn/fg/prog/meas modes
% input is a vector of the form [dsel=(0:3) gsel0 addr_en prog meas lock]
global ports;

cfg = [fliplr(dec2binvec(iv(1),2)) iv(2:end)]; 
write_fpga_reg(ports.nn_config,binvec2dec(fliplr(cfg)));
end

