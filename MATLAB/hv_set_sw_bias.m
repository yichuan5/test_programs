function hv_set_sw_bias(v);
global ports;
write_dac_v(ports.dac.sw_bias,v/2);