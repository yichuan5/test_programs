%%%%%%%%%%%%%%%%%%%%%%%custom voltage accuracy%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
write_fpga_reg(ports.n_count,1)
activatetriggerin(xem,ports.trigs,0) %count_trig
activatetriggerin(xem,ports.trigs,1) %out_trig
activatetriggerin(xem,ports.trigs,2) %reset_trig

write_dac_v(ports.dac.LDO_en,3.3);
write_dac_v(ports.dac.LDOt_en,0);



activatetriggerin(xem,ports.trigs,2) %reset_trig
fileID = fopen('v.txt','w');

for i = 0.1:0.1:1.4
    write_dac_v(ports.dac.comp0,i);
    write_dac_v(ports.dac.comp1,i);
    write_dac_v(ports.dac.comp2,i);
    pause(1)
    repeatADC(4,32)/2
    repeatADC(8,32)/2
    repeatADC(5,32)/2
    
    pause(1)
    fprintf(fileID,'%f ,%f ,%f , %f\r\n',i,repeatADC(4,32),repeatADC(8,32),repeatADC(5,32));
end
fclose(fileID);

read_adc_v(6)